#!/usr/bin/env python3

"""
Routine to test all the implemented test cases.
Following utilizes the Python FEMOCS wrapper.

@author : T.Tiirats
@date : 24.10.2022

Comments:

To run this routine FEMOCS needs to be compiled as Python library: make pylib

This little testing routine calls all the tests added into the "List_of_tests"
list. It producesa dictionary with True - PASS and False - FAIL values.

The routine checks that FEMOCS does not crash during a run and passes the small
verifications. Furthermore, it compares the I_tot values from "emission.dat" with
values from "reference_emission.dat" (earlier established run). So if new contribu-
tion will change the I_tot values, it will display it as FALSE. In that case, the
reference file needs to updated.

The input file needs to have "heat_mode = transient" to produce the emission.dat
file.
"""

import multiprocessing
import numpy as np
import math as mat
from femocs import Femocs
import csv
import os


# List of tests to consider:
List_of_tests = [
    "extended_flat",
    "extended_round",
    "flat",
    "hemicone",
    "kmc_mushroom",
    "md_small",
    "md_molten",
    "runaway",
    "plasma" # Uses a restart file!
]





Dict_of_test_results = dict.fromkeys(List_of_tests,"False")

## FUNCTIONS #########################################
def load_input_atoms_file(file_name,fem_obj):
    """
    Function to read in atoms coordinates from file.
    """
    n_atoms = fem_obj.import_file(file_name)
    if n_atoms < 0:
       raise RuntimeError("Could not import atoms")
    else:
       print("Imported {} atoms".format(n_atoms))

def emission_data_file_check():
    """
    If PIC is solved, run additional check on the I_tot values.
    """
    reference_result = np.loadtxt('reference_emission.dat',skiprows=1,usecols=1)
    test_result = np.loadtxt('out/emission.dat',skiprows=1,usecols=1)
    if np.shape(test_result) != np.shape(reference_result):
        print("\n======================================================\n",
              "Emission data files have different length !! ",
              "\n======================================================\n" )

    # In case one of the emission files has a single value
    if test_result.size < reference_result.size: nr_terms = test_result.size
    else: nr_terms = reference_result.size
    if nr_terms == 1:
        if test_result.size*reference_result.size == 1: is_equal = np.allclose(test_result,reference_result,rtol=1e-2)
        else:
            if test_result.size == 1: is_equal = np.allclose(test_result,reference_result[0],rtol=1e-2)
            else: is_equal = np.allclose(test_result[0],reference_result,rtol=1e-2)
    else:
        is_equal = np.allclose(test_result[:nr_terms],reference_result[:nr_terms],rtol=1e-2)


    
    if is_equal:
        return True
    else:
        print("Result: ", test_result, "\nReference: ", reference_result)
        print("\n======================================================\n",
              "I_tot != I_tot(reference) || CHECK PIC IMPLEMENTATION!! ",
              "\n======================================================\n")
        return False

def run_testcase(test_name):
    """
    Run the individual test.
    Needs to be in the project folder.
    """
    fem = Femocs("md.in")
    #print("\nFemocs version =", fem.version())

    # Search for folder for existing input files
    if os.path.exists('atoms.ckx'): load_input_atoms_file('atoms.ckx',fem)
    if os.path.exists('atoms.xyz'): load_input_atoms_file('atoms.xyz',fem)

    # Run FEMOCS
    error = fem.run(0, 0.0)
    if error:
        raise RuntimeError("Failed to run Femocs; error code: {}".format(error))

    # Check for results
    Dict_of_test_results[test_name] = emission_data_file_check()





## MAIN #################################################
for test in List_of_tests:

    print("\nLAUNCHING TEST IN: ", test, "=========================================\n")
    os.chdir(test)
    try:
        os.mkdir("out")
    except(FileExistsError):
        pass

    print(test)
    try:
        process = multiprocessing.Process(target=run_testcase(test))
        process.start()
        process.join()
    except KeyboardInterrupt:
        process.terminate()

    os.chdir('../')

print("\nTESTING COMPLETE =========================\n")
print("RESULTS:")
[print(key,':',value) for key, value in Dict_of_test_results.items()]

## END ############
