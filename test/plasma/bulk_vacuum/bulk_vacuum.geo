// Mesh.Algorithm = 1; // MeshAdapt
Mesh.Algorithm = 8; // frontal-delaunay for quads
Mesh.RecombinationAlgorithm = 1; // blossom
Mesh.RecombineAll = 1;
Mesh.SubdivisionAlgorithm = 1; // all quads

sf = 1.e-3; // global mesh size factor
coarsener = 40.0; // bulk bottom and vacuum top/edge coarsening factor
// factors to determine mesh size on tip (< 1 to refine)
tipcoarsener1 = 1.0; // top
tipcoarsener2 = 5.0; // side
tipcoarsener3 = 60.0; // bottom

Rtip = 500;  // radius of the tip
hangle = 15; // aperture angle of the tip (deg)
hangle = hangle * Pi / 180; // to radians
dist = 10 * Rtip; // tip-anode distance
height = 10 * Rtip; // height of the tip

// dimensions of vacuum and bulk
wbox = 0.5 * (height + dist);
hbox = dist + Rtip;
hbulk = 0.5 * height;

vacuum = 1;
cathode = 2;
anode = 8;
bottom = 7;
bulk = 10;

lc = sf; // lc scales geometry mesh size

theta = 0.5 * Pi - hangle;
gamma = Rtip / Sin[hangle];
beta = (height - Rtip); // height from circle center
base = (beta + gamma) * Tan[hangle];

Include "geometry.geo";

// vacuum
Line Loop(12) = {1, 2, 4, 5, 6, 7};
Plane Surface(12) = {12};

Physical Line(cathode) = {1, 2, 4}; // cathode
Physical Line(anode) = {6};
Physical Surface(vacuum) = {12};

Mesh 2;
OptimizeMesh "Laplace2D";
Save "vacuum.msh";
Delete Model;

Include "geometry.geo";

// bulk
Line Loop(13) = {1, 2, 4, 8, 9, 10};
Plane Surface(13) = {13};

Physical Line(cathode) = {1, 2, 4}; // cathode
Physical Surface(bulk) = {13};
Physical Line(bottom) = {9};

Mesh 2;
OptimizeMesh "Laplace2D";
Save "bulk.msh";

