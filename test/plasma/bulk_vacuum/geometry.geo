// vacuum-bulk interface -> dense mesh
Point(0) = {0,0,0, lc * (base + beta) * tipcoarsener1}; // circle center
Point(1) = {0, Rtip, 0, lc * (base + beta) * tipcoarsener1}; // tip
Point(2) = {Rtip * Sin[theta], Rtip * Cos[theta], 0, lc * (base + beta) * tipcoarsener1}; // end of circle
Point(3) = {base, -beta, 0, lc * (base + beta) * tipcoarsener2}; // end of triangle
Point(5) = {wbox, -beta,  0, lc * wbox * tipcoarsener3}; // right box corner

// edges of simulation domain -> coarse mesh
Point(6) = {wbox, hbox, 0,  lc * (wbox + hbox) * coarsener}; // right anode corner
Point(7) = {0,   hbox, 0, lc * hbox * coarsener}; // left anode corner
Point(8) = {0, -beta - hbulk, 0, lc * beta * coarsener}; // bottom left of the bulk
Point(9) = {wbox, -beta - hbulk, 0, lc * wbox * coarsener}; // bottom right of the bulk

Circle(1) = {1, 0, 2}; // hemisphere
Line(2) = {2, 3}; // triangle hypotenuse
Line(4) = {3, 5}; // bottom
Line(5) = {5, 6}; // right wall
Line(6) = {6, 7}; // anode
Line(7) = {7, 1}; // left wall

Line(8) = {5, 9}; // side bulk
Line(9) = {9, 8}; // bottom
Line(10) = {8, 1}; // left side

