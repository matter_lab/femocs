#############################################################################
#     Makefile for building FEMOCS - Finite Elements on Crystal Surfaces
#                             Mihkel Veske 2016
#
#############################################################################
# Before running make install-taito or running Femocs in Taito, run
#   source ./build/load_modules.sh taito
#
# Before running make install-alcyone or running Femocs in Alcyone, run
#   source ./build/load_modules.sh alcyone
#
# Before running make install-kale or running Femocs in Kale, run
#   source ./build/load_modules.sh kale
#############################################################################

include build/makefile.defs
-include share/makefile.femocs


all: lib

lib: femocs_lib
femocs_lib:
	@+make --no-print-directory -f build/makefile.lib build_type=Release lib_type=lib

dlib: femocs_dlib
femocs_dlib:
	@+make --no-print-directory -f build/makefile.lib build_type=Debug lib_type=lib

pylib: femocs_pylib
femocs_pylib:
	@+make --no-print-directory -f build/makefile.lib build_type=Release lib_type=pylib

pydlib: femocs_pydlib
femocs_pydlib:
	@+make --no-print-directory -f build/makefile.lib build_type=Debug lib_type=pylib

test_f90: femocs_f90
femocs_f90:
	@+make --no-print-directory -f build/makefile.main main=${FMAIN} compiler=${F90} libs="${FEMOCS_LIB}" opts="${FEMOCS_OPT}" build_type=Release

test_c: femocs_c
femocs_c:
	@+make --no-print-directory -f build/makefile.main main=${CMAIN} compiler=${CC} libs="${FEMOCS_LIB}" opts="${FEMOCS_OPT}" build_type=Release

test_cpp: femocs_release

release: femocs_release
femocs_release:
	@+make --no-print-directory -f build/makefile.main main=${CXXMAIN} compiler=${CXX} libs="${FEMOCS_LIB}" opts="${FEMOCS_OPT}" build_type=Release

debug: femocs_debug
femocs_debug:
	@+make --no-print-directory -f build/makefile.main main=${CXXMAIN} compiler=${CXX} libs="${FEMOCS_DLIB}" opts="${FEMOCS_DOPT}" build_type=Debug

exec: femocs_exec
femocs_exec:
	@+make --no-print-directory -f build/makefile.exec build_type=Release

dexec: femocs_dexec
femocs_dexec:
	@+make --no-print-directory -f build/makefile.exec build_type=Debug

doc: femocs_doc
femocs_doc:
	make -f build/makefile.doc

install-ubuntu:
	@bash build/install.sh ubuntu
	@+make --no-print-directory -f build/makefile.install

install-arch:
	@bash build/install.sh arch
	@+make --no-print-directory -f build/makefile.install

install-taito:
	@bash build/install.sh taito
	@+make --no-print-directory -f build/makefile.install

install-alcyone:
	@bash build/install.sh alcyone
	@+make --no-print-directory -f build/makefile.install

install-kale:
	@bash build/install.sh kale
	@+make --no-print-directory -f build/makefile.install

install-turso:
	@chmod +x ./build/install.sh
	@./build/install.sh turso

uninstall-all:
	@bash build/uninstall-all.sh

clean:
	make -s -f build/makefile.lib clean
	make -s -f build/makefile.exec clean
	make -s -f build/makefile.main clean
	make -s -f build/makefile.doc clean

clean-all:
	make -s -f build/makefile.lib clean-all
	make -s -f build/makefile.exec clean-all
	make -s -f build/makefile.main clean-all
	make -s -f build/makefile.doc clean-all

help:
	@echo ''
	@echo 'make all        pick default build type for Femocs'
	@echo ''
	@echo 'make install-'
	@echo '       arch     build in Arch/Manjaro desktop Femocs mandatory dependencies'
	@echo '       ubuntu   build in Ubuntu desktop Femocs mandatory dependencies'
	@echo '       taito    build in CSC Taito cluster all the external libraries that Femocs needs'
	@echo '       alcyone  build in Alcyone cluster all the external libraries that Femocs needs'
	@echo '       kale     build in Kale cluster all the external libraries that Femocs needs'
	@echo ''
	@echo 'make uninstall-'
	@echo '       all      remove all installation files'
	@echo ''
	@echo 'make lib        build Femocs as static library with maximum optimization level'
	@echo 'make dlib       build Femocs as static library with debugging features enabled'
	@echo 'make pylib      build Femocs as a python library'
	@echo 'make exec       build Femocs executable from c++ main with highest optimization level by using cmake'
	@echo 'make dexec      build Femocs executable from c++ main with debugging features enabled by using cmake'
	@echo 'make release    build Femocs executable from c++ main with highest optimization level by using make'
	@echo 'make debug      build Femocs executable from c++ main with debugging features enabled by using make'
	@echo ''
	@echo 'make test_f90   build Femocs executable from Fortran main'
	@echo 'make test_c     build Femocs executable from C main'
	@echo 'make test_cpp   build Femocs executable from C++ main'
	@echo ''
	@echo 'make doc        generate Femocs documentation in html and pdf format'
	@echo 'make clean      delete key files excluding installed libraries to start building from the scratch'
	@echo 'make clean-all  delete all the files and folders produced during the make process'
	@echo ''
