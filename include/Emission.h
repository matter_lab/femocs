/*
 * Emission.h
 *
 *  Created on: 11 Nov 2019
 *      Author: kyritsak
 */

#ifndef INCLUDE_EMISSION_H_
#define INCLUDE_EMISSION_H_

#include "FileWriter.h"
#include <deal.II/dofs/dof_handler.h>


namespace femocs {
using namespace std;
using namespace dealii;


/** Some forward declarations to minimize # included headers */
class Config;
template<int dim> class Pic;
template<int dim> class FieldSolver;
template<int dim> class CurrentHeatSolver;


/** Class to calculate field emission effects with GETELEC */
template <int dim>
class Emission: public FileWriter {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename DoFHandler<dim>::active_face_iterator Face;

    Emission(const FieldSolver<dim> *psolver, const Config &conf,
            const CurrentHeatSolver<dim> *chsolver);

    /** Store fields on surface centroids; needs to be called before calc_emission */
    void extract_fields();

    /** Calculates the emission currents and Nottingham heat distributions,
     * including a rough estimation of the space charge effects.
     * @param conf Emission configuration parameters
     * @param Veff effective applied voltage for space charge calculations. if =0, SC neglected
     * @param deff effective gap distance for space charge calculations. if=0, Veff is used
     */
    int calc_emission(double deff = 0., bool update_eff_region = false);

    /** Calculates bombardment heat */
    void calc_bombardment(const vector<Face> &bombard_faces, const vector<double> &bombard_energies);

    /** Calculates the vapor flux and the vapor heat absorbption on surface faces
     * for vapor particles of a given @param mass [amu] */
    void calc_evaporation(double mass);

    /** Calculates the the cooling on surface faces due to black-body radiation */
    void calc_blackbody();

    /** Calculates the total heat deposited on the surface */
    void calc_total_heat();

    /** Calculates the mean and the standard deviation of the total current for the last N_calls */
    void calc_global_stats();

    /** Initialises class data */
    void initialize();

    void reduce_work_function(double reduction_factor){
    	work_function *= reduction_factor;
    }

    double get_work_function(){
    	return work_function;
    }

    /** Return nr of faces */
    int size() const { return centroids.size(); }

    /** Return the temperature on the i-th face in the vacuum indexing */
    double get_temperature(int i) const;

    /** Getter for the vapor flux on a i-th face in the vacuum indexing */
    double get_vapor_flux(int i) const;

    /** Getter for the current value on a i-th face in the vacuum indexing */
    double get_current(int i) const;

    /** Getter for the i-th interface cell from in the vacuum indexing */
    Cell get_cell(int i) const;

    /** Getter for the i-th interface face index in the vacuum indexing */
    int get_face(int i) const;

    /** Getter for the i-th interface face area in the vacuum indexing */
    double get_area(int i) const;

    /** Getter for the i-th interface cell volume in the vacuum indexing */
    double get_vol(int i) const;

    /** Centroid of the i-th face in the vacuum indexing */
    Point<dim> get_centroid(int i) const;

    /** Electric field at the centroid of the i-th face in the vacuum indexing */
    Tensor<1,dim> get_field(int i) const;

    const vector<double>* get_current_densities() const { return &current_densities; }

    const vector<double>* get_surface_heat() const { return &surface_heats; }

    void set_sfactor(double factor) { global_data.sfactor = factor; }

    void set_chsolver(const CurrentHeatSolver<dim> *chs) { chsolver = chs; }

    /** Write the statistics into file */
    void write_stats(ofstream &out, const string &pre_header="") const;

    /** Write the global data into dat-file */
    void write_dat(ofstream &out, const string &pre_header="") const;

    /** Write all emission data */
    void write();

    struct GlobalData {
        double theta = 1;       ///< correction multiplier for Space Charge
        double sfactor = 1;     ///< factor that rescales the field (accounts for changed applied V or F)
        double Jmax = 0;    ///< Maximum current density of the emitter [in amps/A^2]
        double Emax = 0;    ///< Maximum electric field value on the input fields [V/A]
        double Fmax = 0;    ///< Maximum local field on the emitter [V/A]
        double Frep = 0;    ///< Representative local field (used for space charge equation) [V/A]
        double Jrep = 0;    ///< Representative current deinsity for space charge. [amps/A^2]
        double I_tot = 0;   ///< Total current running through the surface [in Amps]
        double I_eff = 0;   ///< Total current within the effective area
        double area = 0;    ///< total area of emitting region
        double Tmax = 0;    ///< Maximum temperature on surface faces
    } global_data;

    struct Stats {
        int N_calls;        ///< Counter keeping the last N_calls

        vector<double> I_tot; ///< List of all the I_tot for the last N_calls (useful for convergence check)
        double Itot_mean = 0;  ///< Mean current of the last Ilist;
        double Itot_std = 0;  ///< STD of the last Ilist;

        vector<double> Jrep; ///< List of all the Jrep for the last N_calls (useful for convergence check)
        double Jrep_mean = 0;  ///< Mean of the last Jrep list;
        double Jrep_std = 0;  ///< STD of the last Jrep list;

        vector<double> Frep; ///< List of all the Frep for the last N_calls (useful for convergence check)
        double Frep_mean = 0;  ///< Mean of the last Frep list;
        double Frep_std = 0;  ///< STD of the last Frep list;

        vector<double> Jmax; ///< List of all the Jmax for the last N_calls (useful for convergence check)
        double Jmax_mean = 0;  ///< Mean of the last Jmax list;
        double Jmax_std = 0;  ///< STD of the last Jmax list;

        vector<double> Fmax; ///< List of all the Fmax for the last N_calls (useful for convergence check)
        double Fmax_mean = 0;  ///< Mean of the last Fmax list;
        double Fmax_std = 0;  ///< STD of the last Fmax list;

        /** String stream prints the statistics */
        friend ostream& operator <<(ostream &stream, const Stats &e) {
            stream << "Itot=" << e.Itot_mean << "+-" << e.Itot_std
                    << ", Jrep=" << e.Jrep_mean << "+-" << e.Jrep_std
                    << ", Frep=" << e.Frep_mean << "+-" << e.Frep_std
                    << ", Jmax=" << e.Jmax_mean << "+-" << e.Jmax_std
                    << ", Fmax=" << e.Fmax_mean << "+-" << e.Fmax_std;
            return stream;
        }
    } stats;

private:
    /** Prepares the line inputed to GETELEC.
     *
     * @param point      Starting point of the line
     * @param direction  Direction of the line
     * @param rmax       Maximum distance that the line extends
     */
    void emission_line(int i, const Tensor<1,dim>& direction, const double rmax);

    /** Calculates all the global values */
    void calculate_globals();

    /** Calculates the interface and mapping data between temperature and field solvers */
    void calc_interface();

    /** Current density on vacuum interface indexing */
    double& current_density(int i) { return current_densities[vac2bulk[i]]; }
    double current_density(int i) const { return current_densities[vac2bulk[i]]; }

    /** Surface heat on vacuum interface indexing */
    double& surface_heat(int i) { return surface_heats[vac2bulk[i]]; }
    double surface_heat(int i) const { return surface_heats[vac2bulk[i]]; }

    /** Reads the temperatures on the surface faces from the chsolver */
    void extract_temperatures();

    /**
     * Calculates the effective emission area (assigns flag to each surface face
     * @param threshold minimum value (fraction of maximum) to be considered effective area
     * @param mode "field" or "current", whether the criterion is on the field or the current
     */
    void calc_effective_region(double threshold, const string &mode);

    /** Specify file types that can be written */
    bool valid_extension(const string &ext) const {
        return ext == "xyz" || ext == "movie" || ext == "dat";
    }

    /** Compose entry to xyz or movie file */
    void write_xyz(ofstream &out) const;

    static constexpr double nm_per_angstrom = 0.1;
    static constexpr double angstrom_per_nm = 10.0;
    static constexpr double nm2_per_angstrom2 = 0.01;
    static constexpr int n_lines = 32; ///< Number of points in the line for GETELEC

    const Config &config;
    const FieldSolver<dim> *psolver;         ///< solver containing the electric field
    const CurrentHeatSolver<dim> *chsolver;  ///< CurrentHeat solver containing current and heat

    /** Data that follow the vacuum ordering */
    vector<double> currents;    ///< Current flux for every face (current_densities * face_areas) [Amps]
    vector<Tensor<1,dim>> fields; ///< Electric field at the faces where current is calculated [V/A]
    vector<double> vapor_flux; ///< Emission of evaporated atoms on surface faces (vacuum ordering) [A^-2 fs^-1].
    vector<double> thetas_SC;   ///< local field reduction factor due to SC
    vector<int> markers;        ///< debug data about how was emission calculated on given face
    vector<bool> is_effective;  ///< effective emission area
    vector<double> temperatures;       ///< Temperatures on the face centroids [K]
    vector<double> nottingham;         ///< Nottingham heat deposition on the faces [W/A^2]
    vector<double> vapor_heat;         ///< Heat deposited (absorbed) due to evaporation [W/A^2]
    vector<double> blackbody_heat;         ///< Heat deposited (absorbed) due to black-body cooling [W/A^2]
    vector<double> bombardment_heat;   ///< Heat deposited due to particle bombardment [W/Å^2]

    /** Data that follow the bulk ordering */
    vector<double> current_densities;  ///< Emitted current densities on the interface faces [Amps/A^2]. Bulk mesh ordering.
    vector<double> surface_heats;      ///< Total heat deposited on the faces [W/Å^2]. Bulk mesh ordering.

    /** Mapping between bulk and vacuum ordering */
    vector<int> vac2bulk;       ///< maps the face indices from the vacuum mesh to the bulk mesh
    vector<int> bulk2vac;       ///< maps the face indices from the bulk mesh to the vacuum mesh

    /** Vectors for emission line */
    vector<double> rline;       ///< Line distance from the face centroid (passed into GETELEC)
    vector<double> Vline;       ///< Potential on the straight line (complements rline)

    vector<Point<dim>> bulk_centroids; ///< Centroids of the bulk boundary faces
    vector<Cell> bulk_cells;      ///< Cells adjacent to bulk boundary
    vector<Point<dim>> centroids; ///< Centroids of the vacuum boundary faces
    vector<Cell> cells;           ///< Cells adjacent to vacuum boundary
    vector<int> faces;            ///< Face indices of the vacuum boundary faces within the cells
    vector<double> areas;         ///< Areas of the boundary faces
    double work_function;		///< Work function
};

}  // namespace femocs

#endif /* INCLUDE_EMISSION_H_ */
