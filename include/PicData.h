/*
 * PicData.h
 *
 *  Created on: Jun 23, 2020
 *      Author: kyritsak
 */

#ifndef PICDATA_H_
#define PICDATA_H_

#include "PhysicalQuantities.h"
#include "Config.h"

namespace femocs {

class PicData: public PhysicalQuantities {
public:
	PicData(const Config::PIC &config);

    /** Get the cross section [\AA^2] for a given ionization @param level with a
     *  given collision @param energy [eV] for neutrals */
    double get_neutral_ionization_cs(double energy, int level = 1) const {
        return loglog_interp(energy, neutral_ionization_cs.front(),
                neutral_ionization_cs[level]) * 1.e20;
    }

    /** Get the cross section [\AA^2] for a given ionization @param level with a
     *  given collision @param energy [eV] for ions */
    double get_ion_ionization_cs(double energy, int level = 1) const {
        return loglog_interp(energy, ion_ionization_cs.front(),
                ion_ionization_cs[level]) * 1.e20;
    }

    /** Get the neutral-neutral cross section [\AA^2] for a given @param energy [eV] */
    double get_nn_elastic_cs(double energy) const {
        return loglog_interp(energy, nn_elastic_cs.front(), nn_elastic_cs.back())
                * 1.e20;
    }

    /** Get the electron-neutral cross section [\AA^2] for a given @param energy [eV] */
    double get_en_elastic_cs(double energy) const {
        return loglog_interp(energy, en_elastic_cs.front(), en_elastic_cs.back())
                * 1.e20;
    }

    /** Get the electron-ion recombination cross section [\AA^2] for a given @param energy [eV] */
    double get_recombination_cs(double energy, int level = 1) const {
        if (level > 1) // TODO: recombination for level > 1
            return 0.;
        return loglog_interp(energy, recombination_cs.front(), recombination_cs.back())
                * 1.e20;
    }

    /** Get the charge exchange and momentum transfer cross section [\AA^2] for a given @param energy [eV] */
    double get_exchange_cs(double energy) const {
        return loglog_interp(energy, exchange_cs.front(), exchange_cs.back())
                * 1.e20;
    }

    /** Get ionization energy for a certain ionization level, e.g. level 1 = Cu+, level 2 = Cu2+, ... */
    double get_ionization_energy(int level = 1) const {
        if (level >= ionization_levels)
            return ionization_energies.back();
        if (level < 1)
            return ionization_energies.front();

        return ionization_energies[level - 1];
    }

    /** Sum ionization energies from neutral to a certain ionization level */
    double sum_ionization_energy(int level) const {
        double Ei = 0.;

        for (int j = 1; j <= level; ++j)
            Ei += get_ionization_energy(j);

        return Ei;
    }

    /** Get the sputtering yield for a given @param energy [eV] and @param angle [deg] */
    double get_sput_yield(double energy, double angle) const {
    	if (!conf.sputtering)
    		return 0.;
        return linear_interp(energy, sput_yields.front(), sput_yields.back());
    }

    /** Get the sputtering @param energy [eV] a given incident @param energy [eV] and @param angle [deg] */
    double get_sput_energy(double energy, double angle) const {
    	if (!conf.sputtering)
    		return 0.;
        return linear_interp(energy, sput_energies.front(), sput_energies.back());
    }

    int ionization_levels;

private:
    const Config::PIC &conf;                              ///< Pic configuration parameters
    vector<double> ionization_energies;                   ///< Ionization energies between levels
    mutable vector<vector<double>> neutral_ionization_cs; ///< Ionization energies for multiple ionization of neutrals, first vector contains energies in eV
    mutable vector<vector<double>> ion_ionization_cs;     ///< Ionization energies for single ionization of ions, first vector contains energies in eV
    mutable vector<vector<double>> nn_elastic_cs;         ///< Ionization energies for elastic neutral-neutral collision, first vector contains energies in eV
    mutable vector<vector<double>> en_elastic_cs;         ///< Ionization energies for elastic electron-neutral collision, first vector contains energies in eV
    mutable vector<vector<double>> recombination_cs;      ///< Ionization energies for recombination collision, first vector contains energies in eV
    mutable vector<vector<double>> exchange_cs;           ///< Ionization energies for recombination collision, first vector contains energies in eV

    mutable vector<vector<double>> sput_energies; ///< Energy distribution of sputtered particles
    mutable vector<vector<double>> sput_yields; ///< Sputtering yield depending on energy

    /** Method to read the tabulated cross section data from file.
     * If file not found, hard coded data are used. */
    void initialize();
};

} /* namespace femocs */

#endif /* PICDATA_H_ */
