/*
 * CircuitModel.h
 *
 *  Created on: Sep 25, 2021
 *      Author: veske
 */

#ifndef INCLUDE_CIRCUITMODEL_H_
#define INCLUDE_CIRCUITMODEL_H_


#include "Config.h"
#include <iostream>


using namespace std;
namespace femocs {


/** General specification for an external circuit. */
class CircuitModel {
public:
    CircuitModel(const Config::CircuitModel *conf);
    virtual ~CircuitModel() {};

    /** Bring circuit to the zeroth time step */
    virtual void init(double E0, double V0);

    /** Calculate the voltage on the gap in the next timestep */
    virtual void timestep(double delta_t, double I_gap) = 0;

    double elfield() const { return el_field; }
    double Vgap() const { return V_gap; };
    double Igap() const { return I_gap; };

    /** Identify and load the proper circuit class */
    static CircuitModel* load_circuit(const Config::CircuitModel *conf);

    /** Define the behaviour of string stream */
    friend std::ostream& operator <<(std::ostream &s, const CircuitModel &cm) {
        return s << "Vgap=" <<  cm.V_gap << ", Igap=" << cm.I_gap;
    }

protected:
    const Config::CircuitModel *conf;  ///< Configuration parameters
    double E0;         ///< Initial applied electric field
    double V0;         ///< Electromotive force of the power source
    double el_field;   ///< Currently applied electric field on top of simubox
    double V_gap;      ///< Voltage applied on the gap
    double I_gap;      ///< Current in the gap
    double I_circuit;  ///< Current in the circuit model
};


/** Keep a fixed voltage on the gap. */
class FixedVoltageCircuit : public CircuitModel {
public:
    FixedVoltageCircuit(const Config::CircuitModel *conf);

    void timestep(double delta_t, double I_gap);
};


/** Fixed supply voltage and a series resistor */
class ResistorCircuit : public CircuitModel {

public:
    ResistorCircuit(const Config::CircuitModel *conf);

    void timestep(double delta_t, double I_gap);
};


/** Fixed supply voltage, a series resistor, and gap capacitance. */
class ResistorCapacitorCircuit : public CircuitModel {
public:
    ResistorCapacitorCircuit(const Config::CircuitModel *conf);

    void timestep(double delta_t, double I_gap);
};

}  // namespace femocs

#endif /* INCLUDE_CIRCUITMODEL_H_ */
