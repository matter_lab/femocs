/*
 * ProjectSwiftHI.h
 *
 *  Created on: 31 Jan 2020
 *      Author: kyritsak
 */


#ifndef PROJECTSWIFTHI_H_
#define PROJECTSWIFTHI_H_


#include "GeneralProject.h"
#include "Emission.h"
#include "HeatData.h"


namespace femocs {

/** Some forward declarations to minimize # included headers */
template<int dim> class PicSolver;


template <int dim>
class ProjectSwiftHI : public GeneralProject {
public:
    ProjectSwiftHI(AtomReader &reader, Config &config);

    int run(const int timestep=-1, const double time=-1);

    int export_data(double* data, const int n_points, const string& data_type) { return 0; }

    int export_data(int* data, const int n_points, const string& data_type) { return 0; }

    int interpolate(double* data, int* flag,
                const int n_points, const string& data_type, const bool near_surface,
                const double* x, const double* y, const double* z) { return 0; }

    int restart(const string& path_to_file) { return 0; }

private:
    vector<double> I_pic, I_sc;

    HeatData phys_quantities;
    PicSolver<dim> pic;
    Emission<dim> emission;
    CurrentHeatSolver<dim> ch_solver;

    void write_output(double factor, bool first_line = true);
};

}  // namespace femocs

#endif /* SRC_PROJECTSWIFTHI_H_ */
