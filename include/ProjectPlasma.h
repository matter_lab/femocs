/*
 * ProjectPlasma.h
 *
 *  Created on: Jun 14, 2020
 *      Author: kyritsak
 */
#ifndef INCLUDE_PROJECTPLASMA_H_
#define INCLUDE_PROJECTPLASMA_H_


#include "GeneralProject.h"
#include "PicSolver.h"
#include "HeatData.h"

using namespace std;
namespace femocs {


template <int dim>
class ProjectPlasma : public GeneralProject {
public:
    ProjectPlasma(AtomReader &reader, Config &config);

    int run(const int timestep = -1, const double time = -1);

    int export_data(double* data, const int n_points, const string& data_type) { return 0; }

    int export_data(int* data, const int n_points, const string& data_type) { return 0; }

    int interpolate(double* data, int* flag,
                const int n_points, const string& data_type, const bool near_surface,
                const double* x, const double* y, const double* z) { return 0; }

    int restart(const string& path_to_file);

private:
    bool fail;                  ///< If some process failed
    bool restarting;
    bool first_run;             ///< True only as long as there is no full run
    double last_heat_time;      ///< Last time heat was updated
    double last_pic_time;       ///< Last time PIC solver was called
    int last_restart_ts;        ///< Last time step reset file was written
    bool mesh_changed;          ///< True if new mesh has been created

    double t0 = 0;
    vector<double> I_pic, I_sc, Vappl, Flap;
    double deff = 100.;

    HeatData heat_data;
    PicSolver<dim> pic;
    Emission<dim> emission;
    CurrentHeatSolver<dim> ch_solver;

    /** Write restart file so that simulation could be started at t>0 time */
    void write_restart(const string& path_to_file);
};

} // namespace femocs

#endif /* INCLUDE_PROJECTPLASMA_H_ */
