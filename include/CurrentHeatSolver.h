/*
 * currents_and_heating.h -> CurrentsAndHeating.h
 *
 *  Created on: Jul 28, 2016
 *      Author: kristjan, Mihkel
 */

#ifndef CURRENTSANDHEATING_H_
#define CURRENTSANDHEATING_H_


#include "DealSolver.h"
#include "HeatData.h"
#include "Config.h"
#include <deal.II/numerics/data_out_dof_data.h>

namespace femocs {

// forward declare some classes to make them available for declaring dependencies
template<int dim> class HeatSolver;
template<int dim> class CurrentHeatSolver;

using namespace dealii;
using namespace std;

template<int dim>
class BulkSolver : public DealSolver<dim> {
public:
    BulkSolver(Triangulation<dim> *tria, const HeatData *pq,
            const Config::Heating *conf, const vector<double>* bc_values);

    /** Solve the matrix equation using conjugate gradient method */
    int solve() { return this->solve_cg(conf->n_cg, conf->cg_tolerance, conf->ssor_param); }

    /** Return the boundary condition value at the centroid of face */
    double get_face_bc(const unsigned int face) const;

protected:
    const HeatData *pq;   ///< tabulated physical quantities (sigma, kappa, gtf emission)
    const Config::Heating *conf;      ///< solver parameters
    const vector<double>* bc_values;  ///< current/heat values on the centroids of surface faces for current/heat solver
};

template<int dim>
class CurrentSolver : public BulkSolver<dim> {
public:
    CurrentSolver(Triangulation<dim> *tria, const HeatSolver<dim> *hs, const HeatData *pq,
            const Config::Heating *conf, const vector<double>* bc_values);

    /** @brief assemble the matrix equation for current density calculation.
     * Calculate sparse matrix elements and right-hand-side vector
     * according to the continuity equation weak formulation and to the boundary conditions.
     * The solution of the system is a "current potential v"  J = grad v"
     */
    void assemble(const bool full_run);

private:
    const HeatSolver<dim>* heat_solver;
    
    typedef typename DealSolver<dim>::LinearSystem LinearSystem;
    typedef typename DealSolver<dim>::ScratchData ScratchData;
    typedef typename DealSolver<dim>::CopyData CopyData;

    // TODO figure out what is written
    void write_vtk(ofstream& out) const;

    /** Calculate the contribution of one cell into global matrix and rhs vector */
    void assemble_local_cell(const typename DoFHandler<dim>::active_cell_iterator &cell,
            ScratchData &scratch_data, CopyData &copy_data) const;

    friend class HeatSolver<dim>;
    friend class CurrentHeatSolver<dim> ;
};

template<int dim>
class HeatSolver : public BulkSolver<dim> {
public:
    HeatSolver(Triangulation<dim> *tria, const CurrentSolver<dim> *cs, const HeatData *pq,
            const Config::Heating *conf, const vector<double>* bc_values);

    /** Assemble the matrix equation for temperature calculation
     * using Crank-Nicolson or implicit Euler time integration method. */
    void assemble(const double delta_time);

    /** Initialize data vectors and matrices */
    void setup_system(bool axisymmetry = false);

private:
    // TODO shouldn't it be temperature dependent?
    static constexpr double cu_rho_cp = 3.4496e-24;  ///< volumetric heat capacity of copper [J/(K*Ang^3)]
    double one_over_delta_time;      ///< inverse of heat solver time step [1/sec]
    Vector<double> joule_heat;       ///< integral Joule heat at dofs [Watt]
    Vector<double> total_heat;       ///< integral Joule+Nottingham heat at dofs [Watt]
    const CurrentSolver<dim>* current_solver;

    /**Calculates Joule heat for exporting*/
    void calc_joule_heat();

    typedef typename DealSolver<dim>::LinearSystem LinearSystem;
    typedef typename DealSolver<dim>::ScratchData ScratchData;
    typedef typename DealSolver<dim>::CopyData CopyData;

    /** @brief assemble the matrix equation for temperature calculation using Crank-Nicolson time integration method
     * Calculate sparse matrix elements and right-hand-side vector
     * according to the time dependent heat equation weak formulation and to the boundary conditions.
     */
    void assemble_crank_nicolson(const double delta_time);

    /** @brief assemble the matrix equation for temperature calculation using implicit Euler time integration method
     * Calculate sparse matrix elements and right-hand-side vector
     * according to the time dependent heat equation weak formulation and to the boundary conditions.
     */
    void assemble_euler_implicit(const double delta_time);

    /** Calculate the contribution of one cell into global matrix and rhs vector */
    void assemble_local_cell(const typename DoFHandler<dim>::active_cell_iterator &cell,
            ScratchData &scratch_data, CopyData &copy_data) const;

    /** Output the temperature [K] and electrical conductivity [1/(Ohm*nm)] in vtk format */
    void write_vtk(ofstream& out) const;

    friend class CurrentSolver<dim>;
    friend class CurrentHeatSolver<dim> ;
};


template<int dim>
class CurrentHeatSolver : public DealSolver<dim> {
public:
    CurrentHeatSolver(const HeatData *pq, const Config::Heating *conf,
            const vector<double> *surface_heat, const vector<double> *current_densities);

    /** Obtain the temperature and current density values on mesh vertices */
    void export_temp_rho(vector<double> &temp, vector<Tensor<1,dim>> &rho) const;

    /** Read CH solver solution from file */
    void read(const string &file_name);

    /** Setup current and heat solvers */
    void setup(const double temperature, bool axisymmetry = false);

    /** Obtain number of degrees of freedom in solver */
    int size() const { return heat.size(); }

    /** Calculate current distribution and advance heat solver by @param dt */
    int run(double dt, bool full_run = false);

    HeatSolver<dim> heat;        ///< data and operations of heat equation solver
    CurrentSolver<dim> current;  ///< data and operations for finding current density in material

private:
    const HeatData *pq;
    const Config::Heating *conf;

    /** Specify allowed types of writable files */
    bool valid_extension(const string &ext) const {
        return ext == "xyz" || ext == "movie" || ext == "restart" || ext == "vtk" || ext == "vtks";
    }

    /** Write nodal data as xyz file */
    void write_xyz(ofstream& out) const;

    /** Write data as vtk file */
    void write_vtk(ofstream& out) const;

    /** Write restart file */
    void write_restart(ofstream &out) const;

    /** Mark different regions of the mesh */
    void mark_mesh();
};

} // namespace femocs

#endif /* CURRENTSANDHEATING_H_ */
