/*
 *  Created on: 27 Mar 2021
 *      Author: veske
 */

#ifndef FIELDSOLVER_H_
#define FIELDSOLVER_H_

#include "DealSolver.h"

#include <random>


namespace femocs {
using namespace dealii;
using namespace std;


/** Some forward declarations to minimize # included headers */
class Config;
class CircuitModel;


template<int dim>
class FieldSolver : public DealSolver<dim> {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename DoFHandler<dim>::active_face_iterator Face;

    FieldSolver(const Config& conf, unsigned int seed);
    ~FieldSolver();

    /** Setup system for solving Poisson equation */
    void setup(double field, double potential, bool axisymmetry=false, bool full=true);

    /** Assemble the laplace part of the equation (space_charge = 0) */
    void assemble_laplace(bool full_run);

    /** Prepare solution export and apply Dirichlet boundary conditions to the system */
    void assemble_finalize();

    /** Solve the matrix equation */
    void solve(bool full_run=true) { this->solve_umf(full_run); }

    /** Get the electric field in the location of real points */
    void probe_field(const vector<Point<dim>> &points,
            const vector<Cell> &cells, vector<Tensor<1,dim>> &fields) const;

    /** Calculate charge densities at mesh vertices */
    void export_charge_dens(vector<double> &charge_dens) const;

    /** Generate uniformly distributed random number from [0, 1] */
    double rnd() const { return uniform(mersenne); }

    BoundingBox<dim> bbox;    ///< Box surrounding the whole triangulation

    /** Random nr generator that maps Mersenne twister output uniformly into range [0.0 1.0] (both inclusive) */
    mutable uniform_real_distribution<double> uniform{0.0, 1.0};
    mutable mt19937 mersenne;        ///< Mersenne twister pseudo-random number engine

protected:
    const Config *conf;       ///< All configuration parameters
    CircuitModel* circuit;    ///< Electrical circuit machinery
    Vector<double> charge_density;   ///< charge density at dofs [e/Angstrom^3]

    /** Custom & Deal.II functionality for probing
     * the field in the location of points */
    void probe_field_fast(const vector<Point<dim>> &unit_points,
            const vector<Cell> &cells, vector<Tensor<1,dim>> &fields) const;

    /** Mark different regions in mesh */
    void mark_mesh();

    /** Return the boundary condition value at the centroid of face */
    double get_face_bc(const unsigned int face) const;

    /** Write the electric potential and field to a file in vtk format */
    void write_vtk(ofstream& out) const;

    /** Write nodal data as xyz file */
    void write_xyz(ofstream& out) const;

    /** Specify allowed types of writable files */
    bool valid_extension(const string &ext) const {
        if (ext == "xyz" || ext == "movie" || ext == "vtk" || ext == "vtks" || ext == "restart" || ext == "msh"){
            return 1;}
        else {
            return DealSolver<dim>::valid_extension(ext);
        };
    };
};

} /* namespace femocs */

#endif /* FIELDSOLVER_H_ */
