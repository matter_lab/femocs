/*
 * Collisions.h
 *
 *  Created on: 17 Jun 2020
 *      Author: koironi
 */

#ifndef COLLISIONS_H_
#define COLLISIONS_H_

#include "Config.h"
#include "Primitives.h"


using namespace dealii;
using namespace std;
namespace femocs {

/** Some forward declarations to minimize # includes in a header */
class PicData;
template<int dim> class PicSolver;
template<int dim> class ParticleSet;
template<int dim> class Neutrals;
template<int dim> class Electrons;
template<int dim> class Ions;


/** Class to perform collisions between electrons, neutrals and ions
 *
 *  This class performs plasma simulation using the PIC-MCC
 *  (particle-in-cell Monte Carlo Collisions) method, following previous work
 *  on the ArcPIC code [1]. The collisions implemented in this class are:
 *
 *  1. Coulomb collisions between charged particles, e.g.
 *       e- + e- -> e- + e-
 *  2. Elastic collisions between electrons and neutrals, e.g.
 *       Cu + e- -> Cu + e-
 *       Cu + Cu -> Cu + Cu
 *  3. Ionizations of both neutrals and ions any number of times, e.g.
 *       Cu + e- -> Cu+ + 2e-
 *       Cu+ + e- -> Cu2+ + 2e-
 *  4. Charge exchange collisions between neutrals and ions
 *       Cu+ + Cu -> Cu + Cu+
 *  5. Recombination collisions between ions and electrons
 *       Cu+ + e- -> Cu
 *
 *  Most of these collisions have energy-dependent cross sections which
 *  determine the probability of the collision occurring. Ionizations also have
 *  ionization energies associated with each level.
 *
 *  Collision routines are implemented based on the methods of Takizuka and
 *  Abe [2], as well as Matyash [3]. Superparticles in cells of the mesh are
 *  paired randomly, after which we determine whether each of these pairs
 *  undergoes collision.
 *
 *  Dynamic superparticle weighting is used enable collisions even when weights
 *  are not the same. Larger SPs can be split into smaller ones during
 *  collision, as well as merged to speed up the simulation. Splitting
 *  conserves energy and momentum, merging conserves only momentum. Specifics
 *  of this implementation are described in [4].
 *
 * [1] H. Timko, K. Ness Sjobak, L. Mether, S. Calatroni, F. Djurabekova, K. Matyash,
 * K. Nordlund, R. Schneider, and W. Wuensch. From field emission to vacuum arc
 * ignition: A new tool for simulating copper vacuum arcs. Contributions to Plasma
 * Physics, 55(4):299–314, 2015.
 * [2] Tomonor Takizuka and Hirotada Abe. A binary collision model for plasma sim-
 * ulation with a particle code. Journal of Computational Physics, 25(3):205–219,
 * 1977.
 * [3] Konstantin Matyash. Kinetic Modeling of Multi-component Edge Plasmas. PhD
 * thesis, University of Greifswald, 2003.
 * [4] Roni Koitermaa. Concurrent multi-scale modelling of vacuum arc plasma
 * initiation. Master's thesis, University of Helsinki, 2022.
 */

template<int dim>
class Collisions {
public:
    Collisions(PicSolver<dim> *solver, Neutrals<dim> &neutrals,
            Electrons<dim> &electrons, Ions<dim> &ions,
            const PicData &pic_data, const Config::PIC &conf);

    /** Invoke collision routines that are enabled in conf */
    void run(double timestep);

    /** Test if event with probability @param p occurs and split particle @param i weights */
    bool split_particle(int i, ParticleSet<dim> &particle_set, double p);
private:
    const double E_epsilon = 1.e-3;  ///< energy conservation tolerance
    const double p_epsilon = 1.e-3;  ///< momentum conservation tolerance

    const PicSolver<dim> *solver;
    const PicData *pic_data;
    const Config::PIC *conf;

    Neutrals<dim> &neutrals;
    Electrons<dim> &electrons;
    Ions<dim> &ions;

    Vec3 system_momentum;   ///< total momentum of all the superparticles
    double system_energy;   ///< total energy of all the superparticles
    double timestep;

    /** Number of collision events */
    unsigned long long elastic_num = 0;
    unsigned long long coulomb_num = 0;
    unsigned long long en_ionization_num = 0;
    unsigned long long ei_ionization_num = 0;
    unsigned long long exchange_num = 0;
    unsigned long long recombination_num = 0;

    /** Collision mode flags */
    enum CollisionMode {
        nn_elastic =    1 << 0,  ///< neutral-neutral elastic collision
        en_elastic =    1 << 1,  ///< electron-neutral elastic collision
        coulomb =       1 << 2,  ///< coulomb collisions
        en_ionization = 1 << 3,  ///< electron-neutral ionization collision
        ei_ionization = 1 << 4,  ///< electron-ion ionization collision
        exchange =      1 << 5,  ///< charge exchange collision
        recombination = 1 << 6   ///< recombination collision
    };

    /** Calculate whether two particles with relative velocity @param v_rel_norm collide with cross section @param cs */
    bool random_coll(double v_rel_norm, double variance_factor, double cs) const {
        return solver->rnd() < 1.0 - exp(-v_rel_norm * variance_factor * cs);
    }

    /** Count number of events occurring with probability @param p out of @param m */
    int random_count(double p, int m) const {
        int n = 0;
        for (int i = 0; i < m; ++i)
            n += (int) (solver->rnd() < p); // does event occur?
        return n; // number of events
    }

    /** Calculate number of colliding particles based on variance factor @param vf0 (v_rel * dt * density * cs) and weights @param w1 and @param w2 */
    double random_ncoll(double vf0, double w1, double w2, double weight_min) const {
        double wp = min(w1, w2); // projectile
        double wt = max(w1, w2); // target
        double nparts = ceil(wp / weight_min); // number of particles in projectile superparticle
        wp /= nparts; // weight of one projectile

        int n = random_count(1.0 - exp(-vf0 * wp * wt), (int) nparts); // count number of projectile collisions

        return (double) n * wp; // weight of particle produced in collision
    }

    /** Randomize the ordering of a vector of indices */
    void shuffle(vector<int> &indices) const;

    /** Randomly shuffle particle pairs @param parts1 and @param parts2 placing result in indices @param inds1 and @param inds2 */
    void group_shuffle(const ParticleSet<dim> &parts1, const ParticleSet<dim> &parts2,
            vector<vector<int>> &inds1, vector<vector<int>> &inds2) const;

    /** Get velocity difference by randomly rotating @param v_rel in relative velocity coordinate fram and scaling by @param w */
    Vec3 delta_v(const Vec3 &v_rel, double w = 1.0) const;
    Vec3 delta_v(const Vec3 &v_rel, double delta, double phi, double w) const;

    /** Uniformly distributed random velocity of magnitude @param u */
    Vec3 rand_v(double u) const;

    /** General collide functions for 1-2 collision particle sets */
    void collide(int mode, ParticleSet<dim> &particle_set);

    /** Collision where particles are injected into destination */
    void collide(int mode, ParticleSet<dim> &particle_set1, ParticleSet<dim> &particle_set2,
            ParticleSet<dim> *destination, int level);

    /** Perform pairwise elastic collision */
    void collide_pair_elastic(int p1, int p2, double density, double cs,
            ParticleSet<dim> &particle_set1, ParticleSet<dim> &particle_set2);

    /** Electron-neutral elastic collision */
    void collide_en_pair_elastic(int p1, int p2, double density);

    /** Neutral-neutral elastic collision */
    void collide_nn_pair_elastic(int p1, int p2, double density);

    /** Coulomb collision */
    void collide_pair_coulomb(int p1, int p2, double density,
            ParticleSet<dim> &particle_set1, ParticleSet<dim> &particle_set2);

    /** Perform pairwise ionization collision */
    bool collide_pair_ionize(int mode, int p1, int p2, double density,
            ParticleSet<dim> &set1, ParticleSet<dim> &set2, int level,
            Vec3 &v1, Vec3 &v2);

    /** Electron-ion ionization collision */
    void collide_ei_pair_ionize(int p1, int p2, double density,
            ParticleSet<dim> &ion, ParticleSet<dim> &ions, int level);

    /** Electron-neutral ionization collision */
    void collide_en_pair_ionize(int p1, int p2, double density,
            ParticleSet<dim> &ions, int level);

    /** Charge exchange collision */
    void collide_pair_exchange(int p1, int p2, double density,
            ParticleSet<dim> &ions);

    /** Radiative recombination collision */
    void collide_pair_recombine(int p1, int p2, double density,
            ParticleSet<dim> &ion, ParticleSet<dim> &ions, int level);

    /** Test if collision happens and split colliding and noncolliding particles based on weight */
    bool split_particles(int p1, int p2, double vf0,
            ParticleSet<dim> &particle_set1, ParticleSet<dim> &particle_set2);

    /** Merge superparticles pairwise if density of particle_set in cell is too large */
    void merge_particles(ParticleSet<dim> &particle_set);

    /** Measure conservation of energy & momentum within the superparticles */
    void check_energy_and_momentum(const string& type);
};

}  // namespace femocs

#endif /* COLLISIONS_H_ */
