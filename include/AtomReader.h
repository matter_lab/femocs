/*
 * AtomReader.h
 *
 *  Created on: 14.12.2015
 *      Author: veske
 */

#ifndef ATOMREADER_H_
#define ATOMREADER_H_

#include "Medium.h"
#include "Config.h"
#include "Primitives.h"
#include "Globals.h"

#include <iomanip>


using namespace std;
namespace femocs {

/** Some forward declarations to minimize # included headers */
class Surface;


/** Class to import atoms from atomistic simulation and to divide them into different categories */
class AtomReader: public Medium {
public:
    AtomReader();
    AtomReader(const Config *conf);

    /** Extract atom with desired types and clean it from lonely atoms;
     * atom is considered lonely if its coordination is lower than threshold.
     * @param surface  Surface where the extracted atoms will be written
     * @param type     type of the atoms that will be read
     * @param invert   if true all the atoms except the 'type'-ones will be stored
     */
    void extract(Surface& surface, const int type, const bool invert=false);

    /** Generate nanotip with high rotational symmetry and without crystallographic faceting */
    int generate_nanotip(double height, double radius, double latconst);

    /** Import atom coordinates and types from a file and check their rmsd
     * @param file_name  path to input file with atomic data
     * @param add_noise  add random noise to the imported atom coordinates to emulate real simulation
     * @return           nr of imported atoms
     */
    int import_file(const string &file_name, const bool add_noise=false);

    /** Add random noise to the atom coordinates */
    void import_noise();

    /** Import atom velocities from LAMMPS */
    bool import_lammps_velocities(int n_atoms, const double* vel);

    /** Import atom velocities from PARCAS */
    bool import_parcas_velocities(int n_atoms, const double* vel);

    /** Import atom velocities
      * @param n_atoms     number of imported atoms
      * @param vel         vector of atom velocities; vx0=vel[0], vy0=vel[1], vz0=vel[2], vx1=vel[3], etc
      * @param scale       scaling factor of velocities
      */
    bool import_velocities(int n_atoms, const double* vel, const Vec3 &scale);

    /** Import atom coordinates from LAMMPS */
    bool import_lammps_coordinates(int n_atoms, const double* xyz);

    /** Import atom coordinates from PARCAS */
    bool import_parcas_coordinates(int n_atoms, const double* xyz);

    /** Import atom coordinates and check their RMSD
      * @param n_atoms     number of imported atoms
      * @param xyz         vector of atom coordinates; x0=xyz[0], y0=xyz[1], z0=xyz[2], x1=xyz[3], etc
      * @param scale       scaling factor of coordinates
      */
    bool import_coordinates(int n_atoms, const double* xyz, const Vec3 &scale);

    /** Import atom coordinates and types and check their rmsd */
    bool import_atoms(const int n_atoms, const double* x, const double* y, const double* z, const int* types);

    /** Measure the RMS distance atoms have moved
     * and re-calculate neighboring data if RMSD >= threshold */
    bool analyse(const int* nborlist);

    /** Calculate coordination for all the atoms by using PARCAS neighbour list
     * or, if it's missing, building first the Verlet neighbour list */
    void calc_coordinations(const int* parcas_nborlist=NULL);

    /** Calculate coordination for all the atoms by using PARCAS neighbour list
     * or, if it's missing, building first the Verlet neighbour list.
     * Before doing so, update nnn, lattice constant and coordination cut-off radius
     * by calculating radial distribution function. */
    void calc_rdf_coordinations(const int* parcas_nborlist=NULL);

    /** Calculate pseudo-coordination for all the atoms using the atom types */
    void calc_pseudo_coordinations();

    /** Rebuild list of close neighbours and run cluster analysis.
     * Atoms are grouped into clusters using density-based spatial clustering technique
     * http://codereview.stackexchange.com/questions/23966/density-based-clustering-of-image-keypoints
     * https://en.wikipedia.org/wiki/DBSCAN */
    void calc_clusters(const int* parcas_nborlist=NULL);

    /** Extract atom types from calculated atom coordinations */
    void extract_types();

    /** Store the atom coordinates from current run */
    void save_current_run_points();

    /** Obtain rms-distance the atoms have moved since the last full iteration.
     * BIG values (>10^308) indicate that current and previous iterations are not comparable. */
    double get_rmsd() const { return data.rms_distance; }

    /** Return number of atom that are detached from the big system */
    int get_n_detached() const { return data.n_detached; }

    /** Return factors to covert SI units to Parcas ones */
    void set_parcas_box(const double* box);

    /** Return factors to covert SI units to Parcas ones */
    Vec3 get_si2parcas_box() const;

    /** Return factors to covert Parcas units to SI ones */
    Vec3 get_parcas2si_box() const;

    /** Return pointer to atomistic velocities */
    const vector<Vec3>* get_velocities() const { return &velocities; }

    /** Obtain the printable statistics */
    friend ostream& operator <<(ostream &os, const AtomReader& ar) {
        os << "time=" << GLOBALS.TIME << fixed << setprecision(3)
                << ", #evap=" << ar.data.n_evaporated << ", #clust=" << ar.data.n_detached;
        return os;
    }

private:
    vector<Vec3> velocities;        ///< atomistic velocities [A/fs]
    vector<int> cluster;            ///< id of cluster the atom is located
    vector<int> coordination;       ///< coordinations of atoms
    vector<int> previous_types;     ///< atom types from previous run
    vector<Point3> previous_points; ///< atom coordinates from previous run
    vector<vector<int>> nborlist;   ///< list of closest neighbours
    Vec3 simubox;                   ///< MD simulation box dimensions; needed to convert SI units to Parcas one

    const Config *conf;                ///< data from configuration file
    const Config::Geometry *geometry;  ///< geometry data from configuration file

    struct Data {
        double rms_distance=0;        ///< rms distance between atoms from previous and current run
        double coord_cutoff=0;        ///< RDF-modified coordination analysis cut-off radius
        double latconst=0;            ///< RDF-modified lattice constant
        unsigned int n_detached=0;    ///< number of atoms that are detached from the big structure
        unsigned int n_evaporated=0;  ///< number of atoms that are evaporated from the big structure
    } data;

    /** Import atoms from different types of file.
     * @param file_name - path to file with atomic data
     */
    void import_xyz(const string& file_name);
    void import_ckx(const string& file_name);

    /** Output atom data in .ckx format that shows atom coordinates and their types (fixed, surface, bulk etc.) */
    void write_ckx(ofstream &outfile) const;

    /** Write restart file for PARCAS */
    void write_restart(ofstream &out) const;

    /** Specify file types that can be written */
    bool valid_extension(const string &ext) const {
        return Medium::valid_extension(ext) || ext == "ckx" || ext == "restart";
    }

    /** Reserve memory for data vectors */
    void reserve(const int n_atoms);

    /** Get i-th entry from all data vectors; i < 0 gives the header of data vectors */
    string get_data_string(const int i) const;

    /** Calculate list of close neighbours using Parcas diagonal neighbour list */
    void calc_nborlist(const double r_cut, const int* parcas_nborlist);

    /** Calculate list of close neighbours using already existing list with >= cut-off radius */
    void recalc_nborlist(const double r_cut);

    /** Calculate the radial distribution function (rdf) in a periodic isotropic system.
     *  Source of inspiration: https://github.com/anyuzx/rdf
     *  Author: Guang Shi, Mihkel Veske
    */
    void calc_rdf(const int n_bins, const double r_cut);

    /** Calculate the root mean square average distance the atoms have moved
     * between previous and current run */
    bool calc_rms_distance();
};

} /* namespace femocs */

#endif /* ATOMREADER_H_ */
