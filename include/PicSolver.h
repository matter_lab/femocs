/*
 *  Created on: 19 Nov 2019
 *      Author: kyritsak
 */

#ifndef PICSOLVER_H_
#define PICSOLVER_H_

#include "FieldSolver.h"
#include "ParticleSet.h"
#include "PicData.h"
#include "Collisions.h"


namespace femocs {
using namespace dealii;
using namespace std;


/** Some forward declarations to minimize # included headers */
class Config;
class FieldReader;
class Interpolator;
template<int dim> class Emission;


template <int dim>
class PicSolver : public FieldSolver<dim> {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename DoFHandler<dim>::active_face_iterator Face;
    typedef typename vector<Cell>::iterator CellIterator;


    PicSolver(Emission<dim> *emission, const Config &conf, unsigned int seed);

    /** Read superparticle-related data from a file */
    void read(const string &file_name);

    /** Calculate the total charge in the rhs */
    double total_charge() const;

    /** Calculate total energy of all the superparticles in the system */
    double total_energy() const;

    /** Calculate total momentum of all the superparticles in the system */
    Vec3 total_momentum() const;

    /** Save field with no particles */
    void save_laplace();

    /** Run for advance_time */
    int run(double advance_time, bool first_run = false);

    /** Runs one iteration of pic */
    int run_step(double delta_t, bool full_run = true);

    /** Run one iteration of PIC using only electron superparticles.
     * In case the user does not want to use field reader or interpolator,
     * one can pass NULL and their usage is omitted. */
    int step_electrons(double delta_t, bool full_run, FieldReader *reader, Interpolator *interpolator);

    /** Runs one iteration of pic for steady state SC calculations. */
    double run_steady_iteration(double theta, bool full_run);

    using FieldSolver<dim>::probe_field;  // to prevent name conflict

private:
    const Config::PIC *pic_conf;        ///< PIC configuration parameters
    PicData pic_data;                   ///< Object containing collision cross section data
    Electrons<dim> electron_table;      ///< Table of electrons read from file

    Electrons<dim> electrons;   ///< electron superparticles
    Neutrals<dim> neutrals;     ///< neutral superparticles
    Ions<dim> ions;             ///< ion superparticles of increasing ionization levels

    Emission<dim> *emission;            ///< object to obtain the field emission data
    Collisions<dim> collisions;         ///< PIC collisions for different particle types

    Vector<double> laplace_solution;    ///< Solution of Laplace field
    Vector<double> system_rhs_save;     ///< right-hand-side of the previous iteration

    double timestep;                    ///< Timestep [fs]
    double cathode_deposited_charge;    ///< Accumulated charge on cathode by impacting ions
    long long atoms_lost;
    unsigned long long n_fionized = 0; ///< Total number field ionization events
    double n_evaporated = 0; ///< Total number of neutrals produced by evaporation
    double n_sputtered = 0; ///< Total number of neutrals produced by sputtering
    double n_bombarded = 0; ///< Total number of ions bombarded on cathode

    Vec3 system_momentum;   ///< total momentum of all the superparticles
    double system_energy;   ///< total energy of all the superparticles

    /** Data for coping local matrix & rhs into global one during parallel assembly */
    struct CopyData {
        Vector<double> cell_rhs;
        vector<unsigned int> dof_indices;
        CopyData(unsigned int dofs_per_cell) :
            cell_rhs(dofs_per_cell), dof_indices(dofs_per_cell)
        {}
    };

    /** Dummy ScratchData to be input in WorkStream.
     * Could not be omitted as WorkStream::run is expecting it. */
    struct ScratchData {};

    /** Ionize neutrals into ions via direct field ionization */
    int field_ionization(double delta_t, int level = 1);

    /** Calc */
    void calculate_number_density(const ParticleSet<dim> &particles, Vector<double> &density);

    /** Add particles charge to the rhs of the solver */
    void assemble_space_charge(const ParticleSet<dim> &particles);
    void assemble_space_charge_serial(const ParticleSet<dim> &particles);

    /** Assembles the RHS of a given cell.*/
    void assemble_cell(const CellIterator &cell_it, const ScratchData &sd,
            CopyData &copy_data, const ParticleSet<dim> &particles) const;

    /** Optimized assembler of the RHS of a given cell.*/
    void assemble_cell_fast(const Cell &cell, CopyData &copy_data, const ParticleSet<dim> &particles) const;

    /** Calculate number densities of each particle species in each cell */
    void calc_number_densities();
    void calc_number_density(const ParticleSet<dim> &particles, vector<double> &density);

    /** Copies the rhs of a local cell to the global system_rhs */
    void copy_cell(const CopyData &copy_data, Vector<double> &system_rhs) const;

    /** Set static superparticle magnetic field */
    void set_magnetic_field(ParticleSet<dim> &particles) const;

    /** Probe the field in the location of superparticles */
    void probe_field(ParticleSet<dim> &particles) const;

    /** Probe the Laplace field (field without space-charge)
     * in the location of superparticles */
    void probe_laplace_field(ChargedParticles<dim> &particles) const;

    /** Probe the field in the location of superparticles that are located in the cell */
    void probe_cell(const CellIterator &cell_it, CopyData &copy_data,
            const ParticleSet<dim> &particles, vector<Tensor<1,dim>> &fields,
            const Vector<double> &solution) const;

    /** Optimized in-cell field prober */
    void probe_cell_fast(const Cell &cell, CopyData &copy_data,
            const ParticleSet<dim> &particles, vector<Tensor<1,dim>> &fields,
            const Vector<double> &solution) const;

    /** Empty function for field prober */
    void dummy_copier() const {}

    /** Update new rhs with (1-theta) * old + theta * new.
     * Returns the error in a form of (new - old).norm() */
    double update_rhs(double theta);

    /** Measure conservation of energy within the superparticles */
    double measure_energy();

    /** Measure conservation of momentum within the superparticles */
    double measure_momentum();

    /** Write particles into a restart file */
    void write_restart(ofstream &out) const;

    /** Write nodal data as xyz file */
    void write_xyz(ofstream& out) const;

    /** Write nodal data as vtk file */
    void write_vtk(ofstream& out) const;
};

} /* namespace femocs */

#endif /* PICSOLVER_H_ */
