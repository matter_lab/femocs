/*
 * physical_quantities.h -> PhysicalQuantities.h
 *
 *  Created on: Apr 30, 2016
 *      Author: kristjan, Andreas
 */

#ifndef PHYSICALQUANTITIES_H_
#define PHYSICALQUANTITIES_H_

#include <vector>
#include <string>
#include <utility>
#include <algorithm>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstdio>
#include <iostream>

namespace femocs {

using namespace std;

/** @brief Evaluation tools for reading tabulated data.  */
class PhysicalQuantities {
public:

	/** The default constructor. */
    PhysicalQuantities(){}

protected:

    /**
     * A data structure to hold the uniform grid information,
     * used in bilinear interpolation
     * Holds 2d information in a 1d vector: to access element v[i][j], use v[i*ynum+j]
     */
    struct InterpolationGrid {
        std::vector<double> v;
        double xmin = 0, xmax = 0, ymin = 0, ymax = 0;
        unsigned xnum = 0, ynum = 0;
    };

    /** Read several columns from input file. No sizes specified. Each column in the input
     * file is read in a vector. The result is a vector of (column) vectors.  */
    int read_columns(vector<vector<double>> &data, string filename);

    /** Read a row of data from input file.  */
    int read_row(vector<double> &data, string filename);

    /** Read pair data from input file. Return 0 if file is not present.
     * Size of data arrays in the first line is present */
    int read_pair(vector<pair<double, double>> &pair, string filename);

    /** Read pair data from input file. Size of data arrays is unknown. Return 0 if file not present. */
    int read_pair_nosize(vector<pair<double, double>> &pair, string filename);

    /** 1D linear interpolation with constant extrapolation using binary search. */
    double linear_interp(double x, vector<pair<double, double>> data) const;

    /** 1D linear interpolation with constant extrapolation using binary search. */
    double linear_interp(double x, vector<double> &xdata, vector<double> &ydata) const;

    /** 1D logarithmic interpolation with constant extrapolation using binary search.  */
    double loglog_interp(double x, vector<double> &xdata, vector<double> &ydata) const;

    /**
     * 1d linear interpolation of the derivative with constant extrapolation using binary search
     * derivative is approximated with central differences (one sided at ends)
     *
     * NB: Derivative is extrapolated by boundary values; out of bounds the real derivative should be zero!
     */
    double deriv_linear_interp(double x, std::vector<std::pair<double, double>> data) const;

    double evaluate_derivative(std::vector<std::pair<double, double>> &data,
            std::vector<std::pair<double, double>>::iterator it) const;

    /** 2D bilinear interpolation with constant extrapolation. Assumes uniform grid */
    double bilinear_interp(double x, double y, const InterpolationGrid &grid_data) const;

    bool load_spreadsheet_grid_data(std::string filepath, InterpolationGrid &grid);

    bool load_compact_grid_data(std::string filepath, InterpolationGrid &grid);

};

} // namespace femocs

#endif /* PHYSICALQUANTITIES_H_ */
