/*
 *  Created on: 9.2.2021
 *      Author: veske
 */

#ifndef DEALUTILITIES_H_
#define DEALUTILITIES_H_

#include <deal.II/hp/fe_values.h>
#include <deal.II/fe/fe_q.h>


using namespace dealii;
using namespace std;

namespace femocs {


/** @brief Tools for using Deal.II based FEM solver
 */
template<int dim>
class DealUtilities {
public:
    DealUtilities(const Triangulation<dim> *tria, const FE_Q<dim>& fe,
            const DoFHandler<dim> &dof_handler, const Vector<double> &solution);
    
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename DoFHandler<dim>::active_face_iterator Face;

    /** Pre-calculate some data to make unit cell mapping faster */
    void precompute();

    /** Check whether the @param point lies in @param cell. Outputs point in reference cell @param ref_point.
     * @return 0 if point found in, 1 if point lies out of cell. */
    bool check_cell(const Point<dim> &point, Point<dim> &ref_point, Cell &cell, double tol = 1.e-10) const;

    /** Check whether a point lies inside a face of a cell */
    bool point_inside_face(const Tensor<1,dim> &point, const Cell &cell, int face, double tol = 1.e-5) const;

    /** Get injection face normal */
    void get_face_normal(const Cell &cell, int face_index, Tensor<1, dim> &face_normal) const;

    /** Get the solution at the specified point of the unit cell */
    double probe_solution_unit(const Point<dim> &point, Cell cell) const;
    double probe_solution_native(const Point<dim> &point, Cell cell) const;

    /** Get the solution value at the specified point given a known cell */
    double probe_solution(const Point<dim> &point, Cell cell) const;

    /** Transform point from real coordinates (x,y,z) to the natural ones (u,v,w) */
    Point<dim> project_to_nat_coords(const Cell& cell, const Point<dim>& point) const;
    Point<dim> project_to_nat_coords_native(const Cell& cell, const Point<dim>& point) const;

    vector<double> shape_funs(const Point<dim>& upoint, const Cell& cell) const;

    vector<double> shape_funs_native(const Point<dim> &upoint, const Cell& cell) const;

    vector<Tensor<1,dim>> shape_fun_grads(const Point<dim>& upoint, const Cell& cell) const;

    vector<Tensor<1,dim>> shape_fun_grads_native(const Point<dim> &upoint, const Cell &cell) const;

    /** Locate the cell where the @param point lies. It can use a cell initial guess
     * for performance.
     * @param point input : point to be located
     * @param ref_point output : the point in the unit cell
     * @param cell : input the cell guess (give dof_handler.end() if unknown)
     * @return integer indicating the status:
     *               -1: point out of triangulation.
     *              0: found after using general DealII search
     *              other N > 0: found after N cell searches (including current cell)
     */
    int locate_cell(const Point<dim> &point, Point<dim> &ref_point, Cell &cell) const;


    /**Locate the cell where the @param point lies. The previous cell and the
     * displacement of the point must be known. The function checks only cells
     * that lie in the path of the displacement vector.
     * @param point input : point to be located
     * @param ref_point output : the point in the unit cell
     * @param cell : previous cell of the particle before displacement
     * @param displacement : point - previous point
     * @return integer indicating the status:
     *              < 0: point out of triangulation. The negative of the boundary ID of the crossed face is returned
     *              0: found in initial guess cell
     *              1: found in 1st nearest neighbor cell
     *              2: found in 2nd nearest neighbor cell
     *              3: found after full search with Deal.II tools
     * @param cross_face : output the face index (in the @param cell) of the face
     * crossed. If no face crossed is -1. If face crossing method fails is -2.
     */
    int locate_cell(const Point<dim> &point, Point<dim> &ref_point, Cell &cell,
            const Tensor<1,dim>& displacement, int &cross_face = -1) const;

protected:
    const Triangulation<dim>* tria;         ///< external triangulation
    const FE_Q<dim>& fe;                    ///< finite element handler
    const DoFHandler<dim>& dof_handler;     ///< external dof handler
    const Vector<double>& solution;         ///< solution of the FEM solver

    /** Check whether the line segment from p0 to p1 crosses the f-th face of the cell */
    bool line_cross_face(const Tensor<1,dim> &p0, const Tensor<1,dim> &p1, Cell cell, int f, double tol = 1.e-10) const;

    double determinant(const Tensor<1,dim> &v1, const Tensor<1,dim> &v2) const;
    double determinant(const Tensor<1,dim> &v1, const Tensor<1,dim> &v2, const Tensor<1,dim> &v3) const;

    vector<Tensor<1,dim>> shape_fun_grads(const vector<Point<dim>> &xyz,
            const vector<Tensor<1,dim>> &dN) const;

    struct Vector3 {
        Vector3() {}
        Vector3(double x, double y, double z) : x(x), y(y), z(z) {}
        double dot(const Point<2> &p) const { return x*p[0] + y*p[1] + z; }
        double x, y, z;
    };

    /// data for mapping point from 2D Cartesian coordinates to natural ones
    vector<array<double, 5>> detA;
    vector<Vector3> detB;
    vector<Vector3> detC;

    /// data for mapping point from 3D Cartesian coordinates to natural ones
    vector<Tensor<1,dim>> f0s;
    vector<Tensor<1,dim>> f1s;
    vector<Tensor<1,dim>> f2s;
    vector<Tensor<1,dim>> f3s;
    vector<Tensor<1,dim>> f4s;
    vector<Tensor<1,dim>> f5s;
    vector<Tensor<1,dim>> f6s;
    vector<Tensor<1,dim>> f7s;

    vector<BoundingBox<dim>> bounding_box; ///< bounding box around each cell
};

} /* namespace femocs */

#endif /* DEALUTILITIES_H_ */
