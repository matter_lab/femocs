/*
 * ParticleSet.h
 *
 *  Created on: May 26, 2020
 *      Author: kyritsak
 */

#ifndef PARTICLESET_H_
#define PARTICLESET_H_

#include "FileWriter.h"
#include "Config.h"
#include "Primitives.h"

#include <deal.II/dofs/dof_handler.h>
#include <random>


using namespace dealii;
using namespace std;
namespace femocs {


/** Some forward declarations to minimize # includes in a header */
template<int dim> class Emission;
template<int dim> class FieldSolver;
class Interpolator;
class PicData;


/** A single super particle for PIC calculations */
template <int dim>
class Particle {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;

    Particle() : weight(1.0), cell_ind(0) {}
    Particle(const Point<dim> &p, const Vec3 &v, const Cell &cell, double Wsp) :
                pos(p), vel(v), cell(cell), weight(Wsp), cell_ind(cell->index()) {}

    /** Define the behaviour of string stream */
    friend std::ostream& operator <<(std::ostream &s, const Particle<dim> &p) {
        return s << p.pos << " " <<  p.vel << " " << p.cell->index() << " " << p.weight;
    }

    Point<dim> pos;        ///< particle position [Å]
    Vec3 vel;              ///< particle velocity [Å/fs]
    Cell cell;             ///< cell where the particle is located
    double weight;         ///< SuperParticle weight
    int cell_ind;          ///< particle cell index
};


/** Common data and routines for all the super particles */
template <int dim>
class ParticleSet : public FileWriter {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename DoFHandler<dim>::active_face_iterator Face;
    typedef typename vector<Point<dim>>::iterator PointIterator;


    ParticleSet(double mass0, double charge0, double Wsp,
            const FieldSolver<dim> *solver, const string &label);
    virtual ~ParticleSet() {}

    /** Update all particle positions using multiple threads */
    int update_positions(double delta_t, bool clear=true);

    /** Resize all arrays to size N */
    virtual void resize(int N);

    /** Clear all arrays */
    virtual void clear();

    /** Inject a new particle in the system */
    virtual void inject(const Particle<dim> &sp);

    /** Delete i-th particle that is out of the triangulation. */
    void clear_lost(int i);

    /** Delete all the particles that are out of the triangulation. */
    int clear_lost();

    /** Read particle data from file */
    void read(const string &filename);

    /** Updates the pic structure of the particle i that has been just moved */
    int update_cell(int i, double dt = 0);

    /** Get label for restart file */
    string get_restart_label() const { return label; }

    /** Get total superparticle charge */
    double get_Csp() const { return Wsp * charge0; }

    /** Get superparticle i weight */
    double get_Wsp(int i) const { return weights[i]; }

    /** Get superparticle i mass */
    double get_Msp(int i) const { return weights[i] * mass0; }

    /** Get superparticle i charge */
    double get_Csp(int i) const { return weights[i] * charge0; }

    /** Get number of actual particles (sum of SP weights) */
    double get_num() const {
        double num = 0.;

        for (auto &w : weights) {
            num += w;
        }

        return num;
    }

    /** Get total charge of all actual particles */
    double charge_tot() const {
        double charge = 0.;

        for (auto &w : weights) {
            charge += w * charge0;
        }

        return charge;
    }

    /** Calculate number density of superparticles in cell i in vacuum */
    double calc_number_density(const Cell &cell) const;

    /** Calculate total energy of the particles */
    double calc_energy() const;

    /** Calculate total momentum of the particles */
    Vec3 calc_momentum() const;

    /** Return indices of particles that are located in an argument cell */
    vector<int>& parts_at(const Cell &cell) { return cell2parts[cell->index()]; }
    vector<int> parts_at(const Cell &cell) const { return cell2parts[cell->index()]; }

    /** Remove random ordering of particles before shuffling them */
    void sort();

    /** Group particles by the cell that surrounds them */
    void regroup_all();

    /** Set the boundary box and the reflection options */
    void set_reflection(int refl) { reflection = refl; }

    /** Specify interpolator to make cell search faster */
    void set_interpolator(const Interpolator* i) { interpolator = i; }

    /** Get the number of particles */
    int size() const { return positions.size(); }

    /** Accessor for accessing the i-th SP */
    Particle<dim> operator [](const size_t i) const;

    using FileWriter::write_restart;    // make write_restart public

    vector<Cell> filled_cells;          ///< cells that have at least 1 superparticle inside
    vector<Point<dim>> positions;       ///< particle position [Å]
    vector<Point<dim>> unit_positions;  ///< position in the reference cell [dimensionless]
    vector<Vec3> velocities;            ///< particle 3D velocity [Å/fs]
    vector<Tensor<1,dim>> fields;       ///< electric fields on particles [V/Å]
    vector<Vec3> magnetic_fields;       ///< magnetic fields on particles [V fs/Å^2] = [1e5 T]
    vector<double> weights;             ///< SuperParticle weights; if 0, particle is marked as out of the mesh
    vector<Cell> cells;                 ///< Cells where each particle lies

    vector<double> ndensity; ///< Actual particle number density at dofs [Å^-3]

    const double q_over_m;      ///< SP charge / its mass [A^2 / (V fs^2)]
    const double q_over_eps0;   ///< (whole) particle charge / eps0 [e/VÅ]
    const double mass0;         ///< mass of a single particle
    const double charge0;       ///< charge of a single particle

protected:
    const string label;         ///< label for restart file
    const FieldSolver<dim> *solver;    ///< Poisson equation solver in the vacuum mesh
    const Interpolator* interpolator;  ///< some custom functions to speed up solution extraction

    double Wsp;                 ///< SP weight [particles/superparticle]
    int reflection;             ///< Parameter defining the reflection properties
    vector<vector<int>> cell2parts;    ///< particle indices that are located in i-th cell

    struct CopyData {
        int index;
        Cell new_cell;
        int searches;
        int face; ///< the face crossed
        CopyData(int i, const Cell &cell) :
            index(i), new_cell(cell), searches(0), face(-1) {}
        CopyData() : index(0), searches(0), new_cell(), face(-1) {}
    };

    /** Dummy ScratchData to be input in WorkStream.
     * Could not be omitted as WorkStream::run is expecting it. */
    struct ScratchData {};

    /** Rotate coordinate system around y-axis until z-coordinate becomes 0 */
    void rotate_2d3v(double delta_t);

    /** Deletes the i-th particle from the cell->particles map */
    void erase_map_entry(int id);

    /** Adds the i-th particle into the cell->particles map */
    void add_map_entry(int id);

    /** Finds the new cell of the particle that has been moved. Returns number of cell searches */
    int find_new_cell(int i, double dt, Cell &new_cell, int &face);

    /** Update position of a single particle running in its own thread */
    void update_position_parallel(const PointIterator &posit,
            const ScratchData &sd, CopyData &cd, double dt);

    /** Transfers data from parallel threads to the main one */
    virtual void copier(const CopyData &cd, int &total_searches) {};

    /** Copies particle from index @param source to index @param dest.
     * Particle in @param dest is lost */
    void relocate_particle(int source, int dest);

    /** Apply reflective boundaries to i-th particle */
    int apply_reflective(int i);

    void update_pcd(int i, const Cell &new_cell);

    /** Generate set of points with an uniform distribution inside a quadrangle */
    void rnd_points_on_face(const Cell &cell, int face, vector<Point<dim>> &points) const;

    /** Get a uniformly distributed random unit vector pointing towards the half-space
     * defined by @param face_normal. If @ face_normal = 0 (default value), the function returns
     * random unit vector in any direction. @param tol gives the minimum component of the velocity
     * parallel to @param face_normal in order to be acceptable. Default tol = 1.e-5 */
    Vec3 rnd_direction(const Tensor<1,dim>& face_normal, double tol = 1.e-5) const;

    /** Write the particle data into xyz file */
    void write_xyz(ofstream &out) const;

    /** Write the particle data into restart file */
    void write_bin(ofstream &out) const;

    /** Specify file types that can be written */
    bool valid_extension(const string &ext) const {
        return ext == "xyz" || ext == "movie" || ext == "restart";
    }
};


template<int dim> class Ions;
/** Data and routines for neutral superparticles */
template <int dim>
class Neutrals : public ParticleSet<dim> {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename DoFHandler<dim>::active_face_iterator Face;
    typedef typename ParticleSet<dim>::CopyData CopyData;


    Neutrals(double mass0, double Wsp, const FieldSolver<dim> *solver);

    /** Inject a set of new neutrals to the system */
    using ParticleSet<dim>::inject;
    double inject(const Emission<dim> &emission, double delta_t, const Config::PIC &conf);
    double inject_sput(const Emission<dim> &emission, double delta_t, const Config::PIC &conf, const Ions<dim> &ions);
    double inject_face(const Emission<dim> &emission, int i, double delta_t, const Config::PIC &conf, const double vnum, const double E0);

private:
    /** Move all the data from source to dest index */
    void relocate_particle(int source, int dest);

    /** Transfers data from parallel threads to the main one */
    void copier(const CopyData &cd, int &total_searches);

    /** Samples the velocity from the Maxwell-Boltzmann distribution with
     * temperature @param T and for a particle of mass @param m  */
    double velocity(double m, double T) const;
};


/** Data and routines for charged superparticles (both electrons and ions) */
template <int dim>
class ChargedParticles : public ParticleSet<dim> {
public:
    ChargedParticles(double mass0, double charge0, double Wsp,
            const FieldSolver<dim> *solver, const string &label);

    /** Resize all arrays to size N */
    void resize(int N);

    /** Clear all arrays */
    void clear();

    /** Inject a new particle to the system */
    void inject(const Particle<dim> &sp);

    /** Update all particle positions using multiple threads */
    int update_positions(double delta_t, bool clear=true);

    /** Update all velocities */
    void update_velocities(double delta_t);

    double current;                   ///< current via Shockley-Ramo
    vector<Tensor<1,dim>> lfields;    ///< Laplace fields / potential on particles [V/Å]

private:
    /** Move all the data from source to dest index */
    void relocate_particle(int source, int dest);
};


/** Data and routines for electron superparticles */
template <int dim>
class Electrons : public ChargedParticles<dim> {
public:
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;
    typedef typename ParticleSet<dim>::CopyData CopyData;


    Electrons(double Wsp, const FieldSolver<dim> *solver);

    /** Update all particle positions using only one thread */
    int update_positions_serial(double delta_t, bool clear=true);

    /** Inject new electrons to the system according to the field emission on the surface */
    using ChargedParticles<dim>::inject;
    double inject(const Emission<dim> &emission, double delta_t, const Config::PIC &conf);

    /** Inject 1 electron at each surface centroid.
     * Weights are calculated according to the field emission current. */
    double inject_paths(const Emission<dim> &emission, double delta_t);

    /** Inject electrons according to an external distribution table */
    double inject_table(const Electrons &table, double delta_t, const Config::PIC &conf);

    /** Read particles from external table */
    int read_table(const string &filepath);

private:
    vector<double> injection_times;     ///< Times to inject electrons from table
    int inj_index = 0;                  ///< index of the last particle injected from table

    /** Transfers data from parallel threads to the main one */
    void copier(const CopyData &cd, int &total_searches);
};


template<int dim> class Ions;


/** Data and routines for ions of a specific ionization level */
template <int dim>
class IonSet : public ChargedParticles<dim> {
public:
    typedef typename ParticleSet<dim>::CopyData CopyData;

    IonSet(double mass0, int ionization, double Wsp,
            const FieldSolver<dim> *solver, Ions<dim> *ions);

private:
    Ions<dim> &ions;  ///< common class of for all the ions

    /** Transfers data from parallel threads to the main one */
    void copier(const CopyData &cd, int &total_searches);
};


/** Data and routines for ions of various ionization levels */
template <int dim>
class Ions {
public:
    typedef typename DoFHandler<dim>::active_face_iterator Face;

    Ions(double mass0, double Wsp, const PicData *pic_data, const FieldSolver<dim> *solver);

    void write(const string &file_name, unsigned int flags=0);

    /** Calculate total energy for all the ions */
    double calc_energy() const;

    /** Calculate total momentum for all the ions */
    Vec3 calc_momentum() const;

    /** Calculate sputtering yield and energy */
    double calc_sput(double E0, double weight);

    /** Accessor for accessing the ions of (i+1)-th ionization level */
    IonSet<dim>& operator [](const size_t i);
    const IonSet<dim>& operator [](const size_t i) const;

    /** Constant iterator to access the leveled ions without modifying them */
    typedef Iterator<Ions<dim>, IonSet<dim>> c_iterator;
    c_iterator begin() const { return c_iterator(this, 0); }
    c_iterator end() const { return c_iterator(this, ions.size()); }

    /** Iterator to access the leveled ions */
    typedef VariableIterator<Ions<dim>, IonSet<dim>> iterator;
    iterator begin() { return iterator(this, 0); }
    iterator end() { return iterator(this, ions.size()); }

    /** Get number of ion species */
    int size() const { return ions.size(); }

    double cathode_deposited_charge;  ///< current caused by bombarding ion
    vector<double> bombard_energies;  ///< energies deposited in the corresponding material faces
    vector<Face> bombard_faces;       ///< faces where particles escape to the material interface
    vector<Face> sput_faces;       ///< faces where sputtering occurs
    vector<double> sput_energies;  ///< energy/atom of sputtered neutrals at each face
    vector<double> sput_yields;       ///< total sputtering yields of neutrals at each face

private:
    const PicData *pic_data;
    vector<IonSet<dim>> ions;
};

} /* namespace femocs */

#endif /* PARTICLESET_H_ */
