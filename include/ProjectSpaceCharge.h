/*
 *  Created on: 8 Nov 2019
 *      Author: kyritsak
 */

#ifndef INCLUDE_PROJECTSPACECHARGE_H_
#define INCLUDE_PROJECTSPACECHARGE_H_


#include "GeneralProject.h"
#include "HeatData.h"

using namespace std;

namespace femocs {

/** Some forward declarations to minimize # included headers */
template<int dim> class PicSolver;
template<int dim> class CurrentHeatSolver;
template<int dim> class Emission;


template<int dim>
class SpaceChargeRunner {
public:
    SpaceChargeRunner(Config &config);
    virtual ~SpaceChargeRunner() {};

    virtual int run() = 0;

protected:
    double theta_picard;        ///< Update factor of the Picard stationary point iteration

    Config &conf;               ///< Configuration parameters
    HeatData phys_quantities;
    PicSolver<dim> pic;
    Emission<dim> emission;
    CurrentHeatSolver<dim> ch_solver;

    int run_steady_state(bool reset);

    /** Run PIC until injection current converges */
    int converge();

    void write_emission(const string &file_name, const string &label, double factor, bool first_line);
};


template <int dim>
class SpaceChargeRamper : public SpaceChargeRunner<dim> {
public:
    SpaceChargeRamper(Config &config);
    int run();

private:
    double t0;
    double deff;
    vector<double> I_pic, I_sc, Vappl, Flap;

    void get_Isc_pic();

    double find_deff();

    void get_Isc_cepd(bool write_file = false);

    double get_current_error() const;

    void write_SC();

    void write_cepd(double Vappl, bool first_line);

    void write_converged(double Vappl, bool first_line);
};


template <int dim>
class SpaceChargeLimitedEmission : public SpaceChargeRunner<dim> {
public:
    SpaceChargeLimitedEmission(Config &config);
    int run();

private:
};


template <int dim>
class ProjectSpaceCharge : public GeneralProject {
public:
    ProjectSpaceCharge(AtomReader &reader, Config &config);
    ~ProjectSpaceCharge();

    int run(const int timestep=-1, const double time=-1);

    int export_data(double* data, const int n_points, const string& data_type) { return 0; }

    int export_data(int* data, const int n_points, const string& data_type) { return 0; }

    /** Interpolate the solution data in the location of specified points */
    int interpolate(double* data, int* flag, const int n_points,
            const string& data_type, const bool near_surface,
            const double* x, const double* y, const double* z) { return 0; }

    /** Read and generate simulation data to continue running interrupted simulation */
    int restart(const string& path_to_file) { return 0; }

private:
    SpaceChargeRunner<dim> *runner;  ///< place where the actual job is done
};


}  // namespace femocs

#endif /* INCLUDE_PROJECTSPACECHARGE_H_ */
