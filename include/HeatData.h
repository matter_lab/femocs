/*
 * HeatData.h
 *
 *  Created on: Jun 22, 2020
 *      Author: kyritsak
 */


#ifndef HEATDATA_H_
#define HEATDATA_H_

#include "PhysicalQuantities.h"
#include "Config.h"

namespace femocs {

class HeatData: public PhysicalQuantities {
public:
	HeatData(const Config::Heating& c) : config(c) {};
	virtual ~HeatData(){}

    /**
     * Load copper resistivity data from file
     * @return true if successful, false otherwise
     */
    bool load_resistivity_data(std::string filepath);

    /**
     * Method to read the tabulated resistivity data from file.
     * If file not found, hard coded data are used.
     */
    void initialize();

    /**
     * Evaluates the electrical resistivity rho
     * @param temperature T in (K)
     * @return resistivity in (Ohm*ang)
     */
    double evaluate_resistivity(double temperature) const;


    double evaluate_resistivity_derivative(double temperature) const;

    /**
     * electrical conductivity sigma in (1/(Ohm*ang))
     */
    double sigma(double temperature) const;

    /**
     * electrical conductivity derivative
     */
    double dsigma(double temperature) const;

    /**
     * thermal conductivity in (W/(ang*K))
     */
    double kappa(double temperature) const;

    /**
     * thermal conductivity derivative
     */
    double dkappa(double temperature) const;

    /**
     * Outputs sigma, kappa, res (and d-s) and emission currents to files in specified path
     * NB: Slow!!!
     */
    void output_to_files() const;

private:
    const Config::Heating& config;

    vector<pair<double, double>> resistivity_data;
};

} /* namespace femocs */

#endif /* HEATDATA_H_ */
