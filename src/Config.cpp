/*
 * Config.cpp
 *
 *  Created on: 11.11.2016
 *      Author: veske
 */

#include <fstream>
#include <algorithm>

#include "Config.h"
#include "Globals.h"
#include "Macros.h"


using namespace std;
namespace femocs {

Config::Config() {
    path.extended_atoms = "";
    path.infile = "";
    path.mesh_file = "";
    path.restart_file = "";
    path.bulk_mesh_file = "";
    path.vacuum_mesh_file = "";
    path.bulk_mesh_file = "";

    behaviour.verbosity = "verbose";
    behaviour.project = "runaway";
    behaviour.n_restart = 0;
    behaviour.restart_multiplier = 10;
    behaviour.n_write_log = 0;
    behaviour.n_read_conf = 0;
    behaviour.interpolation_rank = 1;
    behaviour.timestep_fs = 4.05773;
    behaviour.mass = 63.5460;
    behaviour.rnd_seed = time(NULL);
    behaviour.n_omp_threads = 1;
    behaviour.axisymmetry = false;
    behaviour.dimension = 3;
    behaviour.run_time = 10.;

    run.cluster_anal = true;
    run.apex_refiner = false;
    run.rdf = false;
    run.output_cleaner = true;
    run.surface_cleaner = true;
    run.field_smoother = true;
    run.smooth_updater = true;

    geometry.nnn = 12;
    geometry.latconst = 3.61;
    geometry.coordination_cutoff = 3.1;
    geometry.cluster_cutoff = 0;
    geometry.charge_cutoff = 30;
    geometry.surface_thickness = 3.1;
    geometry.box_width = 10;
    geometry.box_height = 6;
    geometry.bulk_height = 20;
    geometry.radius = 0.0;
    geometry.theta = 0.0;
    geometry.height = 0.0;
    geometry.distance_tol = 0.0;
    geometry.beta_atoms = 0.0;

    tolerance.charge_min = 0.8;
    tolerance.charge_max = 1.2;
    tolerance.field_min = 0.1;
    tolerance.field_max = 5.0;

    field.E0 = 0.0;
    field.ssor_param = 1.2;
    field.cg_tolerance = 1e-9;
    field.n_cg = 10000;
    field.V0 = 0.0;
    field.anode_BC = "neumann";
    field.mode = "laplace";
    field.V_min = -1.0;
    field.V_max = 1e4;
    field.magnetic_field = {0., 0., 0.};

    heating.mode = "none";
    heating.rhofile = "rho_table.dat";
    heating.lorentz = 2.44e-8;
    heating.t_ambient = 300.0;
    heating.n_cg = 2000;
    heating.cg_tolerance = 1e-9;
    heating.ssor_param = 1.2;         // 1.2 is known to work well with Laplace
    heating.delta_time = 10.0;
    heating.dt_max = 1.0e5;
    heating.tau = 100.0;
    heating.T_min = 0.0;
    heating.T_max = 1e5;
    heating.T_clamp = false;
    heating.T_preheat = 0.;
    heating.nottingham_heating = true;
    heating.bombardment_heating = false;
    heating.blackbody = false;
    heating.heating_file = "ch_solver.movie";

    emission.blunt = true;
    emission.cold = false;
    emission.omega = 0.0;
    emission.J_min = 0.0;
    emission.J_max = 1e-4;
    emission.work_function = 4.5;     // work function [eV]
    emission.omega = -1;           // SC is ignored in Emission by default
    emission.param_file = "in/GetelecPar.in";
    emission.emission_files = {"emission.dat", "emission.movie"};

    force.mode = "none";
    force.beta = 1.0;

    mesh.quality = "2.0";
    mesh.volume = "";
    mesh.algorithm = "laplace";
    mesh.n_steps = 0;
    mesh.lambda = 0.6307;
    mesh.mu = -0.6732;
    mesh.coplanarity = 1.0;

    cfactor.amplitude = 0.4;
    cfactor.r0_cylinder = 0;
    cfactor.r0_sphere = 0;
    cfactor.exponential = 0.5;

    pic.dt_max = 1.0;
    pic.density_max = 0.;
    pic.weight_min = 1.;
    pic.weight_max = 1000.;
    pic.weight_el =  0.;
    pic.weight_neutral =  1.; // TODO: Weight for neutrals
    pic.weight_ion = 1.0; // TODO: Weight for ions
    pic.vapor_mass = 63.546; // Cu atomic mass [amu]
    pic.fractional_push = true;
    pic.splitting = true;
    pic.merging = false;
    pic.ionize_all = false;
    pic.collide_ee = true;
    pic.collide_elastic = true;
    pic.collide_coulomb = true;
    pic.collide_ionization = true;
    pic.collide_exchange = false;
    pic.collide_recombination = false;
    pic.sputtering = false;
    pic.periodic = false;
    pic.reflective == "none";
    pic.landau_log = 13.0;
    pic.max_injected = 50000;
    pic.max_radius = -1.;
    pic.injections = 100;
    pic.injection_table = "";
    pic.injection_interval = 10.;
    pic.save_removed = false;
    pic.inject_neutrals = false;
    pic.vapor_poly = {-4.83695381e+09,  6.83908293e+06, -4.08936679e+04,  2.54439136e+01};
    pic.vapor_heat = 3.43; //Cu value default
    pic.ionization_energies_file = "ionization_energies.dat";
    pic.neutral_ionization_file = "neutral_ionization_cs.dat";
    pic.ion_ionization_file = "ion_ionization_cs.dat";
    pic.nn_elastic_file = "nn_elastic_cs.dat";
    pic.en_elastic_file = "en_elastic_cs.dat";
    pic.recombination_file = "recombination_cs.dat";
    pic.exchange_file = "exchange_cs.dat";
    pic.sput_energies_file = "sput_energies.dat";
    pic.sput_yields_file = "sput_yields.dat";
    pic.field_ionization = true;
    pic.pic_file = "pic_solver.movie";
    pic.electrons_file = "electrons.movie";
    pic.neutrals_file = "neutrals.movie";
    pic.ions_file = "ions.movie";

    scharge.convergence = 1.;
    scharge.Fl_apex_min = .5;     ///< Minimum apex Laplace field to run
    scharge.Fl_apex_max = 4.;     ///< Maximum apex Laplace field to run
    scharge.N_fields = 10;           ///< Number of runs for field sweep
    scharge.fpi_relaxation = 1.;
    scharge.theta_lim = 0.4;
    scharge.mode = "ramp_field";

    circuit_model.R_series = 0.0;
    circuit_model.C_gap = 0.0;
}

void Config::append_input_dir(string &file_name) const {
    if (file_name != "")
        file_name = MODES.IN_FOLDER + file_name;
}

void Config::append_input_dir() {
    append_input_dir(path.extended_atoms);
    append_input_dir(path.infile);
    append_input_dir(path.mesh_file);
    append_input_dir(path.bulk_mesh_file);
    append_input_dir(path.vacuum_mesh_file);

    append_input_dir(heating.rhofile);

    append_input_dir(pic.ionization_energies_file);
    append_input_dir(pic.neutral_ionization_file);
    append_input_dir(pic.ion_ionization_file);
    append_input_dir(pic.nn_elastic_file);
    append_input_dir(pic.en_elastic_file);
    append_input_dir(pic.recombination_file);
    append_input_dir(pic.exchange_file);

    append_input_dir(pic.sput_energies_file);
    append_input_dir(pic.sput_yields_file);
}

// Remove the noise from the beginning of the string
void Config::trim(string& str) {
    str.erase(0, str.find_first_of(comment_symbols + data_symbols));
}

void Config::read_all() {
    if (behaviour.n_read_conf > 0 && GLOBALS.TIMESTEP % behaviour.n_read_conf == 0)
        read_all(file_name, false);
}

void Config::read_all(const string& fname, bool full_run) {
    if (fname == "") return;
    file_name = fname;

    // Store the commands and their arguments
    parse_file(fname);

    if (full_run) {
        // Check for the obsolete commands
        check_obsolete("postprocess_marking");
        check_obsolete("force_factor");
        check_obsolete("use_histclean");
        check_obsolete("maxerr_SC");
        check_obsolete("SC_mode");
        check_obsolete("heat_assemble");
        check_obsolete("field_assemble");
        check_obsolete("timestep_step");

        // Check for the changed commands
        check_changed("heating", "heat_mode");
        check_changed("surface_thichness", "surface_thickness");
        check_changed("smooth_factor", "surface_smooth_factor");
        check_changed("surface_cleaner", "clean_surface");
        check_changed("write_log", "n_write_log");
        check_changed("run_pic", "field_mode");
        check_changed("electronWsp", "electron_weight");
        check_changed("field_solver", "field_mode");
        check_changed("pic_mode", "field_mode");
        check_changed("heating_mode", "heat_mode");
        check_changed("n_writefile", "write_period");
        check_changed("Vappl_SC", "sc_omega & Vappl");
        check_changed("omega_SC", "sc_omega");
        check_changed("n_write_restart", "restart_period");
        check_changed("current_limit", "rho_limit");
        check_changed("restart", "restart_file");
    }

    // Modify the parameters that are specified in input script
    read_command("work_function", emission.work_function);
    read_command("emitter_blunt", emission.blunt);
    read_command("sc_omega", emission.omega);
    read_command("emitter_cold", emission.cold);
    read_command("getelec_parmeter_file", emission.param_file);
    read_command("emission_files", emission.emission_files);

    read_command("heat_mode", heating.mode);
    read_command("rhofile", heating.rhofile);
    read_command("lorentz", heating.lorentz);
    read_command("t_ambient", heating.t_ambient);
    read_command("heat_ncg", heating.n_cg);
    read_command("heat_cgtol", heating.cg_tolerance);
    read_command("heat_ssor", heating.ssor_param);
    read_command("heat_dt", heating.delta_time);
    read_command("heat_dtmax", heating.dt_max);
    read_command("heat_clamp", heating.T_clamp);
    read_command("preheat", heating.T_preheat);
    read_command("vscale_tau", heating.tau);
    read_command("nottingham_heating", heating.nottingham_heating);
    read_command("bombardment_heating", heating.bombardment_heating);
    read_command("blackbody", heating.blackbody);
    read_command("heating_file", heating.heating_file);

    read_command("field_mode", field.mode);
    read_command("field_ssor", field.ssor_param);
    read_command("field_cgtol", field.cg_tolerance);
    read_command("field_ncg", field.n_cg);
    read_command("elfield", field.E0);
    read_command("Vappl", field.V0);
    read_command("anode_bc", field.anode_BC);

    read_command("mesh_quality", mesh.quality);
    read_command("element_volume", mesh.volume);
    read_command("smooth_steps", mesh.n_steps);
    read_command("smooth_lambda", mesh.lambda);
    read_command("smooth_mu", mesh.mu);
    read_command("smooth_algorithm", mesh.algorithm);
    read_command("coplanarity", mesh.coplanarity);

    read_command("force_mode", force.mode);
    read_command("charge_smooth_factor", force.beta);

    read_command("surface_smooth_factor", geometry.beta_atoms);
    read_command("distance_tol", geometry.distance_tol);
    read_command("latconst", geometry.latconst);
    read_command("coord_cutoff", geometry.coordination_cutoff);
    read_command("cluster_cutoff", geometry.cluster_cutoff);
    read_command("charge_cutoff", geometry.charge_cutoff);
    read_command("surface_thickness", geometry.surface_thickness);
    read_command("nnn", geometry.nnn);
    read_command("radius", geometry.radius);
    read_command("coarse_theta", geometry.theta);
    read_command("tip_height", geometry.height);
    read_command("box_width", geometry.box_width);
    read_command("box_height", geometry.box_height);
    read_command("bulk_height", geometry.bulk_height);

    read_command("extended_atoms", path.extended_atoms);
    read_command("infile", path.infile);
    read_command("mesh_file", path.mesh_file);
    read_command("restart_file", path.restart_file);
    read_command("bulk_mesh", path.bulk_mesh_file);
    read_command("vacuum_mesh", path.vacuum_mesh_file);
    read_command("out_folder", path.out_folder);

    read_command("smooth_update", run.smooth_updater);
    read_command("cluster_anal", run.cluster_anal);
    read_command("refine_apex", run.apex_refiner);
    read_command("use_rdf", run.rdf);
    read_command("clear_output", run.output_cleaner);
    read_command("clean_surface", run.surface_cleaner);
    read_command("smoothen_field", run.field_smoother);
    read_command("femocs_periodic", MODES.PERIODIC);

    read_command("n_read_conf", behaviour.n_read_conf);
    read_command("restart_period", behaviour.n_restart);
    read_command("restart_multiplier", behaviour.restart_multiplier);
    read_command("n_write_log", behaviour.n_write_log);
    read_command("femocs_verbose_mode", behaviour.verbosity);
    read_command("project", behaviour.project);
    read_command("interpolation_rank", behaviour.interpolation_rank);
    read_command("write_period", MODES.WRITE_PERIOD);
    read_command("md_timestep", behaviour.timestep_fs);
    read_command("mass(1)", behaviour.mass);
    read_command("seed", behaviour.rnd_seed);
    read_command("n_omp", behaviour.n_omp_threads);
    read_command("axisymmetry", behaviour.axisymmetry);
    read_command("dimension", behaviour.dimension);
    read_command("run_time", behaviour.run_time);

    read_command("csfile", pic.csfile);
    read_command("pic_dtmax", pic.dt_max);
    read_command("density_max", pic.density_max);
    read_command("electron_weight", pic.weight_el);
    read_command("neutral_weight", pic.weight_neutral);
    read_command("ion_weight", pic.weight_ion);
    read_command("vapor_heat", pic.vapor_heat);
    read_command("vapor_mass", pic.vapor_mass);
    read_command("electron_weight", pic.weight_el);
    read_command("pic_fractional_push", pic.fractional_push);
    read_command("pic_splitting", pic.splitting);
    read_command("pic_merging", pic.merging);
    read_command("pic_collide_ee", pic.collide_ee);
    read_command("pic_collide_elastic", pic.collide_elastic);
    read_command("pic_collide_coulomb", pic.collide_coulomb);
    read_command("pic_collide_ionization", pic.collide_ionization);
    read_command("pic_collide_exchange", pic.collide_exchange);
    read_command("pic_collide_recombination", pic.collide_recombination);
    read_command("pic_field_ionization", pic.field_ionization);
    read_command("pic_sputtering", pic.sputtering);
    read_command("pic_periodic", pic.periodic);
    read_command("pic_landau_log", pic.landau_log);
    read_command("pic_ionize_all", pic.ionize_all);
    read_command("max_injected", pic.max_injected);
    read_command("pic_reflective", pic.reflective);
    read_command("max_radius", pic.max_radius);
    read_command("injections", pic.injections);
    read_command("injections_table", pic.injection_table);
    read_command("injection_interval", pic.injection_interval);
    read_command("save_removed_particles", pic.save_removed);
    read_command("inject_neutrals", pic.inject_neutrals);
    read_command("ionization_energies_file", pic.ionization_energies_file);
    read_command("neutral_ionization_file", pic.neutral_ionization_file);
    read_command("ion_ionization_file", pic.ion_ionization_file);
    read_command("nn_elastic_file", pic.nn_elastic_file);
    read_command("en_elastic_file", pic.en_elastic_file);
    read_command("recombination_file", pic.recombination_file);
    read_command("exchange_file", pic.exchange_file);
    read_command("sput_energies_file", pic.sput_energies_file);
    read_command("sput_yields_file", pic.sput_yields_file);
    read_command("evaporation_polynomial", pic.vapor_poly);
    read_command("pic_file", pic.pic_file);
    read_command("electrons_file", pic.electrons_file);
    read_command("neutrals_file", pic.neutrals_file);
    read_command("ions_file", pic.ions_file);
    
    read_command("circ_R", circuit_model.R_series);
    read_command("circ_C", circuit_model.C_gap);

    read_command("SC_converge_criterion", scharge.convergence);
    read_command("SC_richardson_omega", scharge.fpi_relaxation);
    read_command("SC_min_apex_field", scharge.Fl_apex_min);
    read_command("SC_max_apex_field", scharge.Fl_apex_max);
    read_command("SC_N_fields", scharge.N_fields);
    read_command("SC_theta_lim", scharge.theta_lim);
    read_command("SC_run_mode", scharge.mode);

    // Read commands with potentially multiple arguments like...
    vector<double> args;
    int n_read_args;

    // ...SP weight limits
    args = {pic.weight_min, pic.weight_max};
    n_read_args = read_command("weight_limit", args);
    pic.weight_min = args[0];
    pic.weight_max = args[1];

    // ...temperature, current density and potential limits
    args = {heating.T_min, heating.T_max};
    n_read_args = read_command("heat_limit", args);
    heating.T_min = args[0];
    heating.T_max = args[1];

    args = {emission.J_min, emission.J_max};
    n_read_args = read_command("rho_limit", args);
    emission.J_min = args[0];
    emission.J_max = args[1];

    args = {field.V_min, field.V_max};
    n_read_args = read_command("potential_limit", args);
    field.V_min = args[0];
    field.V_max = args[1];

    // ...charge and field tolerances
    args = {0, 0};
    n_read_args = read_command("charge_tolerance", args);
    if (n_read_args == 1) {
        tolerance.charge_min = 1.0 - args[0];
        tolerance.charge_max = 1.0 + args[0];
    } else if (n_read_args == 2) {
        tolerance.charge_min = args[0];
        tolerance.charge_max = args[1];
    }

    n_read_args = read_command("field_tolerance", args);
    if (n_read_args == 1) {
        tolerance.field_min = 1.0 - args[0];
        tolerance.field_max = 1.0 + args[0];
    } else if (n_read_args == 2) {
        tolerance.field_min = args[0];
        tolerance.field_max = args[1];
    }

    // ...magnetic field components
    args = {field.magnetic_field[0], field.magnetic_field[1], field.magnetic_field[2]};
    n_read_args = read_command("magnetic_field", args);
    
    if (n_read_args == 3) {
        field.magnetic_field[0] = args[0];
        field.magnetic_field[1] = args[1];
        field.magnetic_field[2] = args[2];
    }

    // ...coarsening factors
    read_command("coarse_rate", cfactor.exponential);
    args = {cfactor.amplitude, (double)cfactor.r0_cylinder, (double)cfactor.r0_sphere};
    n_read_args = read_command("coarse_factor", args);
    cfactor.amplitude = args[0];
    cfactor.r0_cylinder = static_cast<int>(args[1]);
    cfactor.r0_sphere = static_cast<int>(args[2]);

    scharge.apply_factors.resize(128);
    n_read_args = read_command("apply_factors", scharge.apply_factors);
    if (n_read_args > 0)
        scharge.apply_factors.resize(n_read_args);
    else
        scharge.apply_factors = {1.};

    scharge.I_pic.resize(128);
    n_read_args = read_command("currents_pic", scharge.I_pic);
    scharge.I_pic.resize(n_read_args);
}

void Config::parse_file(const string& file_name) {
    ifstream file(file_name);
    require(file.is_open(), "File not found: " + file_name);

    string line;
    data.clear();

    // loop through the lines in a file
    while (getline(file, line)) {
        line += " "; // needed to find the end of line

        bool line_started = true;
        // store the command and its parameters from non-empty and non-pure-comment lines
        while(line.size() > 0) {
            trim(line);
            int i = line.find_first_not_of(data_symbols);
            if (i <= 0) break;

            if (line_started && line.substr(0, i) == "femocs_end") return;
            if (line_started) data.push_back({});
            if (line_started) {
                // force all the characters in a command to lower case
                string command = line.substr(0, i);
                std::transform(command.begin(), command.end(), command.begin(), ::tolower);
                data.back().push_back(command);
            } else {
                data.back().push_back( line.substr(0, i) );
            }

            line = line.substr(i);
            line_started = false;
        }
    }
}

void Config::check_obsolete(const string& command) {
    for (const vector<string>& cmd : data)
        if (cmd[0] == command) {
            write_silent_msg("Command '" + command + "' is obsolete! You can safely remove it!");
            return;
        }
}

void Config::check_changed(const string& command, const string& substitute) {
    for (const vector<string>& cmd : data)
        if (cmd[0] == command) {
            write_silent_msg("Command '" + command + "' has changed!"
                    " It is similar yet different to the command '" + substitute + "'!");
            return;
        }
}

int Config::read_command(string param, string& arg) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param) {
            arg = str[1]; return 0;
        }
    return 1;
}

int Config::read_command(string param, bool& arg) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param) {
            istringstream is1(str[1]);
            istringstream is2(str[1]);
            bool result;
            // try to parse the bool argument in text format
            if (is1 >> std::boolalpha >> result) { arg = result; return 0; }
            // try to parse the bool argument in numeric format
            else if (is2 >> result) { arg = result; return 0; }
            return 1;
        }
    return 1;
}

int Config::read_command(string param, unsigned int& arg) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param) {
            istringstream is(str[1]); int result;
            if (is >> result) { arg = result; return 0; }
            return 1;
        }
    return 1;
}

int Config::read_command(string param, int& arg) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param) {
            istringstream is(str[1]); int result;
            if (is >> result) { arg = result; return 0; }
            return 1;
        }
    return 1;
}

int Config::read_command(string param, double& arg) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param) {
            istringstream is(str[1]); double result;
            if (is >> result) { arg = result; return 0; }
            return 1;
        }
    return 1;
}

int Config::read_command(string param, vector<string>& args) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    int n_read_args = 0;
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param)
            for (unsigned i = 0; i < args.size() && i < (str.size()-1); ++i) {
                args[i] = str[i+1];
		    n_read_args++;
            }
    return n_read_args;
}

int Config::read_command(string param, vector<double>& args) {
    // force the parameter to lower case
    std::transform(param.begin(), param.end(), param.begin(), ::tolower);
    // loop through all the commands that were found from input script
    int n_read_args = 0;
    for (const vector<string>& str : data)
        if (str.size() >= 2 && str[0] == param)
            for (unsigned i = 0; i < args.size() && i < (str.size()-1); ++i) {
                istringstream is(str[i+1]);
                double result;
                if (is >> result) { args[i] = result; n_read_args++; }
            }
    return n_read_args;
}

void Config::print_data() {
    if (!MODES.VERBOSE) return;
    const int cmd_len = 20;

    for (const vector<string>& line : data) {
        for (const string& ln : line) {
            int str_len = ln.length();
            int whitespace_len = max(1, cmd_len - str_len);
            cout << ln << string(whitespace_len, ' ');
        }

        cout << endl;
    }
}

} // namespace femocs
