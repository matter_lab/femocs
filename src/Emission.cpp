/*
 * Emission.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: kyritsak
 */

#include "Emission.h"
#include "FieldSolver.h"
#include "CurrentHeatSolver.h"
#include "Config.h"
#include "getelec.h"


using namespace std;
namespace femocs {


template<int dim>
Emission<dim>::Emission(const FieldSolver<dim> *ps, const Config &conf,
        const CurrentHeatSolver<dim> *chs) :
        psolver(ps), chsolver(chs), config(conf), work_function(conf.emission.work_function)
{}

template<int dim>
void Emission<dim>::initialize() {
    calc_interface();

    int N = size();
    require(N > 0, "Emission can't use empty fields!");

    // deallocate and allocate currents data
    fields.resize(N);
    current_densities.resize(N);
    nottingham.resize(N);
    currents.resize(N);
    thetas_SC.resize(N);
    markers.resize(N);
    temperatures.resize(N);
    surface_heats.resize(N);
    vapor_heat.resize(N);
    vapor_flux.resize(N);
    blackbody_heat.resize(N);
    bombardment_heat.resize(N);

    // fill with zeros for first calculation
    fill(nottingham.begin(), nottingham.end(), 0.0);
    fill(current_densities.begin(), current_densities.end(), 0.0);
    fill(currents.begin(), currents.end(), 0.0);
    fill(surface_heats.begin(), surface_heats.end(), 0.0);
    fill(bombardment_heat.begin(), bombardment_heat.end(), 0.0);

    if (!config.emission.blunt){
        rline.resize(n_lines);
        Vline.resize(n_lines);
    }

    //Initialise data
    global_data.Jmax = 0.;
    global_data.Frep = 0.;
    global_data.Jrep = 0.;
    global_data.Tmax = 0.;

    stats.N_calls = 0;
    stats.I_tot.resize(0);
    stats.Jrep.resize(0);
    stats.Jmax.resize(0);
    stats.Frep.resize(0);
    stats.Fmax.resize(0);
}

template<int dim>
void Emission<dim>::calc_interface() {
    const double tol2 = 1e-10;  // max allowed tolerance between the meshes

    // Export the vacuum mesh interface
    psolver->export_surface_centroids(&centroids, &cells, &faces, &areas);

    // Export the bulk mesh interface
    if (chsolver)
        chsolver->heat.export_surface_centroids(&bulk_centroids, &bulk_cells, NULL, NULL);
    else {
        bulk_centroids = centroids;
        bulk_cells = cells;
    }

    const int n_centroids = centroids.size();
    if (n_centroids != bulk_centroids.size())
    	throw runtime_error("Vacuum and bulk mesh interfaces do not have the same size.\n");

    // Calculate the mapping between vacuum and the bulk mesh interfaces
    bulk2vac = vector<int>(n_centroids, -1);
    vac2bulk = vector<int>(n_centroids, -1);

    for (int i = 0; i < n_centroids; i++) {
        if (vac2bulk[i] >= 0) continue;

        for (int j = 0; j < n_centroids; j++) {
            if (bulk2vac[j] >= 0) continue;

            // Use squared distance as it's faster
            double distance2 = centroids[i].distance_square(bulk_centroids[j]);
            if (distance2 < tol2) {
                bulk2vac[j] = i;
                vac2bulk[i] = j;
                break;
            }
        }

        if (vac2bulk[i] < 0)
            throw runtime_error("Vacuum face " + d2s(i) + " does not match with any bulk face!\n");
    }
}

template<int dim>
void Emission<dim>::emission_line(int i_face, const Tensor<1,dim>& direction, const double rmax) {
    const double rmin = 1.e-5 * rmax;

    // calculate potentials on line starting from face centroid
    // and moving in direction of its norm
    Cell cell = cells[i_face];
    Point<dim> p_cell;
    vector<Point<dim>> line_points(n_lines);
    for (int i = 0; i < n_lines; i++){
        rline[i] = rmin + ((rmax - rmin) * i) / (n_lines - 1);
        line_points[i] = centroids[i_face] + rline[i] * direction;
    }

    for (int i = 0; i < n_lines; i++){
        int searches = psolver->utilities.locate_cell(line_points[i], p_cell, cell);
        Vline[i] = global_data.sfactor * psolver->utilities.probe_solution_unit(p_cell, cell);
        rline[i] *= nm_per_angstrom;
    }

    for (int i = 0; i < n_lines; i++){
        Vline[i] -= Vline[0];
        rline[i] -= rline[0];
    }

    // Check data condition (should be monotonous)
    for (int i = 1; i < n_lines; ++i) { // go through points
        if (Vline[i] < Vline[i-1]) { // if decreasing at a point
            double dVdx = 0.0;
            int j;
            for (j = i + 1; j < n_lines; ++j) {
                if (Vline[j] > Vline[i-1]) {
                    dVdx = (Vline[j] - Vline[i-1]) / (rline[j] - rline[i-1]);
                    break;
                }
            }

            if (dVdx == 0.0) {
                if (i > 1)
                    dVdx = (Vline[i-1] - Vline[i-2]) / (rline[i-1] - rline[i-2]);
                else
                    write_verbose_msg("Non-monotonous Vline could not be recovered at i = " + d2s(i));
            }
            for (int k = 0; k <= j; ++k)
                Vline[k] =  Vline[i-1] + (rline[k] - rline[i-1]) * dVdx;
        }
    }
}

template<int dim>
void Emission<dim>::calculate_globals() {
    global_data.I_tot = 0;
    global_data.I_eff = 0;
    global_data.area = 0;

    if (is_effective.size() != size()){
        is_effective.resize(size());
        fill(is_effective.begin(), is_effective.end(), true);
    }

    double Fsum = 0.0;

    // loop through face centroids
    for (unsigned int i = 0; i < size(); ++i) {
        double Floc = fields[i].norm() * global_data.sfactor;
        double area = areas[i];

        currents[i] = area * current_density(i);
        global_data.I_tot += currents[i];

        if (is_effective[i]) {
            global_data.area += area;         // increase total area
            global_data.I_eff += currents[i]; // increase total current
            Fsum += Floc * area;
        }
    }

    global_data.Jrep = global_data.I_eff / global_data.area;
    global_data.Frep = Fsum / global_data.area;

    stats.N_calls++;
    stats.I_tot.push_back(global_data.I_tot);
    stats.Jrep.push_back(global_data.Jrep);
    stats.Jmax.push_back(global_data.Jmax);
    stats.Fmax.push_back(global_data.Fmax);
    stats.Frep.push_back(global_data.Frep);

}

template<int dim>
void Emission<dim>::calc_effective_region(double threshold, const string &mode) {
    require(mode == "current" || mode == "field", "Unimplemented mode: " + mode);

    int n_faces = size();
    is_effective.resize(n_faces);

    if (mode == "current") {
        for (int i = 0; i < n_faces; ++i)
            is_effective[i] = current_density(i) > global_data.Jmax * threshold;
    } else {
        for (int i = 0; i < n_faces; ++i)
            is_effective[i] = fields[i].norm() > global_data.Fmax * threshold;
    }
}

template<int dim>
void Emission<dim>::calc_bombardment(const vector<Face> &bombard_faces,
        const vector<double> &bombard_energies)
{
    double dt = config.heating.delta_time;
    for (auto &bh : bombardment_heat) bh = 0.;

    for (int i = 0; i < bombard_faces.size(); ++i) {
        for (int j = 0; j < size(); ++j) {
            Face face = cells[j]->face(faces[j]);

            if (bombard_faces[i] == face) {
                double area = areas[j];
                double bh = bombard_energies[i] / (CONSTANTS.watt * dt * area);
                bombardment_heat[j] += bh;
            }
        }
    }
}

template<int dim>
void Emission<dim>::calc_evaporation(double mass) {
    double vheat = config.pic.vapor_heat; // eV

    for (int i = 0; i < size(); i++){
        double T = temperatures[i];
        double Tinv = 1. / T;

        // Evaluate polynomial for log(vapor pressure [Pa])
        double logp = 0.;
        int Npoly = config.pic.vapor_poly.size();
        for (int j = 0; j < Npoly; j++)
            logp += pow(Tinv, j) * config.pic.vapor_poly[Npoly - 1 - j];

        double rtfactor = 1. / sqrt(2.0 * M_PI * mass * CONSTANTS.amu * CONSTANTS.kboltz  * T);
        double flux = rtfactor * exp(logp) * CONSTANTS.pascal;

        vapor_flux[i] = flux; // (1 / Å^2 / fs)
        // convert to W/A^2 and make negative (cooling)
        vapor_heat[i] = -flux * (vheat + CONSTANTS.kboltz * T) / CONSTANTS.watt;
    }
}

template<int dim>
void Emission<dim>::calc_blackbody() {
    for (int i = 0; i < size(); i++) {
        double T = temperatures[i];
        double emissivity = 1.0;

        blackbody_heat[i] = -emissivity * CONSTANTS.stefboltz * pow(T, 4);
    }
}

template<int dim>
void Emission<dim>::extract_temperatures() {
    // TODO why is there an option not to have chsolver?
    if (chsolver)
        for (int i = 0; i < size(); ++i)
            temperatures[bulk2vac[i]] = chsolver->heat.utilities.probe_solution(bulk_centroids[i], bulk_cells[i]);
    else
        fill(temperatures.begin(), temperatures.end(), config.heating.t_ambient);
}

template<int dim>
void Emission<dim>::extract_fields() {
    psolver->probe_field(centroids, cells, fields);
    global_data.Emax = 0;
    for (const Tensor<1,dim> &field : fields)
        global_data.Emax = max(global_data.Emax, field.norm_square());
    global_data.Emax = sqrt(global_data.Emax);
}

template<int dim>
int Emission<dim>::calc_emission(double deff, bool update_eff_region) {
    constexpr int J_error = -10;      // error code of current density is outside the limits
    constexpr int sign_error = -11;   // error code of field in wrong direction
    constexpr int fitting_error = -2; // error code of model fitting failed

    struct emission gt;
    gt.W = work_function;    // set workfuntion, must be set in conf. script
    gt.R = 1.e4;       // radius of curvature (overrided by femocs potential distribution)
    gt.gamma = 10;       // enhancement factor (overrided by femocs potential distribution)
    gt.theta = 1.;
    gt.pfilename = config.emission.param_file.c_str();
    gt.pfile_length = config.emission.param_file.size(); 
    double F, J;         // Local field and current density in femocs units
    bool fitting_failed = false;  // flag of non-fatal error

    global_data.Fmax = 0;
    global_data.Jmax = 0;
    global_data.Tmax = 0;

    extract_temperatures();

    for (int i = 0; i < size(); ++i) {
        double elfield = fields[i].norm();
        Tensor<1,dim> normal = - fields[i] / elfield;

        Cell cell = cells[i];
        int face = faces[i];

        Tensor<1,dim> face_normal;
        psolver->utilities.get_face_normal(cell, face, face_normal);

        if (normal * face_normal < 0.)
            elfield = 0.;

        if (global_data.Fmax < elfield) global_data.Fmax = elfield;

        F = global_data.sfactor * elfield;
        gt.mode = 0;
        gt.F = angstrom_per_nm * F;
        gt.Temp = temperatures[i];

        gt.voltage = F * deff;

        markers[i] = 0; // marker==0: no full calculation

        // Full calculation with line only for high field points
        if (F > 0.5 * global_data.Fmax && F > 0.1 && !config.emission.blunt) {
            // get emission line data
            emission_line(i, normal, 1.6 * work_function / F);

            gt.Nr = n_lines;
            gt.xr = &rline[0];
            gt.Vr = &Vline[0];
            gt.mode = -21;  // set mode to potential input data
            markers[i] = 1; // marker==1: emission calculated with line
        }

        gt.approx = 0; // GTF approximation
        if (deff <= 0)
            cur_dens_c(&gt); // calculate emission
        else
            cur_dens_SC(&gt);

        J = gt.Jem * nm2_per_angstrom2; // current density in femocs units

        // If J is worth it, calculate with full energy integration
        if ((J > 0.01 * global_data.Jmax || gt.ierr) && !config.emission.cold) {
            gt.approx = 1;
            if (deff <= 0)
                cur_dens_c(&gt); // calculate emission
            else {
                gt.voltage = F * deff;
                cur_dens_SC(&gt);
            }

            J = gt.Jem * nm2_per_angstrom2;
            markers[i] = 2;
        }

        if (gt.ierr != 0) {
            write_verbose_msg("GETELEC error. ierr = " + to_string(gt.ierr));
            if (gt.ierr == fitting_error)
                fitting_failed = true;
            else
                return gt.ierr;
        }

        current_density(i) = J;
        nottingham[i] = config.heating.nottingham_heating ? nm2_per_angstrom2 * gt.heat : 0.;
        surface_heat(i) = nottingham[i];
        thetas_SC[i] = gt.theta;

        global_data.Fmax = max(global_data.Fmax, F * gt.theta);
        global_data.Jmax = max(global_data.Jmax, J);
        global_data.Tmax = max(global_data.Tmax, gt.Temp);
    }

    if (global_data.Jmax > config.emission.J_max)
        return J_error;

    if (update_eff_region)
        calc_effective_region(0.9, "field");

    calculate_globals();

    if (fitting_failed)
        write_verbose_msg("Model fitting failed. Applied rough FN approximation.");
    return 0;
}

template<int dim>
void Emission<dim>::write_xyz(ofstream &out) const {
    FileWriter::write_xyz(out);

    out << "properties=id:I:1:pos:R:" << dim << ":area:R:1:marker:I:1:force:R:" << dim
            << ":current_density:R:1:current:R:1:nottingham_heat:R:1:bombardment_heat:R:1"
            << ":temperature:R:1:vapor_flux:R:1:vapor_heat:R:1:blackbody_heat:R:1:total_heat:R:1\n";

    for (int i = 0; i < size(); ++i) {
        out << i << ' ' << centroids[i] << ' ' << areas[i] << ' ' << markers[i]
                << ' ' << fields[i] << ' ' << current_density(i) << ' '
                << currents[i] << ' ' << nottingham[i] << ' '
                << bombardment_heat[i] << ' ' << temperatures[i] << ' '
                << vapor_flux[i] << ' ' << vapor_heat[i] << ' ' << blackbody_heat[i] << ' '
                << surface_heat(i) << endl;
    }
}

template<int dim>
void Emission<dim>::write_dat(ofstream &out, const string &pre_header) const {
    // In case of empty file, write data header
    if (this->first_line(out)) {
        out << pre_header
            << "time      Itot        Imean        I_fwhm        Area"
            << "        Jrep         Frep         Jmax        Fmax\n";
    }

    if (pre_header != "") return;

    double I_mean = 0.0;
    for (auto x : stats.I_tot)
        I_mean += x;
    I_mean /= stats.N_calls;

    // Write one line of data
    out << fixed << setprecision(2) << GLOBALS.TIME;
    out << scientific << setprecision(6)
            << ' ' << global_data.I_tot << ' ' << I_mean
            << ' ' << global_data.I_eff << ' ' << global_data.area
            << ' ' << global_data.Jrep << ' ' << global_data.Frep
            << ' ' << global_data.Jmax << ' ' << global_data.Fmax << endl;
}

template<int dim>
void Emission<dim>::write_stats(ofstream &out, const string &pre_header) const {
    // specify data header
    if (this->first_line(out)) {
        out << pre_header
            << "    Itot_mean    Itot_std     Jrep_mean    Jrep_std"
            << "    Jmax_mean    Jmax_std     Frep_mean    Frep_std"
            << "    Fmax_mean    Fmax_std\n";
    }

    if (pre_header != "") return;

    // specify data
    out << ' ' << stats.Itot_mean << ' ' << stats.Itot_std
        << ' ' << stats.Jrep_mean << ' ' << stats.Jrep_std
        << ' ' << stats.Jmax_mean << ' ' << stats.Jmax_std
        << ' ' << stats.Frep_mean << ' ' << stats.Frep_std
        << ' ' << stats.Fmax_mean << ' ' << stats.Fmax_std
        << endl;
}

template<int dim>
void Emission<dim>::write() {
    const string dat = config.emission.emission_files[0];
    const string movie = config.emission.emission_files[1];
    FileWriter::write(dat, FileIO::no_update);
    FileWriter::write(movie);
}

template<int dim>
void Emission<dim>::calc_total_heat() {
    for (int i = 0; i < size(); i++)
        surface_heat(i) = vapor_heat[i] + nottingham[i] + bombardment_heat[i] + blackbody_heat[i];
}

template<int dim>
void Emission<dim>::calc_global_stats() {
    //initialise statistics
    stats.Itot_mean = 0; stats.Itot_std = 0;
    stats.Jmax_mean = 0; stats.Jmax_std = 0;
    stats.Fmax_mean = 0; stats.Fmax_std = 0;
    stats.Jrep_mean = 0; stats.Jrep_std = 0;
    stats.Frep_mean = 0; stats.Frep_std = 0;

    //calculate mean values
    for (int i = 0; i < stats.N_calls; ++i) {
        stats.Itot_mean += stats.I_tot[i] / stats.N_calls;
        stats.Jmax_mean += stats.Jmax[i] / stats.N_calls;
        stats.Jrep_mean += stats.Jrep[i] / stats.N_calls;
        stats.Fmax_mean += stats.Fmax[i] / stats.N_calls;
        stats.Frep_mean += stats.Frep[i] / stats.N_calls;
    }

    //calculate standard deviations
    for (int i = 0; i < stats.N_calls; ++i) {
        stats.Itot_std += pow(stats.I_tot[i] - stats.Itot_mean, 2);
        stats.Jmax_std += pow(stats.Jmax[i] - stats.Jmax_mean, 2);
        stats.Jrep_std += pow(stats.Jrep[i] - stats.Jrep_mean, 2);
        stats.Fmax_std += pow(stats.Fmax[i] - stats.Fmax_mean, 2);
        stats.Frep_std += pow(stats.Frep[i] - stats.Frep_mean, 2);
    }
    stats.Itot_std = sqrt(stats.Itot_std / stats.N_calls);
    stats.Jmax_std = sqrt(stats.Jmax_std / stats.N_calls);
    stats.Jrep_std = sqrt(stats.Jrep_std / stats.N_calls);
    stats.Fmax_std = sqrt(stats.Fmax_std / stats.N_calls);
    stats.Frep_std = sqrt(stats.Frep_std / stats.N_calls);

    //re-initialise statistics
    stats.N_calls = 0;
    stats.I_tot.resize(0);
    stats.Jmax.resize(0);
    stats.Jrep.resize(0);
    stats.Fmax.resize(0);
    stats.Frep.resize(0);
}

template<int dim>
double Emission<dim>::get_temperature(int i) const {
    require(i >= 0 && i < (int)temperatures.size(), "Invalid index: " + d2s(i));
    return temperatures[i];
}

template<int dim>
double Emission<dim>::get_vapor_flux(int i) const {
    require(i >= 0 && i < (int)vapor_flux.size(), "Invalid index: " + d2s(i));
    return vapor_flux[i];
}

template<int dim>
double Emission<dim>::get_current(int i) const {
    require(i >= 0 && i < (int)currents.size(), "Invalid index: " + d2s(i));
    return currents[i];
}

template<int dim>
typename DoFHandler<dim>::active_cell_iterator Emission<dim>::get_cell(int i) const {
    require(i >= 0 && i < (int)cells.size(), "Invalid index: " + d2s(i));
    return cells[i];
}

template<int dim>
int Emission<dim>::get_face(int i) const {
    require(i >= 0 && i < (int)faces.size(), "Invalid index: " + d2s(i));
    return faces[i];
}

template<int dim>
double Emission<dim>::get_area(int i) const {
    require(i >= 0 && i < (int)areas.size(), "Invalid index: " + d2s(i));
    return areas[i];
}

template<int dim>
double Emission<dim>::get_vol(int i) const {
    require(i >= 0 && i < cells.size(), "Invalid index: " + d2s(i));
    return chsolver->get_cell_vol(cells[i]);
}

template<int dim>
Point<dim> Emission<dim>::get_centroid(int i) const {
    require(i >= 0 && i < (int)centroids.size(), "Invalid index: " + d2s(i));
    return centroids[i];
}

template<int dim>
Tensor<1,dim> Emission<dim>::get_field(int i) const {
    require(i >= 0 && i < (int)fields.size(), "Invalid index: " + d2s(i));
    return fields[i];
}

template class Emission<2> ;
template class Emission<3> ;

}  // namespace femocs
