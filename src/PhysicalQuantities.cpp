/*
 * physical_quantities.cc -> PhysicalQuantities.cpp
 *
 *  Created on: Apr 30, 2016
 *      Author: kristjan
 */

#include "Macros.h"

#include "PhysicalQuantities.h"


namespace femocs {


bool PhysicalQuantities::load_spreadsheet_grid_data(std::string filepath, InterpolationGrid &grid) {
    std::ifstream infile(filepath);
    if (!infile) {
        std::cerr << "Couldn't open \"" << filepath << "\"\n";
        return false;
    }

    grid.v.clear();

    std::vector<double> row;
    std::string line;
    double last_x = 0, last_y = 0;
    bool first_line = true;

    unsigned line_counter = 0;

    while (std::getline(infile, line)) {
        if (line[0] == '%' || line[0] == '\n')
            continue;
        std::istringstream stm(line);
        double x, y, z;
        stm >> x >> y >> z;

        if (first_line) {
            grid.xmin = x;
            grid.ymin = y;
            first_line = false;
        } else if (last_x != x) {
            grid.ynum = line_counter;
        }
        grid.v.push_back(z);

        last_x = x;
        last_y = y;
        line_counter++;
    }
    grid.xmax = last_x;
    grid.ymax = last_y;
    grid.xnum = grid.v.size() / grid.ynum;

    infile.close();
    return true;
}

bool PhysicalQuantities::load_compact_grid_data(std::string filepath, InterpolationGrid &grid) {
    std::ifstream infile(filepath);
    if (!infile) {
        std::cerr << "Couldn't open \"" << filepath << "\"\n";
        return false;
    }

    grid.v.clear();

    std::string line;
    int line_counter = 0;

    while (std::getline(infile, line)) {
        if (line[0] == '%' || line.size() == 0 || !contains_digit(line))
            continue;

        std::istringstream stm(line);
        double val;

        if (line_counter == 0) {
            stm >> grid.xmin >> grid.xmax >> grid.xnum;
        } else if (line_counter == 1) {
            stm >> grid.ymin >> grid.ymax >> grid.ynum;
        } else {
            stm >> val;
            grid.v.push_back(val);
        }
        line_counter++;
    }
    infile.close();
    return true;
}

double PhysicalQuantities::linear_interp(double x,
        std::vector<std::pair<double, double>> data) const {
    if (x <= data[0].first)
        return data[0].second;
    if (x >= data.back().first)
        return data.back().second;
    typedef std::pair<double, double> myPair;
    auto pair_comp = [](myPair lhs, myPair rhs) -> bool {return lhs.first < rhs.first;};
	// use binary search for the position:
    auto it1 = std::lower_bound(data.begin(), data.end(), std::make_pair(x, 0.0), pair_comp);
    auto it2 = it1 - 1;
    return it2->second + (it1->second - it2->second) * (x - it2->first) / (it1->first - it2->first);
}

double PhysicalQuantities::linear_interp(double x, vector<double> &xdata, vector<double> &ydata) const {
    if (x <= xdata[0])
        return ydata[0];
    if (x >= xdata.back())
        return ydata.back();
	// use binary search for the position:
    auto it1 = lower_bound(xdata.begin(), xdata.end(), x);
    int ind1 = it1 - xdata.begin();
    auto ind2 = ind1 - 1;
    return ydata[ind2] + (ydata[ind1] - ydata[ind2]) * (x - xdata[ind2]) / (xdata[ind1] - xdata[ind2]);
}

double PhysicalQuantities::loglog_interp(double x, vector<double> &xdata, vector<double> &ydata) const {
    if (x <= xdata[0])
        return ydata[0];
    if (x >= xdata.back())
        return ydata.back();
	// use binary search for the position:
    auto it1 = lower_bound(xdata.begin(), xdata.end(), x);
    int ind1 = it1 - xdata.begin();
    auto ind2 = ind1 - 1;
    double dlogy = log(ydata[ind1] / ydata[ind2]);
    double dlogx = log(xdata[ind1] / xdata[ind2]);
    return ydata[ind2] * pow(x / xdata[ind2], dlogy/dlogx);
}

double PhysicalQuantities::evaluate_derivative(std::vector<std::pair<double, double>> &data,
        std::vector<std::pair<double, double>>::iterator it) const {
    if (it == data.begin()) {
        return ((it + 1)->second - it->second) / ((it + 1)->first - it->first);
    } else if (it == data.end() - 1) {
        return (it->second - (it - 1)->second) / (it->first - (it - 1)->first);
    }
    return ((it + 1)->second - (it - 1)->second) / ((it + 1)->first - (it - 1)->first);
}

double PhysicalQuantities::deriv_linear_interp(double x, std::vector<std::pair<double, double>> data) const {
    double eps = 1e-10;
    if (x <= data[0].first)
        x = data[0].first + eps;
    if (x >= data.back().first)
        x = data.back().first;
    typedef std::pair<double, double> myPair;
    auto pair_comp = [](myPair lhs, myPair rhs) -> bool {return lhs.first < rhs.first;};
    // use binary search for the position:
    auto it = std::lower_bound(data.begin(), data.end(), std::make_pair(x, 0.0), pair_comp);
    auto itp = it - 1;

    double dit = evaluate_derivative(data, it);
    double ditp = evaluate_derivative(data, itp);

    return ditp + (dit - ditp) * (x - itp->first) / (it->first - itp->first);
}

double PhysicalQuantities::bilinear_interp(double x, double y, const InterpolationGrid &grid_data) const {
	//std::printf("%f, %f, %f\n", x, grid_data.xmin, grid_data.xmax);
	//std::printf("%f, %f, %f\n", y, grid_data.ymin, grid_data.ymax);
    double eps = 1e-10;
    if (x <= grid_data.xmin)
        x = grid_data.xmin;
    if (x >= grid_data.xmax)
        x = grid_data.xmax - eps;
    if (y <= grid_data.ymin)
        y = grid_data.ymin;
    if (y >= grid_data.ymax)
        y = grid_data.ymax - eps;

    // Note that # of "transitions" = # of rows - 1
    double dx = (grid_data.xmax - grid_data.xmin) / (grid_data.xnum - 1);
    double dy = (grid_data.ymax - grid_data.ymin) / (grid_data.ynum - 1);

    // indexes of the "square", where (x,y) is located

    int xi = int((x - grid_data.xmin) / dx);
    int yi = int((y - grid_data.ymin) / dy);

    // coordinates of (x,y) on the "unit square"
    double xc = (x - grid_data.xmin) / dx - xi;
    double yc = (y - grid_data.ymin) / dy - yi;

    //std::printf("%.6f, %.6f, %.6f, %d\n", x, xc, dx, xi);
    //std::printf("%.6f, %.6f, %.6f, %d\n", y, yc, dy, yi);

    unsigned yn = grid_data.ynum;

    return grid_data.v[xi * yn + yi] * (1 - xc) * (1 - yc)
            + grid_data.v[(xi + 1) * yn + yi] * xc * (1 - yc)
            + grid_data.v[xi * yn + yi + 1] * (1 - xc) * yc
            + grid_data.v[(xi + 1) * yn + yi + 1] * xc * yc;
}

int PhysicalQuantities::read_pair(vector<pair<double, double>> &pair, string filename){

    ifstream is(filename);
    if (is.is_open()) {
        write_verbose_msg("\nReading data from " + filename);
		int Nline;
		is >> Nline;
		pair.resize(Nline);
		for (int i = 0; i < Nline; ++i){
			is >> pair[i].first;
			is >> pair[i].second;
		}
		is.close();
		return 1;
    } else
    	return 0;
}

int PhysicalQuantities::read_pair_nosize(vector<pair<double, double>> &pair, string filename){

	ifstream is(filename);
    if (is.is_open()) {
    	write_verbose_msg("\nReading data from " + filename);
    	double x, y;
    	while (is >> x >> y)
    		pair.push_back(make_pair(x, y));
    	is.close();
    	return 1;
    } else
    	return 0;
}

int PhysicalQuantities::read_columns(vector<vector<double>> &data, string filename){
	ifstream is(filename);
	if (!is.is_open())
		return 0;

	write_verbose_msg("\nReading data from " + filename);
	string line;
	getline(is, line);
	stringstream ss(line);
    double x;
    int Ncolumns;

    for(Ncolumns = 0; ss >> x; Ncolumns++)
    	data.push_back(vector<double>({x}));


    while (getline(is, line)) {
    	stringstream ssn(line);; //reinit the ss with new line
    	int i = 0;
        for(; ssn >> x; i++)
        	data[i].push_back(x);

        if (i != Ncolumns) // check that correct number of columns is read in line
        	throw("Input data file " + filename + " has not correct columns.");
    }

    write_verbose_msg("Read " + to_string(data.size()) + " data columns of size "
    		+ to_string(Ncolumns) + " each");


    return 1;
}

int PhysicalQuantities::read_row(vector<double> &data, string filename){
    ifstream is(filename);
    if (!is.is_open())
        return 0;

    write_verbose_msg("\nReading data from " + filename);
    string line;
    getline(is, line);
    stringstream ss(line);
    double x;
    int Ncolumns;

    for(Ncolumns = 0; ss >> x; Ncolumns++)
        data.push_back(x);

    return 1;
}

} // namespace femocs

