/*
 * HeatData.cpp
 *
 *  Created on: Jun 22, 2020
 *      Author: kyritsak
 */

#include "HeatData.h"
#include "Macros.h"

#include <sys/stat.h>

namespace femocs {

void HeatData::initialize() {
    string ftype = get_file_type(config.rhofile);
    require(ftype=="dat", "Unknown file type: " + config.rhofile);

    if (read_pair(resistivity_data, config.rhofile))
    	return;
    else {
        write_silent_msg("Resistivity file " + config.rhofile + " not found! Using resistivities of bulk Cu.");
        resistivity_data = {
        		{200,       1.04880891725670e1},
				{250,       1.38746906313026e1},
				{273.15,    1.54302576609579e1},
				{300,       1.72303253767393e1},
				{350,       2.05822342557969e1},
				{400,       2.39476190263668e1},
				{450,       2.73376276457821e1},
				{500,       3.07598544167200e1},
				{550,       3.42197627890272e1},
				{600,       3.77214997429762e1},
				{650,       4.12683756079926e1},
				{700,       4.48631564739703e1},
				{750,       4.85082484331790e1},
				{800,       5.22058173711257e1},
				{850,       5.59578691681847e1},
				{900,       5.97663048918425e1},
				{950,       6.36329597840890e1},
				{1000,      6.75596315079091e1},
				{1050,      7.15481011300350e1},
				{1100,      7.56001491045029e1},
				{1150,      7.97175677635730e1},
				{1200,      8.39021713382574e1},
				{1250,      8.81558042149690e1},
				{1300,      9.24803479251429e1},
				{1350,      9.68777272230961e1},
				{1400,      1.01349915510292e2}
        };
    }

}

double HeatData::evaluate_resistivity(double temperature) const {
    if (temperature < resistivity_data.front().first)
        temperature = resistivity_data.front().first;
    if (temperature > resistivity_data.back().first)
        temperature = resistivity_data.back().first;
    return 10. * linear_interp(temperature, resistivity_data);
}

double HeatData::evaluate_resistivity_derivative(double temperature) const {
    if (temperature < resistivity_data.front().first)
        temperature = resistivity_data.front().first;
    if (temperature > resistivity_data.back().first)
        temperature = resistivity_data.back().first;
    return 10. * deriv_linear_interp(temperature, resistivity_data);
}

double HeatData::sigma(double temperature) const {
    double rho = evaluate_resistivity(temperature);
    return 1.0 / rho;
}

double HeatData::dsigma(double temperature) const {
    double rho = evaluate_resistivity(temperature);
    return -evaluate_resistivity_derivative(temperature) / (rho * rho);
}

double HeatData::kappa(double temperature) const {
    if (temperature < resistivity_data.front().first)
        temperature = resistivity_data.front().first;
    if (temperature > resistivity_data.back().first)
        temperature = resistivity_data.back().first;

    return config.lorentz * temperature * sigma(temperature);
}

double HeatData::dkappa(double temperature) const {
    if (temperature < resistivity_data.front().first)
        temperature = resistivity_data.front().first;
    if (temperature > resistivity_data.back().first)
        temperature = resistivity_data.back().first;
    return config.lorentz * (sigma(temperature) + temperature * dsigma(temperature));
}

void HeatData::output_to_files() const {

    // temperature for emission current evaluation
    double temperature = 500.0;
    std::string filepath = "./output/";

    struct stat info;
    if (stat(filepath.c_str(), &info) != 0) {
        std::cerr << "Can't access " + filepath << std::endl;
        return;
    }
    std::cout << "Outputting physical quantities to \"" + filepath + "\"" << std::endl;

    FILE *rho_file = fopen((filepath + "rho_file.txt").c_str(), "w");
    FILE *sigma_file = fopen((filepath + "sigma_file.txt").c_str(), "w");
    FILE *kappa_file = fopen((filepath + "kappa_file.txt").c_str(), "w");

    for (double t = 100.0; t < 1500.0; t += 5.0) {
        fprintf(rho_file, "%.5e %.5e %.5e\n", t, evaluate_resistivity(t),
                evaluate_resistivity_derivative(t));
        fprintf(sigma_file, "%.5e %.5e %.5e\n", t, sigma(t), dsigma(t));
        fprintf(kappa_file, "%.5e %.5e %.5e\n", t, kappa(t), dkappa(t));
    }


    fclose(rho_file);
    fclose(sigma_file);
    fclose(kappa_file);
}


} /* namespace femocs */
