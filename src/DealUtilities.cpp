/*
 *  Created on: 9.2.2021
 *      Author: veske
 */

#include "DealUtilities.h"
#include "Macros.h"
#include "Primitives.h"

#include <deal.II/grid/grid_tools.h>
#include <deal.II/dofs/dof_tools.h>

#include <queue>


using namespace dealii;
using namespace std;

namespace femocs {

template<int dim>
DealUtilities<dim>::DealUtilities(const Triangulation<dim> *tr, const FE_Q<dim>& feq,
        const DoFHandler<dim> &dofh, const Vector<double> &s) :
        tria(tr), fe(feq), dof_handler(dofh), solution(s)
{}

template<int dim>
int DealUtilities<dim>::locate_cell(const Point<dim> &point, Point<dim> &ref_point,
        Cell &cell, const Tensor<1,dim>& displacement, int &f) const
{
    Cell cell_orig = cell;
    Cell previous_cell = cell;

    int max_searches = 50;
    f = -1;

    for(int searches = 1; searches < max_searches; searches++){
        if (check_cell(point, ref_point, cell))
            return searches;

        unsigned fpc = GeometryInfo<dim>::faces_per_cell;
        for (f = 0; f < fpc; ++f) { //loop through cell faces
            //and check if displacement vector crosses face
            if (line_cross_face(point, displacement, cell, f)){
                // if boundary face is crossed, particle is out
                if (cell->face(f)->at_boundary())
                    return -1 * cell->face(f)->boundary_id();

                //if crossed, get the corresponding neighbor as candidate
                Cell candidate_cell = cell->neighbor(f);
                // eliminate the case we are getting back to the same cell
                if (candidate_cell != previous_cell){
                    previous_cell = cell;
                    cell = candidate_cell;
                    break;
                }
            }
        }
        if (f == fpc) // all faces crossed without success.
            //Method failed. Try with more general cell search
            break;
//      if (searches > 5) // If too many cells crossed, particle displacement is too long.
//          //Code gets slower and if happens too frequently PIC is not accurate. Give warning
//          cout << "WARNING: cell search for point " << point << " with displacement " << displacement <<
//                  " crossed " << searches << " cells. Consider reducing pic timestep." << endl;
    }
    f = -2;

    cell = cell_orig;
    return locate_cell(point, ref_point, cell);
}

template<int dim>
int DealUtilities<dim>::locate_cell(const Point<dim> &point, Point<dim> & ref_point, Cell &cell) const {
    // if there is no clue about the cell, start immediately the full search
    if (cell != dof_handler.end()) {
        // if found in the present cell return without modifying the cell
        if (check_cell(point, ref_point, cell))
            return 1;

        const int max_searches = 70; //found to be optimal by trial and error

        // queue of cells to be checked
        queue<Cell> Q;
        Q.push(cell);

        // vector to keep all visited cells
        vector<unsigned> visited;
        visited.push_back(cell->index());

        for (int i = 0; i < max_searches; i++){
            if (i){//first is already checked, skip for speed
                if (check_cell(point, ref_point, Q.front())){
                    cell = Q.front();
                    return i + 2; // return the number of searches performed
                }
            }

            //get the neighbors
            vector<Cell> nbors;
            GridTools::get_active_neighbors<DoFHandler<dim>>(Q.front(), nbors);
            for (Cell& neighbor : nbors){ //iterate neighbors

                //first check if neighbor already in the queue
                bool cell_visited = false;
                for (unsigned cell_ind : visited){
                    if (cell_ind == neighbor->index()){
                        cell_visited = true;
                        break;
                    }
                }

                if (!cell_visited) {
                    Q.push(neighbor);    // put the cell in the list
                    visited.push_back(neighbor->index()); // and mark it as visited
                }
            }
            Q.pop(); //pop it out of the list
            if (Q.empty()) // if the list is empty everything is search. Try with DealII search
                break;
        }

        cerr << "WARNING: cell search for a point (" << point << ") failed. "
                << "Searching the whole triangulation!"<< endl;
    }

    // try to find the cell from the whole triangulation
    for (cell = this->dof_handler.begin_active(); cell != this->dof_handler.end(); ++cell)
        if (check_cell(point, ref_point, cell))
            return 0;
    cerr << "Searched the whole triangulation and could not find a cell "
            "for a point (" << point << ")\n";
    return -1;
}

template<int dim>
bool DealUtilities<dim>::line_cross_face(const Tensor<1,dim> &point, const Tensor<1,dim> &displacement,
        Cell cell, int f, double tol) const
{
    //calculate the face normal vector
    Tensor<1,dim> face_normal;
    Tensor<1,dim> v1 = cell->face(f)->vertex(1) - cell->face(f)->vertex(0);
    if (dim == 3){
        Tensor<1,dim> v2 = cell->face(f)->vertex(2) - cell->face(f)->vertex(0);
        face_normal = cross_product_3d(v2, v1);
    }else
        face_normal = cross_product_2d(v1);

    double dot_normal = (displacement * face_normal);
    double normality = abs(dot_normal) / (face_normal.norm() * displacement.norm());
    if(normality < 1.e-10) // displacement and face are parallel
        return false;

    double d = ((cell->face(f)->vertex(0) - point) * face_normal) / dot_normal;

    if (d < (-1. - tol) || d > tol)
        return false;

    Tensor<1,dim> intersect(point);
    intersect += d * displacement;


    return point_inside_face(intersect, cell, f);

}

template<int dim>
bool DealUtilities<dim>::point_inside_face(const Tensor<1,dim> &point, const Cell &cell,
        int f, double tol) const
{
    if (dim == 2){ //check if the inner product of the vectors from intersect to vertices is -1
        Tensor<1,dim> pv1 = (cell->face(f)->vertex(0) - point);
        Tensor<1,dim> pv2 = (cell->face(f)->vertex(1) - point);
        double costh = pv1 * pv2 / sqrt(pv1.norm_square() * pv2.norm_square());
        return  (abs(costh + 1.) < 1.e-5);
    } else { // check if all angles of rotation sum up to 2 * pi
        double angle = 0;
        for(int i = 0; i < GeometryInfo<dim>::lines_per_face; i++){
            Tensor<1,dim> pv1 = (cell->face(f)->line(i)->vertex(0) - point);
            Tensor<1,dim> pv2 = (cell->face(f)->line(i)->vertex(1) - point);
            double costh = pv1 * pv2 / sqrt(pv1.norm_square() * pv2.norm_square());
            costh =  abs(costh) < 1. ? costh : costh / abs(costh);
            angle += acos(costh);
        }
        return abs(angle - M_PI * 2) < 1.e-5;
    }
}

template<int dim>
void DealUtilities<dim>::get_face_normal(const Cell &cell, int face_index,
        Tensor<1, dim> &face_normal) const
{
    Tensor<1, dim> v1 = cell->face(face_index)->vertex(1) - cell->face(face_index)->vertex(0);
    if (dim == 3) {
        Tensor<1, dim> v2 = cell->face(face_index)->vertex(2) - cell->face(face_index)->vertex(0);
        face_normal = cross_product_3d(v2, v1);
    } else
        face_normal = cross_product_2d(v1);

    double dot = (cell->center() - cell->face(face_index)->center()) * face_normal;
    face_normal *= sign(dot);

    if (face_normal.norm() != 0.0)
        face_normal /= face_normal.norm();
}

template<int dim>
double DealUtilities<dim>::probe_solution(const Point<dim> &point, Cell cell) const {
    Point<dim> p_cell;
    bool inside = check_cell(point, p_cell, cell, 1.e-5);
    require(inside, "Point asked to probe solution out of mesh. Exiting");
    return probe_solution_unit(p_cell, cell);
}

template<int dim>
double DealUtilities<dim>::probe_solution_native(const Point<dim> &point, Cell cell) const {
    const Quadrature<dim> quadrature(GeometryInfo<dim>::project_to_unit_cell(point));
    FEValues<dim> fe_values(fe, quadrature, update_values);
    fe_values.reinit(cell);

    vector<double> value(1);
    fe_values.get_function_values(solution, value);
    return value[0];
}

template<int dim>
double DealUtilities<dim>::probe_solution_unit(const Point<dim> &upoint, Cell cell) const {
    static constexpr int n_dofs = GeometryInfo<dim>::vertices_per_cell;
    vector<unsigned int> dof_indices(n_dofs);
    cell->get_dof_indices(dof_indices);
    vector<double> shape_funs = this->shape_funs(upoint, cell);

    double interpolation = 0;
    for (int i = 0; i < n_dofs; ++i)
        interpolation += solution(dof_indices[i]) * shape_funs[i];

    return interpolation;
}

template<>
void DealUtilities<2>::precompute() {
    const uint n_cells = this->tria->n_active_cells();

    detA.resize(n_cells);
    detB.resize(n_cells);
    detC.resize(n_cells);
    bounding_box.resize(n_cells);

    // Loop through all the hexahedra
    for (Cell cell = this->dof_handler.begin_active(); cell != this->dof_handler.end(); ++cell) {
        int i = cell->active_cell_index();

        // The weird ordering of vertices comes from the difference in
        // vertex ordering in Deal.II and in a system provided by NASA.
        // The vertex ordering used in Deal.II is described in
        // https://www.dealii.org/current/doxygen/deal.II/structGeometryInfo.html
        const Point<2> x1 = cell->vertex(0);
        const Point<2> x2 = cell->vertex(1);
        const Point<2> x3 = cell->vertex(3);
        const Point<2> x4 = cell->vertex(2);

        Tensor<1,2> a1 = x1;
        Tensor<1,2> a2 = Tensor<1,2>(-x1 + x2);
        Tensor<1,2> a3 = Tensor<1,2>(-x1 + x4);
        Tensor<1,2> a4 = Tensor<1,2>(x1 - x2 + x3 - x4);

        detA[i] = { 2.0 * determinant(a4, a3), a1[0], a2[0], a3[0], a4[0] };
        detB[i] = { a4[1], -a4[0], determinant(a4, a1) + determinant(a2, a3) };
        detC[i] = { a2[1], -a2[0], determinant(a2, a1) };

        bounding_box[i] = cell->bounding_box();
    }
}

template<>
void DealUtilities<3>::precompute() {
    const uint n_cells = this->tria->n_active_cells();
    f0s.resize(n_cells);
    f1s.resize(n_cells);
    f2s.resize(n_cells);
    f3s.resize(n_cells);
    f4s.resize(n_cells);
    f5s.resize(n_cells);
    f6s.resize(n_cells);
    f7s.resize(n_cells);
    bounding_box.resize(n_cells);

    // Loop through all the hexahedra
    for (Cell cell = this->dof_handler.begin_active(); cell != this->dof_handler.end(); ++cell) {
        // The weird ordering of vertices comes from the difference in
        // vertex ordering in Deal.II and in a system provided by NASA.
        // The vertex ordering used in Deal.II is described in
        // https://www.dealii.org/current/doxygen/deal.II/structGeometryInfo.html
        const Point<3> x1 = cell->vertex(0);
        const Point<3> x2 = cell->vertex(1);
        const Point<3> x3 = cell->vertex(5);
        const Point<3> x4 = cell->vertex(4);
        const Point<3> x5 = cell->vertex(2);
        const Point<3> x6 = cell->vertex(3);
        const Point<3> x7 = cell->vertex(7);
        const Point<3> x8 = cell->vertex(6);

        int i = cell->active_cell_index();
        f0s[i] = Tensor<1,3>(( x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8) / 8.0);
        f1s[i] = Tensor<1,3>((-x1 + x2 + x3 - x4 - x5 + x6 + x7 - x8) / 8.0);
        f2s[i] = Tensor<1,3>((-x1 - x2 + x3 + x4 - x5 - x6 + x7 + x8) / 8.0);
        f3s[i] = Tensor<1,3>((-x1 - x2 - x3 - x4 + x5 + x6 + x7 + x8) / 8.0);
        f4s[i] = Tensor<1,3>(( x1 - x2 + x3 - x4 + x5 - x6 + x7 - x8) / 8.0);
        f5s[i] = Tensor<1,3>(( x1 - x2 - x3 + x4 - x5 + x6 + x7 - x8) / 8.0);
        f6s[i] = Tensor<1,3>(( x1 + x2 - x3 - x4 - x5 - x6 + x7 + x8) / 8.0);
        f7s[i] = Tensor<1,3>((-x1 + x2 - x3 + x4 + x5 - x6 + x7 - x8) / 8.0);

        bounding_box[i] = cell->bounding_box();
    }
}

template<int dim>
bool DealUtilities<dim>::check_cell(const Point<dim> &point, Point<dim> &ref_point,
         Cell &cell, double tol) const
{
    int cell_index = cell->index();
    require(cell_index < bounding_box.size(), "Invalid index: " + d2s(cell_index));
    if (!bounding_box[cell_index].point_inside(point))
        return false;

    ref_point = project_to_nat_coords(cell, point);
    return GeometryInfo<dim>::is_inside_unit_cell(ref_point, tol);
}

template<int dim>
Point<dim> DealUtilities<dim>::project_to_nat_coords_native(const Cell& cell,
        const Point<dim>& point) const
{
    return GeometryInfo<dim>::project_to_unit_cell(
            StaticMappingQ1<dim>::mapping.transform_real_to_unit_cell(cell, point));
}

/* The inspiration for mapping the 2D point was taken from
 * https://www.particleincell.com/2012/quad-interpolation/
 */
template<>
Point<2> DealUtilities<2>::project_to_nat_coords(const Cell& cell,
        const Point<2>& point) const
{
    int quad = cell->index();

    const auto &A = detA[quad];
    double a = A[0];
    double b = detB[quad].dot(point);
    double c = detC[quad].dot(point);

    double v = (-b + sqrt(b*b - 2.0*a*c)) / a;
    double u = (point[0] - A[1] - A[3] * v) / (A[2] + A[4] * v);
    return Point<2>(u, v);
}

/* The inspiration for mapping the 3D point was taken from
 * https://www.grc.nasa.gov/www/winddocs/utilities/b4wind_guide/trilinear.html
 */
template<>
Point<3> DealUtilities<3>::project_to_nat_coords(const Cell& cell,
        const Point<3>& point) const
{
    static constexpr int n_newton_iterations = 20;
    static constexpr double zero = 1e-15; ///< tolerance of calculations

    int hex = cell->index();

    double du, dv, dw;
    Tensor<1,3> f0 = point - f0s[hex];
    Tensor<1,3> f1 = f1s[hex];
    Tensor<1,3> f2 = f2s[hex];
    Tensor<1,3> f3 = f3s[hex];
    Tensor<1,3> f4 = f4s[hex];
    Tensor<1,3> f5 = f5s[hex];
    Tensor<1,3> f6 = f6s[hex];
    Tensor<1,3> f7 = f7s[hex];

    double D = determinant(f1, f2, f3);
    require(D != 0, "Invalid determinant: " + d2s(D));

    D  = 1.0 / D;
    double u = determinant(f0, f2, f3) * D;
    double v = determinant(f1, f0, f3) * D;
    double w = determinant(f1, f2, f0) * D;

    // In 3D, direct calculation of uvw is very expensive (not to say impossible),
    // because system of 3 nonlinear equations should be solved.
    // More efficient is to perform Newton iterations to calculate uvw approximately.

    // loop until the desired accuracy is met or # max iterations is done
    for (int i = 0; i < n_newton_iterations; ++i) {
        Tensor<1,3> f  = f0 - f1*u - f2*v - f3*w - f4*(u*v) - f5*(u*w) - f6*(v*w) - f7*(u*v*w);
        Tensor<1,3> fu = f1 + f4*v + f5*w + f7*(v*w);
        Tensor<1,3> fv = f2 + f4*u + f6*w + f7*(u*w);
        Tensor<1,3> fw = f3 + f5*u + f6*v + f7*(u*v);

        // solve the set of following linear equations for du, dv and dw
        //   | fu.x * du + fv.x * dv + fw.x * dw = f.x;
        //   | fu.y * du + fv.y * dv + fw.y * dw = f.y;
        //   | fu.z * du + fv.z * dv + fw.z * dw = f.z;

        D = determinant(fu, fv, fw);
        require(D != 0, "Invalid determinant: " + d2s(D));

        D  = 1.0 / D;
        double du = determinant(f, fv, fw) * D;
        double dv = determinant(fu, f, fw) * D;
        double dw = determinant(fu, fv, f) * D;

        u += du;
        v += dv;
        w += dw;

        if (du * du + dv * dv + dw * dw < zero)
            break;
    }

    // map the values from [-1, 1] to [0, 1]
    u += 1.0; v += 1.0; w += 1.0;
    u *= 0.5; v *= 0.5; w *= 0.5;

    // if the coordinate is within an error-margin outside of the box,
    // force its value into [0, 1] limits
    static constexpr double lower = -zero, upper = 1.0 + zero;

    if (u < 0 && u >= lower) u = 0.0;
    if (u > 1 && u <= upper) u = 1.0;

    if (v < 0 && v >= lower) v = 0.0;
    if (v > 1 && v <= upper) v = 1.0;

    if (w < 0 && w >= lower) w = 0.0;
    if (w > 1 && w <= upper) w = 1.0;

    // Not sure why we need to swap v & w,
    // but otherwise the results won't match with Deal.II ones
    return Point<3>(u, w, v);
}

template<int dim>
vector<double> DealUtilities<dim>::shape_funs_native(const Point<dim> &upoint, const Cell& cell) const {
    // create virtual quadrature point
    const Quadrature<dim> quadrature(upoint);

    // define fe_values object
    FEValues<dim> fe_values(fe, quadrature, update_values);
    fe_values.reinit(cell);

    // store shape functions
    vector<double> sfuns(fe.dofs_per_cell);
    for (unsigned i = 0; i < sfuns.size(); ++i)
        sfuns[i] = fe_values.shape_value(i, 0);

    return sfuns;
}

template<>
vector<double> DealUtilities<2>::shape_funs(const Point<2>& upoint, const Cell& cell) const {
    // map natural coordinates from [0, 1] to [-1, 1]
    double u = 2.0 * upoint[0] - 1;
    double v = 2.0 * upoint[1] - 1;

    // use natural coordinates to calculate shape functions
    return {
            (1 - u) * (1 - v) / 4.0,
            (1 + u) * (1 - v) / 4.0,
            (1 - u) * (1 + v) / 4.0,
            (1 + u) * (1 + v) / 4.0
    };
}

template<>
vector<double> DealUtilities<3>::shape_funs(const Point<3>& upoint, const Cell& cell) const {
    // map natural coordinates from [0, 1] to [-1, 1]
    double u = 2.0 * upoint[0] - 1;
    double v = 2.0 * upoint[1] - 1;
    double w = 2.0 * upoint[2] - 1;

    // use natural coordinates to calculate shape functions
    return {
            (1 - u) * (1 - v) * (1 - w) / 8.0,
            (1 + u) * (1 - v) * (1 - w) / 8.0,
            (1 - u) * (1 + v) * (1 - w) / 8.0,
            (1 + u) * (1 + v) * (1 - w) / 8.0,
            (1 - u) * (1 - v) * (1 + w) / 8.0,
            (1 + u) * (1 - v) * (1 + w) / 8.0,
            (1 - u) * (1 + v) * (1 + w) / 8.0,
            (1 + u) * (1 + v) * (1 + w) / 8.0
    };
}

template<int dim>
vector<Tensor<1,dim>> DealUtilities<dim>::shape_fun_grads_native(const Point<dim> &upoint, const Cell& cell) const {
    // create virtual quadrature point
    const Quadrature<dim> quadrature(upoint);

    // define fe_values object
    FEValues<dim> fe_values(fe, quadrature, update_gradients);
    fe_values.reinit(cell);

    // store shape function gradients
    vector<Tensor<1,dim>> sfun_grads(fe.dofs_per_cell);
    for (unsigned i = 0; i < sfun_grads.size(); ++i)
        sfun_grads[i] = fe_values.shape_grad(i, 0);

    return sfun_grads;
}

/* For more information, see the lecture materials in
 * https://www.colorado.edu/engineering/CAS/courses.d/AFEM.d/AFEM.Ch11.d/AFEM.Ch11.pdf
 */
template<>
vector<Tensor<1,2>> DealUtilities<2>::shape_fun_grads(const Point<2>& upoint, const Cell& cell) const {
    // map natural coordinates from [0, 1] to [-1, 1]
    double u = 2.0 * upoint[0] - 1.0;
    double v = 2.0 * upoint[1] - 1.0;

    // arrange cell vertices into a matrix
    vector<Point<2>> xyz = {
            cell->vertex(0), cell->vertex(1),
            cell->vertex(2), cell->vertex(3)
    };

    // calculate gradient of shape functions in uvw-space
    vector<Tensor<1,2>> dN = {
            Tensor<1,2>({-(1-v), -(1-u) }),
            Tensor<1,2>({ (1-v), -(1+u) }),
            Tensor<1,2>({-(1+v),  (1-u) }),
            Tensor<1,2>({ (1+v),  (1+u) }),
    };
    for (uint i = 0; i < 4; ++i)
        dN[i] *= 0.5;

    return shape_fun_grads(xyz, dN);
}

template<>
vector<Tensor<1,3>> DealUtilities<3>::shape_fun_grads(const Point<3>& upoint, const Cell& cell) const {
    // map natural coordinates from [0, 1] to [-1, 1]
    double u = 2.0 * upoint[0] - 1.0;
    double v = 2.0 * upoint[1] - 1.0;
    double w = 2.0 * upoint[2] - 1.0;

    // arrange cell vertices into a matrix
    vector<Point<3>> xyz = {
            cell->vertex(0), cell->vertex(1),
            cell->vertex(2), cell->vertex(3),
            cell->vertex(4), cell->vertex(5),
            cell->vertex(6), cell->vertex(7)
    };

    // calculate gradient of shape functions in uvw-space
    vector<Tensor<1,3>> dN = {
            Tensor<1,3>({-(1-v)*(1-w), -(1-u)*(1-w), -(1-u)*(1-v) }),
            Tensor<1,3>({ (1-v)*(1-w), -(1+u)*(1-w), -(1+u)*(1-v) }),
            Tensor<1,3>({-(1+v)*(1-w),  (1-u)*(1-w), -(1-u)*(1+v) }),
            Tensor<1,3>({ (1+v)*(1-w),  (1+u)*(1-w), -(1+u)*(1+v) }),
            Tensor<1,3>({-(1-v)*(1+w), -(1-u)*(1+w),  (1-u)*(1-v) }),
            Tensor<1,3>({ (1-v)*(1+w), -(1+u)*(1+w),  (1+u)*(1-v) }),
            Tensor<1,3>({-(1+v)*(1+w),  (1-u)*(1+w),  (1-u)*(1+v) }),
            Tensor<1,3>({ (1+v)*(1+w),  (1+u)*(1+w),  (1+u)*(1+v) })
    };
    for (uint i = 0; i < 8; ++i)
        dN[i] *= 0.25;

    return shape_fun_grads(xyz, dN);
}

template<int dim>
vector<Tensor<1,dim>> DealUtilities<dim>::shape_fun_grads(
        const vector<Point<dim>> &xyz, const vector<Tensor<1,dim>> &dN) const
{
    const int n_dofs = xyz.size();

    // calculate Jacobian
    Tensor<2,dim> jacobian;
    for (int k = 0; k < n_dofs; ++k)
        for (int i = 0; i < dim; ++i)
            jacobian[i] += xyz[k] * dN[k][i];

    // calculate inverse of Jacobian
    Tensor<2,dim> inverse_jacobian = invert(jacobian);

    // transpose the inverse of Jacobian
    for (int i = 0; i < dim-1; ++i)
        for (int j = i + 1; j < dim; ++j)
            std::swap(inverse_jacobian[i][j], inverse_jacobian[j][i]);

    // calculate gradient of shape functions in xyz-space
    vector<Tensor<1,dim>> sfg(n_dofs);
    for (int k = 0; k < n_dofs; ++k) {
        for (int i = 0; i < dim; ++i)
            sfg[k] += inverse_jacobian[i] * dN[k][i];
    }

    return sfg;
}

template<int dim>
inline double DealUtilities<dim>::determinant(const Tensor<1,dim> &v1,
        const Tensor<1,dim> &v2) const
{
    return v1[0] * v2[1] - v2[0] * v1[1];
}

template<int dim>
inline double DealUtilities<dim>::determinant(const Tensor<1,dim> &v1,
        const Tensor<1,dim> &v2, const Tensor<1,dim> &v3) const
{
    return v1[0] * (v2[1] * v3[2] - v3[1] * v2[2]) - v2[0] * (v1[1] * v3[2] - v3[1] * v1[2])
            + v3[0] * (v1[1] * v2[2] - v2[1] * v1[2]);
}

template class DealUtilities<2>;
template class DealUtilities<3>;

} /* namespace femocs */
