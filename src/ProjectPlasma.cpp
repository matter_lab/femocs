/*
 * ProjectPlasma.cpp
 *
 *  Created on: Jun 14, 2020
 *      Author: kyritsak
 */

#include "ProjectPlasma.h"
#include "Emission.h"
#include "CurrentHeatSolver.h"

namespace femocs {

template <int dim>
ProjectPlasma<dim>::ProjectPlasma(AtomReader &reader, Config &config) :
        GeneralProject(reader, config),
        heat_data(config.heating),
        pic(&emission, config, config.behaviour.rnd_seed),
        emission(&pic, config, NULL),
        ch_solver(&heat_data, &config.heating,
                emission.get_surface_heat(), emission.get_current_densities()),
        I_pic(config.scharge.N_fields), I_sc(config.scharge.N_fields),
        Flap(config.scharge.N_fields), Vappl(config.scharge.N_fields)

{
    if (conf.path.bulk_mesh_file != "") {
        heat_data.initialize();
        start_msg(t0, "Reading bulk mesh from " + conf.path.bulk_mesh_file + " and setting up emission");
        ch_solver.import_mesh(conf.path.bulk_mesh_file);
        ch_solver.setup(conf.heating.t_ambient, conf.behaviour.axisymmetry);
        emission.set_chsolver(&ch_solver);
        end_msg(t0);
    }

    start_msg(t0, "Reading vacuum mesh from " + conf.path.vacuum_mesh_file + " and setting up solvers");
    pic.import_mesh(conf.path.vacuum_mesh_file);
    pic.setup(conf.field.E0, conf.field.V0, conf.behaviour.axisymmetry);
    pic.write("mesh.msh", FileIO::no_update);
    emission.initialize();
    end_msg(t0);
}

template <int dim>
void ProjectPlasma<dim>::write_restart(const string& path_to_file) {
    if (conf.behaviour.n_restart <= 0 ||
             (GLOBALS.TIMESTEP - last_restart_ts) < conf.behaviour.n_restart)
        return;

    unsigned int flags = FileIO::force | FileIO::no_update;

    ch_solver.write(path_to_file, flags);

    pic.write(path_to_file, flags | FileIO::append);

    last_restart_ts = GLOBALS.TIMESTEP;
}

template <int dim>
int ProjectPlasma<dim>::restart(const string &path_to_file) {
    ifstream in(path_to_file);

    if (!in.good()) {
        write_verbose_msg("Restart file not found!");
        return 0;
    }

    start_msg(t0, "Reading restart file from " + path_to_file);

    ch_solver.read(path_to_file);

    pic.read(path_to_file);

    last_pic_time = GLOBALS.TIME - conf.behaviour.timestep_fs;
    last_heat_time = GLOBALS.TIME - conf.behaviour.timestep_fs;
    last_restart_ts = GLOBALS.TIMESTEP;
    end_msg(t0);

    return 0;
}

template <int dim>
int ProjectPlasma<dim>::run(const int timestep, const double time) {
    fail = ch_solver.run(conf.heating.delta_time, true);

    bool preheat = conf.heating.T_preheat > 0;
    const double dt = conf.heating.delta_time;
    int preheat_steps = 0;

    pic.run_step(0.0, true);
    pic.save_laplace(); // save probed laplace field

    // If needed, restore project data from restart file
    if (conf.path.restart_file != "")
        restart(conf.path.restart_file);

    if (preheat && !restarting) {
        write_verbose_msg("Running pic for dt=" + d2s(2*dt)
                + " fs to stabilize SC limited current");
        pic.run(2*dt, true);

        write_verbose_msg("Running pre-heating up to " + d2s(conf.heating.T_preheat, 1) + "K");
        for (int i = 0; i < 100000; i++) {
            fail = ch_solver.run(dt, i == 0);
            ++preheat_steps;
            if (ch_solver.heat.stat.sol_max > conf.heating.T_preheat)
                break;
        }
    }

    write_verbose_msg("Preheated for " + d2s(preheat_steps) + " steps");
    write_verbose_msg("Running synchronous PIC with heat");

    for (int i = 0; i < 100000; i++) {
        pic.run(dt, i == 0);
        fail = ch_solver.run(dt, i == 0);

        if (conf.path.restart_file != "")
            write_restart(conf.path.restart_file);

        if (GLOBALS.TIME > conf.behaviour.run_time)
            break;
    }
    return 0;
}

template class ProjectPlasma<2>;
template class ProjectPlasma<3>;

} /* namespace femocs */
