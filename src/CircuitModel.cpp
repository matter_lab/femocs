/*
 * CircuitModel.cpp
 *
 *  Created on: Sep 25, 2021
 *      Author: veske
 */

#include "CircuitModel.h"
#include "Macros.h"
#include "Globals.h"


using namespace std;
namespace femocs {

/* =====================================================================
 *                             CircuitModel
 * ===================================================================== */

CircuitModel::CircuitModel(const Config::CircuitModel *c) : conf(c) {
    init(0, 0);
}

void CircuitModel::init(double E0, double V0) {
    this->E0 = E0;
    this->V0 = V0;
    el_field = E0;
    V_gap = V0;
    I_gap = 0;
    I_circuit = 0;
}

CircuitModel* CircuitModel::load_circuit(const Config::CircuitModel *conf) {
    CircuitModel* model = NULL;

    if (conf->R_series <= 0 && conf->C_gap <= 0)
        model = new FixedVoltageCircuit(conf);
    else if (conf->C_gap <= 0)
        model = new ResistorCircuit(conf);
    else
        model = new ResistorCapacitorCircuit(conf);

    return model;
}

/* =====================================================================
 *                          FixedVoltageCircuit
 * ===================================================================== */

FixedVoltageCircuit::FixedVoltageCircuit(const Config::CircuitModel *conf) :
        CircuitModel(conf)
{}

void FixedVoltageCircuit::timestep(double delta_t, double I_gap) {}

/* =====================================================================
 *                           ResistorCircuit
 * ===================================================================== */

ResistorCircuit::ResistorCircuit(const Config::CircuitModel *conf) :
        CircuitModel(conf)
{}

void ResistorCircuit::timestep(double delta_t, double I_gap) {
    this->I_gap = I_gap;
    I_circuit = (V_gap - V0) / conf->R_series;
    V_gap = V0 - I_gap * conf->R_series;
}

/* =====================================================================
 *                        ResistorCapacitorCircuit
 * ===================================================================== */

ResistorCapacitorCircuit::ResistorCapacitorCircuit(const Config::CircuitModel *conf) :
        CircuitModel(conf)
{}

void ResistorCapacitorCircuit::timestep(double delta_t, double I_gap) {
    this->I_gap = I_gap;
    I_circuit = (V_gap - V0) / conf->R_series;
    V_gap += (-I_circuit - I_gap) * delta_t / (conf->C_gap * CONSTANTS.second);
    // ArcPIC version:
    // V_gap += (I_circuit - I_gap) / conf->C_gap;
    // I_circuit = (conf->V0 - V_gap) / conf->R_series;
}

}  // namespace femocs
