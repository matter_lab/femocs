/*
 * Collisions.cpp
 *
 *  Created on: 17 Jun 2020
 *      Author: koironi
 */

#include "Collisions.h"
#include "PicSolver.h"
#include "PicData.h"
#include "ParticleSet.h"

namespace femocs {

template<int dim>
Collisions<dim>::Collisions(PicSolver<dim> *solver, Neutrals<dim> &neutrals,
        Electrons<dim> &electrons, Ions<dim> &ions, const PicData &pic_data,
        const Config::PIC &conf) :
        solver(solver), pic_data(&pic_data), conf(&conf), neutrals(neutrals), electrons(
                electrons), ions(ions), system_energy(0), timestep(0) {
}

template<int dim>
void Collisions<dim>::check_energy_and_momentum(const string &type) {
    // check conservation of energy
    double prev_sys_E = system_energy;
    system_energy = solver->total_energy();

    double delta = system_energy - prev_sys_E;
    if (abs(delta) > E_epsilon)
        write_silent_msg(type + " energy not conserved; dE = " + d2s(delta));

    // check conservation of momentum
    Vec3 prev_sys_p = system_momentum;
    system_momentum = solver->total_momentum();

    Vec3 delta_p = system_momentum - prev_sys_p;
    delta = delta_p.norm();
    if (delta > p_epsilon)
        write_silent_msg(type + " momentum not conserved; dp = " + d2s(delta));
}

template<int dim>
void Collisions<dim>::run(double tstep) {
    timestep = tstep;

    if (conf->merging) {
        system_momentum = solver->total_momentum();
        system_energy = solver->total_energy();

        merge_particles(electrons);
        merge_particles(neutrals);
        for (auto &ion : ions)
            merge_particles(ion);

        check_energy_and_momentum("merging");
    }

    system_momentum = solver->total_momentum();
    system_energy = solver->total_energy();

    if (conf->collide_elastic)
        collide(nn_elastic, neutrals);
    check_energy_and_momentum("nn elastic");

    if (conf->collide_elastic)
        collide(en_elastic, electrons, neutrals, NULL, 0);
    check_energy_and_momentum("en elastic");

    if (conf->collide_ee)
        collide(coulomb, electrons);
    check_energy_and_momentum("ee coulomb");

    if (conf->collide_coulomb) {
        for (auto &ion1 : ions) {
            for (auto &ion2 : ions) {
                collide(coulomb, ion1, ion2, NULL, 0);
            }
        }
    }
    check_energy_and_momentum("ion coulomb");

    if (conf->collide_ionization) {
        int level = 1;
        for (auto &ion : ions) {
            int level2 = 1;
            for (auto &ion2 : ions) {
                if (level2 <= level) {
                    level2++;
                    continue;
                }

                collide(ei_ionization, electrons, ion, &ion2, level2++);
            }
            ++level;
        }
        check_energy_and_momentum("ei ionization");

        level = 1;
        for (auto &ion : ions) {
            collide(en_ionization, electrons, neutrals, &ion, level++);
        }
        check_energy_and_momentum("en ionization");
    }

    if (conf->collide_exchange) {
        auto ion = ions.begin();
        collide(exchange, neutrals, *ion, NULL, 0);
    }
    check_energy_and_momentum("exchange");

    if (conf->collide_recombination) {
        int level = 1;
        auto ion2 = ions.begin();
        for (auto ion = ions.begin(); ion != ions.end(); ++ion) {
            collide(recombination, electrons, *ion, &(*ion2), level++);
            ion2 = ion;
        }
    }

    if (solver->write_time()) {
        printf("  collisions: elastic=%llu, coulomb=%llu, en_ionization=%llu, "
                "ei_ionization=%llu, recombination=%llu, exchange=%llu\n",
                elastic_num, coulomb_num, en_ionization_num, ei_ionization_num,
                recombination_num, exchange_num);
    }
}

template<int dim>
void Collisions<dim>::shuffle(vector<int> &indices) const {
    // no need to shuffle <= 2 particles, as their pairing doesn't change
    int N = indices.size();
    if (N < 3)
        return;

    uniform_int_distribution<int> rnd_int(0, N - 1);

    for (int &p : indices) {
        int new_index = rnd_int(solver->mersenne);
        swap(p, indices[new_index]);
    }
}

template<int dim>
void Collisions<dim>::group_shuffle(const ParticleSet<dim> &parts1,
        const ParticleSet<dim> &parts2, vector<vector<int>> &inds1,
        vector<vector<int>> &inds2) const {
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;

    for (const Cell &cell1 : parts1.filled_cells) {
        for (const Cell &cell2 : parts2.filled_cells) {
            if (cell1->index() != cell2->index())
                continue;

            vector<int> p1_particles = parts1.parts_at(cell1);
            vector<int> p2_particles = parts2.parts_at(cell2);
            if (p1_particles.size() >= p2_particles.size())
                shuffle(p1_particles);
            else
                shuffle(p2_particles);

            inds1.push_back(p1_particles);
            inds2.push_back(p2_particles);
        }
    }
}

template<int dim>
void Collisions<dim>::collide(int mode, ParticleSet<dim> &particle_set) {
    typedef typename DoFHandler<dim>::active_cell_iterator Cell;

    for (const Cell &cell : particle_set.filled_cells) {
        vector<int> particles = particle_set.parts_at(cell);

        //shuffle vector
        shuffle(particles);
        size_t n_parts = particles.size();
        if (n_parts <= 1)
            continue;

        // particle density
        double density = (n_parts-1) / solver->get_cell_vol(cell);

        size_t start_p = 0;

        // for odd number of particles, perform first three collisions differently
        if (n_parts % 2) {
            start_p = 3;
            int p1 = particles[0];
            int p2 = particles[1];
            int p3 = particles[2];

            if (mode & coulomb) {
                collide_pair_coulomb(p1, p2, 0.5 * density, particle_set,
                        particle_set);
                collide_pair_coulomb(p1, p3, 0.5 * density, particle_set,
                        particle_set);
                collide_pair_coulomb(p2, p3, 0.5 * density, particle_set,
                        particle_set);
            }
            if (mode & nn_elastic) {
                collide_nn_pair_elastic(p1, p2, 0.5 * density);
                collide_nn_pair_elastic(p2, p3, 0.5 * density);
                collide_nn_pair_elastic(p2, p3, 0.5 * density);
            }
        }

        // perform pair-wise collision
        for (size_t p = start_p; p < n_parts; p += 2) {
            int p1 = particles[p];
            int p2 = particles[p + 1];

            if (mode & coulomb)
                collide_pair_coulomb(p1, p2, density, particle_set,
                        particle_set);
            if (mode & nn_elastic)
                collide_nn_pair_elastic(p1, p2, density);
        }
    }
}

template<int dim>
void Collisions<dim>::collide(int mode, ParticleSet<dim> &particle_set1,
        ParticleSet<dim> &particle_set2, ParticleSet<dim> *destination,
        int level) {
    vector<vector<int>> inds1, inds2;
    group_shuffle(particle_set1, particle_set2, inds1, inds2);
    require(inds1.size() == inds2.size(), "pairing vectors have wrong sizes");

    for (int i = 0; i < inds1.size(); i++) {
        size_t p1n = inds1[i].size();
        size_t p2n = inds2[i].size();
        if (p1n < 1 || p2n < 1)
            continue;

        int minsize = min(p1n, p2n);
        double vol = solver->get_cell_vol(particle_set1.cells[inds1[i][0]]);

        // particle density (largest)
        double density = max(p1n, p2n) / vol;

        // perform pair-wise collision
        for (int j = 0; j < minsize; j++) {
            int p1 = inds1[i][j];
            int p2 = inds2[i][j];

            if (particle_set1.weights[p1] == 0.
                    || particle_set2.weights[p2] == 0.)
                continue;

            if (mode & nn_elastic) // select collision type based on mode flag
                collide_nn_pair_elastic(p1, p2, density);
            if (mode & en_elastic)
                collide_en_pair_elastic(p1, p2, density);
            if (mode & coulomb)
                collide_pair_coulomb(p1, p2, density, particle_set1,
                        particle_set2);
            if (mode & en_ionization)
                collide_en_pair_ionize(p1, p2, density, *destination, level);
            if (mode & ei_ionization)
                collide_ei_pair_ionize(p1, p2, density, particle_set2,
                        *destination, level);
            if (mode & exchange)
                collide_pair_exchange(p1, p2, density, particle_set2);
            if (mode & recombination)
                collide_pair_recombine(p1, p2, density, particle_set2,
                        *destination, level);
        }
    }
}

template<int dim>
void Collisions<dim>::collide_pair_coulomb(int p1, int p2, double density,
        ParticleSet<dim> &particle_set1, ParticleSet<dim> &particle_set2) {
    // Relative velocity
    Vec3 v_rel = particle_set1.velocities[p1] - particle_set2.velocities[p2];
    double v_rel_norm = v_rel.norm();

    // If u->0, the relative change might be big,
    // but it's a big change on top of nothing => SKIP
    if (v_rel_norm < 1.e-10)
        return;

    double m1 = particle_set1.get_Msp(p1);
    double m2 = particle_set2.get_Msp(p2);
    double effm = m1 * m2 / (m1 + m2); // effective mass

    // Use MC to find the scattering angle, through transformation of delta
    // which is is distributed with P(delta) = Gauss(0, <delta^2>)
    double variance_factor = particle_set1.get_Csp(p1)
            * particle_set2.get_Csp(p2) * density * timestep * conf->landau_log;
    variance_factor /= 4. * CONSTANTS.twopi * CONSTANTS.eps0 * CONSTANTS.eps0
            * effm * effm * v_rel_norm * v_rel_norm * v_rel_norm;
    std::normal_distribution<double> rnd_gauss(0.0, sqrt(variance_factor));

    double delta = rnd_gauss(solver->mersenne);     // scattering angle
    double phi = CONSTANTS.twopi * solver->rnd();   // azimuth angle
    Vec3 v_delta = delta_v(v_rel, delta, phi, 1.0);

    // Update the particle velocities
    particle_set1.velocities[p1] += v_delta * effm / m1;
    particle_set2.velocities[p2] -= v_delta * effm / m2;

    ++coulomb_num;
}

template<int dim>
void Collisions<dim>::collide_nn_pair_elastic(int p1, int p2, double density) {
    // Relative velocity
    Vec3 v_rel = neutrals.velocities[p1] - neutrals.velocities[p2];
    double effm = 0.5 * neutrals.mass0; // effective mass for single particles
    double E0 = effm * v_rel.norm_square() / 2.0;
    double cs = pic_data->get_nn_elastic_cs(E0); // single particle collision cross section

    collide_pair_elastic(p1, p2, density, cs, neutrals, neutrals);
}

template<int dim>
void Collisions<dim>::collide_en_pair_elastic(int p1, int p2, double density) {
    // Relative velocity
    Vec3 v_rel = electrons.velocities[p1] - neutrals.velocities[p2];
    double me = electrons.mass0;
    double mn = neutrals.mass0;
    double effm = me * mn / (me + mn); // effective mass for single particles
    double E0 = effm * v_rel.norm_square() / 2.0;
    double cs = pic_data->get_en_elastic_cs(E0); // single particle collision cross section

    collide_pair_elastic(p1, p2, density, cs, electrons, neutrals);
}

template<int dim>
void Collisions<dim>::merge_particles(ParticleSet<dim> &particle_set) {
    if (conf->density_max <= 0.) // merging disabled
        return;

    typedef typename DoFHandler<dim>::active_cell_iterator Cell;

    for (const Cell &cell : particle_set.filled_cells) {
        vector<int> particles = particle_set.parts_at(cell);

        size_t n_parts = particles.size();
        if (n_parts <= 1)
            continue;

        // particle density
        double density = n_parts / solver->get_cell_vol(cell);

        if (density <= conf->density_max) // check if density exceeded in cell
            continue;

        //shuffle vector
        shuffle(particles);

        // merge pairwise
        for (size_t p = 0; p + 1 < n_parts; p += 2) {
            int p1 = particles[p];
            int p2 = particles[p + 1];

            double w1 = particle_set.weights[p1];
            double w2 = particle_set.weights[p2];

            if (w1 + w2 > conf->weight_max) // don't merge with weight exceeding maximum
                continue;

            double effm = w1 * w2 / (w1 + w2);

            particle_set.weights[p1] = w1 + w2; // combined superparticle
            particle_set.weights[p2] = 0.;

            particle_set.positions[p1] = particle_set.positions[p1] * effm / w2
                    + particle_set.positions[p2] * effm / w1; // center of mass
            particle_set.velocities[p1] = particle_set.velocities[p1] * effm
                    / w2 + particle_set.velocities[p2] * effm / w1; // weighted average conserves momentum
        }
    }

    particle_set.clear_lost(); // remove other of merged particles
}

template<int dim>
bool Collisions<dim>::split_particle(int i, ParticleSet<dim> &particle_set,
        double p) {
    double w = particle_set.get_Wsp(i); // superparticle weight

    if (!conf->splitting || conf->ionize_all)
        return solver->rnd() < pow(p, w); // ionize whole SP, binomial probability

    double nparts = ceil(w / conf->weight_min); // number of subparticles
    int wp = w / nparts; // weight of one subparticle

    double ncoll = (double) random_count(p, (int) nparts) * wp; // number of collisions / weight of colliding particle

    if (ncoll < conf->weight_min) // if weight of produced particle less than minimum, consider no collision to happen
        return false;

    Particle<dim> particle_nc(particle_set[i]); // noncolliding particles

    particle_nc.weight = w - ncoll; // calculate weights of noncolliding particles

    if (particle_nc.weight > 0.) // do we have noncolliding particles?
        particle_set.inject(particle_nc);

    particle_set.weights[i] = ncoll; // colliding particle weight = number of collisions

    return true;
}

template<int dim>
bool Collisions<dim>::split_particles(int p1, int p2, double vf0,
        ParticleSet<dim> &particle_set1, ParticleSet<dim> &particle_set2) {
    double w1 = particle_set1.get_Wsp(p1); // superparticle weights
    double w2 = particle_set2.get_Wsp(p2);

    if (!conf->splitting)
        return random_coll(1., w1 * w2 * vf0, 1.) && w1 == w2; // require weights to be equal

    double ncoll;

    if (conf->ionize_all) { // collide with maximum weight
        ncoll = min(w1, w2);

        if (!random_coll(1., ncoll * ncoll * vf0, 1.)) // check if collision occcurs with weight ncoll
            return false;
    } else {
        ncoll = random_ncoll(vf0, w1, w2, conf->weight_min); // number of collisions / weight of colliding particle

        if (ncoll < conf->weight_min) // if weight of produced particle less than minimum, consider no collision to happen
            return false;
    }

    Particle<dim> particle1_nc(particle_set1[p1]); // noncolliding particles
    Particle<dim> particle2_nc(particle_set2[p2]);

    particle1_nc.weight = w1 - ncoll; // calculate weights of noncolliding particles
    particle2_nc.weight = w2 - ncoll;

    if (particle1_nc.weight > 0.) // do we have noncolliding particles?
        particle_set1.inject(particle1_nc);
    if (particle2_nc.weight > 0.)
        particle_set2.inject(particle2_nc);

    particle_set1.weights[p1] = ncoll; // colliding particle weight = number of collisions
    particle_set2.weights[p2] = ncoll;

    return true;
}

template<int dim>
void Collisions<dim>::collide_pair_elastic(int p1, int p2, double density,
        double cs, ParticleSet<dim> &particle_set1,
        ParticleSet<dim> &particle_set2) {
    // Relative velocity
    Vec3 v_rel = particle_set1.velocities[p1] - particle_set2.velocities[p2];
    double v_rel_norm = v_rel.norm();

    // If u->0, the relative change might be big,
    // but it's a big change on top of nothing => SKIP
    if (v_rel_norm < 1.e-10)
        return;

    if (!random_coll(v_rel_norm, timestep * density, cs))
        return;

    double m1 = particle_set1.get_Msp(p1); // superparticle mass = weight * m0
    double m2 = particle_set2.get_Msp(p2);
    double effm = m1 * m2 / (m1 + m2); // superparticle effective mass

    Vec3 v_delta = delta_v(v_rel);
    Vec3 v1 = particle_set1.velocities[p1] + v_delta * effm / m1; // velocities after elastic collision, conserve momentum
    Vec3 v2 = particle_set2.velocities[p2] - v_delta * effm / m2;

    particle_set1.velocities[p1] = v1;
    particle_set2.velocities[p2] = v2;

    ++elastic_num;
}

template<int dim>
bool Collisions<dim>::collide_pair_ionize(int mode, int p1, int p2,
        double density, ParticleSet<dim> &particle_set1,
        ParticleSet<dim> &particle_set2, int level, Vec3 &v1, Vec3 &v2) {
    // Relative velocity
    Vec3 v_rel = particle_set1.velocities[p1] - particle_set2.velocities[p2];
    double v_rel_norm = v_rel.norm();

    // If u->0, the relative change might be big,
    // but it's a big change on top of nothing => SKIP
    if (v_rel_norm < 1.e-10)
        return true;

    double effm0 = particle_set1.mass0 * particle_set2.mass0
            / (particle_set1.mass0 + particle_set2.mass0); // single pair effective mass
    double E0 = effm0 * v_rel_norm * v_rel_norm / 2.0; // single pair energy

    double cs = 0; // cross section for single particle collision
    if (mode & ei_ionization)
        cs = pic_data->get_ion_ionization_cs(E0, level);
    if (mode & en_ionization)
        cs = pic_data->get_neutral_ionization_cs(E0, level);

    if (!split_particles(p1, p2, v_rel_norm * timestep * cs * density,
            particle_set1, particle_set2))
        return false;

    double m1 = particle_set1.get_Msp(p1); // superparticle mass = weight * m0
    double m2 = particle_set2.get_Msp(p2);
    double effm = m1 * m2 / (m1 + m2); // superparticle effective mass
    double E = effm * v_rel_norm * v_rel_norm / 2.0;
    double Ei = 0;
    if (mode & ei_ionization) // ionization of ion
        Ei = pic_data->get_ionization_energy(level) * particle_set2.get_Wsp(p2);
    if (mode & en_ionization) // ionization of neutral, sum energy
        Ei = pic_data->sum_ionization_energy(level) * particle_set2.get_Wsp(p2);

    if (E < Ei) // not enough energy to ionize
        return false;

    double w = sqrt(1.0 - Ei / E); // scaling factor from energy
    Vec3 v_delta = delta_v(v_rel, w);

    v1 = particle_set1.velocities[p1] + v_delta * effm / m1; // conserve momentum
    v2 = particle_set2.velocities[p2] - v_delta * effm / m2;

    return true;
}

template<int dim>
void Collisions<dim>::collide_ei_pair_ionize(int p1, int p2, double density,
        ParticleSet<dim> &ion, ParticleSet<dim> &ions, int level) {
    Vec3 vep, vip;
    // do splitting, determine whether collision occurs
    bool collides = collide_pair_ionize(ei_ionization, p1, p2, density,
            electrons, ion, level, vep, vip);
    if (!collides)
        return;

    double me = electrons.get_Msp(p1); // superparticle mass = weight * m0
    double w2 = ion.get_Wsp(p2);

    electrons.velocities[p1] = vep;

    Particle<dim> new_electron(electrons[p1]);
    new_electron.vel = vip;
    new_electron.weight = w2;
    double me2 = electrons.mass0 * w2; // new electron mass

    Vec3 u2 = electrons.velocities[p1] - new_electron.vel;
    Vec3 u2_delta = delta_v(u2);

    double eeffm = me * me2 / (me + me2); // impacting electron - new electron effective mass
    new_electron.vel -= u2_delta * eeffm / me2; // elastic collision with impacting electron
    electrons.velocities[p1] += u2_delta * eeffm / me;

    electrons.inject(new_electron);

    Particle<dim> new_ion(ion[p2]);
    new_ion.vel = vip;
    ions.inject(new_ion);

    ion.weights[p2] = 0.;

    ++ei_ionization_num;
}

template<int dim>
void Collisions<dim>::collide_en_pair_ionize(int p1, int p2, double density,
        ParticleSet<dim> &ions, int level) {
    Vec3 vep, vnp;
    // do splitting, determine whether collision occurs
    bool collides = collide_pair_ionize(en_ionization, p1, p2, density,
            electrons, neutrals, level, vep, vnp);
    if (!collides)
        return;

    double me = electrons.get_Msp(p1); // superparticle mass = weight * m0
    double w2 = neutrals.get_Wsp(p2);

    electrons.velocities[p1] = vep;

    for (int i = 1; i <= level; ++i) { // electrons created in ionization
        Particle<dim> new_electron(electrons[p1]);
        new_electron.vel = vnp;
        new_electron.weight = w2;
        double me2 = electrons.mass0 * w2; // new electron mass

        Vec3 u2 = electrons.velocities[p1] - new_electron.vel;
        Vec3 u2_delta = delta_v(u2);

        double eeffm = me * me2 / (me + me2); // impacting electron - new electron effective mass
        new_electron.vel -= u2_delta * eeffm / me2; // elastic collision with impacting electron
        electrons.velocities[p1] += u2_delta * eeffm / me;

        electrons.inject(new_electron);
    }

    Particle<dim> new_ion(neutrals[p2]);
    new_ion.vel = vnp;
    ions.inject(new_ion); // inject ion created in collision

    neutrals.weights[p2] = 0.; // remove neutral

    ++en_ionization_num;
}

template<int dim>
void Collisions<dim>::collide_pair_exchange(int p1, int p2, double density,
        ParticleSet<dim> &ions) {
    // Relative velocity
    Vec3 v_rel = neutrals.velocities[p1] - ions.velocities[p2];
    double v_rel_norm = v_rel.norm();

    // If u->0, the relative change might be big,
    // but it's a big change on top of nothing => SKIP
    if (v_rel_norm < 1.e-10)
        return;

    double effm0 = neutrals.mass0 * ions.mass0 / (neutrals.mass0 + ions.mass0); // single particle effective mass
    double E0 = effm0 * v_rel_norm * v_rel_norm / 2.0;

    double cs = pic_data->get_exchange_cs(E0); // cross section

    // does collision occur?
    if (!split_particles(p1, p2, v_rel_norm * timestep * cs * density, neutrals,
            ions))
        return;

    Vec3 vn = neutrals.velocities[p1];
    Vec3 vi = ions.velocities[p2];
    double vn_norm = vn.norm();
    double vi_norm = vi.norm();

    double mn = neutrals.get_Msp(p1);
    double mi = ions.get_Msp(p2);
    double effm = mi * mn / (mi + mn); // superparticle effective mass
    Vec3 vcm = vn * effm / mi + vi * effm / mn; // center of mass velocity
    double vcm_norm = vcm.norm();
    double Ecm = (mn * vn_norm * vn_norm + mi * vi_norm * vi_norm
            - (mn + mi) * vcm_norm * vcm_norm) / 2.0; // center of mass energy

    Vec3 v_delta = rand_v(sqrt(2.0 * Ecm / effm)); // random direction with magnitude from Ecm

    Vec3 v1 = vcm + v_delta * effm / mn; // conserve momentum
    Vec3 v2 = vcm - v_delta * effm / mi;

    neutrals.velocities[p1] = v1;
    ions.velocities[p2] = v2;

    Tensor<1, dim> ps1_pos = neutrals.positions[p1]; // switch particles
    neutrals.positions[p1] = ions.positions[p2];
    ions.positions[p2] = ps1_pos;

    ++exchange_num;
}

template<int dim>
void Collisions<dim>::collide_pair_recombine(int p1, int p2, double density,
        ParticleSet<dim> &ion, ParticleSet<dim> &ions, int level) {
    // Relative velocity
    Vec3 v_rel = electrons.velocities[p1] - ion.velocities[p2];
    double v_rel_norm = v_rel.norm();

    // If u->0, the relative change might be big,
    // but it's a big change on top of nothing => SKIP
    if (v_rel_norm < 1.e-10)
        return;

    double me = electrons.mass0;
    double mi = ion.mass0;
    double effm = mi * me / (mi + me); // effective mass of one pair
    double E0 = effm * v_rel_norm * v_rel_norm / 2.0;

    double cs = pic_data->get_recombination_cs(E0, level); // cross section

    // does collision occur?
    if (!random_coll(v_rel_norm,
            electrons.weights[p1] * ions.weights[p2] * timestep * density, cs))
        return;

    Vec3 vn = ion.velocities[p2]; // the electron energy and momentum is lost to a photon

    if (electrons.weights[p1] < level) { // electron can't recombine whole ion, inject ion
        Particle<dim> new_ion(ion[p2]);
        new_ion.vel = vn;
        ions.inject(new_ion);
    } else { // recombine whole ion, inject neutral
        Particle<dim> new_neutral(ion[p2]);
        new_neutral.vel = vn;
        neutrals.inject(new_neutral);
    }

    if (electrons.weights[p1] > level) // subtract required number of electrons from weight
        electrons.weights[p1] -= level;
    else
        electrons.weights[p1] = 0.; // remove electron

    ion.weights[p2] = 0.; // remove ion

    ++recombination_num;
}

template<int dim>
Vec3 Collisions<dim>::delta_v(const Vec3 &v_rel, double w) const {
    double delta = acos(1.0 - 2.0 * solver->rnd()); // scattering angle
    double phi = CONSTANTS.twopi * solver->rnd();   // azimuth angle
    return delta_v(v_rel, delta, phi, w);
}

template<int dim>
Vec3 Collisions<dim>::rand_v(double u) const {
    double delta = acos(1.0 - 2.0 * solver->rnd()); // scattering angle
    double phi = CONSTANTS.twopi * solver->rnd();   // azimuth angle

    Vec3 v;
    v[0] = u * cos(phi) * sin(delta);
    v[1] = u * sin(delta) * sin(phi);
    v[2] = u * cos(delta);

    return v;
}

template<int dim>
Vec3 Collisions<dim>::delta_v(const Vec3 &v_rel, double delta, double phi,
        double w) const {
    double sin_phi = sin(phi);
    double cos_phi = cos(phi);
    double sin_delta = sin(delta);
    double cos_delta = cos(delta);

    double vr = v_rel.norm();
    if (vr == 0.)
        return Vec3(0.);

    double vphi = atan2(v_rel[1], v_rel[0]); // rotate in relative velocity coordinate frame
    double vdelta = acos(v_rel[2] / vr);
    double sin_vphi = sin(vphi);
    double cos_vphi = cos(vphi);
    double sin_vdelta = sin(vdelta);
    double cos_vdelta = cos(vdelta);

    Vec3 v_rot; // velocity in laboratory frame, scaled by w
    v_rot[0] = w * cos_phi * sin_delta * cos_vdelta * cos_vphi
            + (w * cos_delta - 1.) * cos_vphi * sin_vdelta
            - w * sin_delta * sin_phi * sin_vphi;
    v_rot[1] = w * sin_delta * sin_phi * cos_vphi
            + w * cos_phi * sin_delta * cos_vdelta * sin_vphi
            + (w * cos_delta - 1.) * sin_vdelta * sin_vphi;
    v_rot[2] = (w * cos_delta - 1.) * cos_vdelta
            - w * cos_phi * sin_delta * sin_vdelta;
    v_rot *= vr;

    return v_rot;
}

template class Collisions<2> ;
template class Collisions<3> ;

}  // namespace femocs
