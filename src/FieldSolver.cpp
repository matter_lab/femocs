/*
 *  Created on: 27 Mar 2021
 *      Author: veske
 */

#include "FieldSolver.h"
#include "Config.h"
#include "CircuitModel.h"

#include <deal.II/grid/grid_tools.h>
#include <deal.II/numerics/data_out.h>

namespace femocs {

// ----------------------------------------------------------------------------------------
/* Class for outputting the field distribution
 * being calculated from potential distribution */
template <int dim>
class LaplacePostProcessor : public DataPostprocessorVector<dim> {
public:
    LaplacePostProcessor() : DataPostprocessorVector<dim>("field", update_gradients) {}

    virtual void evaluate_scalar_field(
        const DataPostprocessorInputs::Scalar<dim> &input_data,
        std::vector<Vector<double>> &computed_quantities) const override
    {
        AssertDimension(input_data.solution_gradients.size(),
                        computed_quantities.size());

        for (unsigned int p = 0; p < input_data.solution_gradients.size(); ++p) {
            AssertDimension(computed_quantities[p].size(), dim);
            for (unsigned int d = 0; d < dim; ++d)
                computed_quantities[p][d] = input_data.solution_gradients[p][d];
        }
    }

    void
    compute_derived_quantities_scalar (
            const vector<double>             &/*uh*/,
            const vector<Tensor<1,dim> >     &duh,
            const vector<Tensor<2,dim> >     &/*dduh*/,
            const vector<Point<dim> >        &/*normals*/,
            const vector<Point<dim> >        &/*evaluation_points*/,
            vector<Vector<double> >          &computed_quantities) const {
        for (unsigned int i=0; i<computed_quantities.size(); i++) {
            for (unsigned int d=0; d<dim; ++d)
                computed_quantities[i](d) = duh[i][d];

        }
    }
};
// ----------------------------------------------------------------------------

template<int dim>
FieldSolver<dim>::FieldSolver(const Config& config, unsigned int seed) :
        DealSolver<dim>(), conf(&config), circuit(NULL),
        mersenne{seed}
{
    circuit = CircuitModel::load_circuit(&config.circuit_model);
}

template<int dim>
FieldSolver<dim>::~FieldSolver() {
    if (circuit) delete circuit;
}

template<int dim>
void FieldSolver<dim>::mark_mesh() {
    this->mark_boundary(BoundaryID::vacuum_top, BoundaryID::copper_surface,
            BoundaryID::vacuum_sides, BoundaryID::copper_surface);
}

template<int dim>
double FieldSolver<dim>::get_face_bc(const unsigned int face) const {
    return -circuit->elfield();
}

template<int dim>
void FieldSolver<dim>::setup(const double field, const double potential, bool axisymmetry, bool full) {
    if (full)
        DealSolver<dim>::setup_system(axisymmetry);

    circuit->init(field, potential);
    bbox = GridTools::compute_bounding_box(this->triangulation);
}

template<int dim>
void FieldSolver<dim>::assemble_laplace(bool full_run) {
    require(conf, "NULL conf can't be used!");
    require(conf->field.anode_BC == "neumann" || conf->field.anode_BC == "dirichlet",
            "Unimplemented anode BC: " + conf->field.anode_BC);

    if (full_run) this->system_matrix = 0;
    this->system_rhs = 0;

    if (full_run)
        this->assemble_parallel();
    else
        this->restore_system_matrix();

    if (conf->field.anode_BC == "neumann") {
        this->append_dirichlet(BoundaryID::copper_surface, this->dirichlet_bc_value);
        this->assemble_rhs(BoundaryID::vacuum_top);
    } else {
        this->append_dirichlet(BoundaryID::copper_surface, this->dirichlet_bc_value);
        this->append_dirichlet(BoundaryID::vacuum_top, this->circuit->Vgap());
    }
}

template<int dim>
void FieldSolver<dim>::assemble_finalize() {
    // save charge density for writing it to file
    // must be before applying Diriclet BCs
    if (this->write_time()) {
        this->charge_density = this->system_rhs;
        this->calc_dof_volumes();
        for (auto dof : this->vertex2dof)
            this->charge_density[dof] /= (CONSTANTS.e_over_eps0 * this->dof_volume[dof]);

    } else
        this->charge_density.reinit(this->system_rhs.size());

    this->apply_dirichlet();
}

template<int dim>
void FieldSolver<dim>::probe_field(const vector<Point<dim>> &points,
        const vector<Cell> &cells, vector<Tensor<1,dim>> &fields) const
{
    const uint n_points = points.size();
    vector<Point<dim>> unit_points(n_points);
    for (uint i = 0; i < n_points; ++i)
        unit_points[i] = this->utilities.project_to_nat_coords(cells[i], points[i]);

    if (this->shape_degree == 1) {
        probe_field_fast(unit_points, cells, fields);
        return;
    }

    const uint n_dofs = this->fe.dofs_per_cell;
    vector<uint> dof_indices(n_dofs);

    const Quadrature<dim> quadrature(unit_points);
    FEValues<dim> fe_values(this->fe, quadrature, update_gradients);

    for (uint i = 0; i < unit_points.size(); ++i) {
        fe_values.reinit(cells[i]);
        cells[i]->get_dof_indices(dof_indices);

        fields[i] *= 0.0;
        for (uint dof = 0; dof < n_dofs; ++dof)
            fields[i] -= this->solution(dof_indices[dof]) * fe_values.shape_grad(dof, i);
    }
}

template<int dim>
void FieldSolver<dim>::probe_field_fast(const vector<Point<dim>> &upoints,
        const vector<Cell> &cells, vector<Tensor<1,dim>> &fields) const
{
    const uint n_dofs = this->fe.dofs_per_cell;
    vector<uint> dof_indices(n_dofs);

    for (uint i = 0; i < upoints.size(); ++i) {
        vector<Tensor<1,dim>> shape_grad = this->utilities.shape_fun_grads(upoints[i], cells[i]);
        cells[i]->get_dof_indices(dof_indices);

        fields[i] *= 0.0;
        for (uint dof = 0; dof < n_dofs; ++dof)
            fields[i] -= this->solution(dof_indices[dof]) * shape_grad[dof];
    }
}

template<int dim>
void FieldSolver<dim>::export_charge_dens(vector<double> &charge_dens) const {
    const int n_verts = this->tria->n_used_vertices();
    require(n_verts == this->vertex2dof.size(), "Mismatch between #vertices and vertex2dof size: "
            + d2s(n_verts) + " vs " + d2s(this->vertex2dof.size()));

    charge_dens.resize(n_verts);
    for (unsigned i = 0; i < n_verts; i++)
        charge_dens[i] = this->charge_density[this->vertex2dof[i]];
}

template<int dim>
void FieldSolver<dim>::write_vtk(ofstream& out) const {
    LaplacePostProcessor<dim> post_processor; // needs to be before data_out
    DataOut<dim> data_out;
    data_out.attach_dof_handler(this->dof_handler);
    data_out.add_data_vector(this->solution, "electric_potential");
    data_out.add_data_vector(this->solution, post_processor);

    data_out.build_patches();
    data_out.write_vtk(out);
}

template<int dim>
void FieldSolver<dim>::write_xyz(ofstream& out) const {
    // write the start of xyz header
    FileWriter::write_xyz(out);

    // extract coordinates of dofs
    vector<Point<dim>> support_points;
    this->export_dofs(support_points);

    vector<Tensor<1,dim>> field;
    this->export_solution_grad(field);

    const int n_dofs = support_points.size();
    const int n_verts = this->tria->n_used_vertices();

    // generate dof index -> vertex index mapping
    vector<int> dof2vertex(n_dofs);
    for (int i = 0; i < n_verts; ++i)
        dof2vertex[this->vertex2dof[i]] = i;

    // write Ovito header
    out << "properties=id:I:1:pos:R:" << support_points[0].dimension <<":force:R:" <<
            field[0].dimension <<":elfield:R:1" <<
            ":charge_density:R:1:volume:R:1:potential:R:1\n";

    // write data
    for (int i = 0; i < n_dofs; ++i) {
        out << dof2vertex[i] << " " << support_points[i] << " " << field[dof2vertex[i]]
                << " " << field[dof2vertex[i]].norm() << " " << this->charge_density(i)
                << " " << this->dof_volume[i] << " " << this->solution(i) << "\n";
    }
}

template class FieldSolver<2>;
template class FieldSolver<3>;

} /* namespace femocs */
