/*
 *  Created on: 8 Nov 2019
 *      Author: kyritsak
 */

#include "ProjectSpaceCharge.h"
#include "Emission.h"
#include "PicSolver.h"
#include "SolutionReader.h"
#include "CurrentHeatSolver.h"

namespace femocs{

/* =====================================================================
 *                         ProjectSpaceCharge
 * ===================================================================== */

template <int dim>
ProjectSpaceCharge<dim>::ProjectSpaceCharge(AtomReader &reader, Config &config) :
        GeneralProject(reader, config), runner(NULL)
{
    string run_mode = config.scharge.mode;
    if (run_mode == "ramp_field")
        runner = new SpaceChargeRamper<dim>(config);
    else if (run_mode == "SCLE")
        runner = new SpaceChargeLimitedEmission<dim>(config);
    else
        throw runtime_error("Unimplemented space-charge mode: " + run_mode);
}

template <int dim>
ProjectSpaceCharge<dim>::~ProjectSpaceCharge() {
    if (runner) delete runner;
}

template <int dim>
int ProjectSpaceCharge<dim>::run(const int timestep, const double time){
    return runner->run();
}

template class ProjectSpaceCharge<2>;
template class ProjectSpaceCharge<3>;

/* =====================================================================
 *                         SpaceChargeRunner
 * ===================================================================== */

template <int dim>
SpaceChargeRunner<dim>::SpaceChargeRunner(Config &config) :
        theta_picard(config.scharge.fpi_relaxation),
        conf(config),
        phys_quantities(config.heating),
        pic(&emission, config, config.behaviour.rnd_seed),
        emission(&pic, config, NULL),
        ch_solver(&phys_quantities, &config.heating,
                emission.get_surface_heat(), emission.get_current_densities())
{
    double t0;

    if (conf.path.bulk_mesh_file != ""){
        start_msg(t0, "Reading bulk mesh from " + conf.path.bulk_mesh_file + " and setting up emission");
        ch_solver.import_mesh(conf.path.bulk_mesh_file);
        ch_solver.setup(300., true);
        emission.set_chsolver(&ch_solver);
        end_msg(t0);
    }

    start_msg(t0, "Reading vacuum mesh from " + conf.path.vacuum_mesh_file + " and setting up solvers");
    pic.import_mesh(conf.path.vacuum_mesh_file);
    pic.setup(conf.field.E0, conf.field.V0, conf.behaviour.axisymmetry);
    pic.write("mesh.msh", FileIO::no_update);
    phys_quantities.initialize();
    emission.initialize();
    end_msg(t0);
}

template<int dim>
int SpaceChargeRunner<dim>::run_steady_state(bool reset) {
    const double tol = conf.scharge.convergence;
    int iconv = 0;
    GLOBALS.TIMESTEP = 0;

    for (int j = 0; j < 10; j++) {
        write_verbose_msg("Running convergence with omega = " + d2s(theta_picard));
        for (int i = 0; i < 20; i++) {
            double Iprev =  emission.global_data.I_tot;
            double error_charge = pic.run_steady_iteration(theta_picard, i==0 && reset);
            double error_current = abs( emission.global_data.I_tot / Iprev - 1.0);
            GLOBALS.TIMESTEP++;
            if (error_charge < tol || error_current < 0.1 * tol)
                iconv++;
            if (iconv >= 2) {
                pic.write("pic_solver.movie");
                return i;
            }


        }
        if (theta_picard > 0.001)
            theta_picard *= 0.9;
    }
    write_verbose_msg("Convergence failed with theta = " + d2s(theta_picard));

    return 0;
}

template<int dim>
int SpaceChargeRunner<dim>::converge() {
    double window_steps = conf.behaviour.timestep_fs / conf.pic.dt_max;
    const auto &stats = emission.stats;

    if (window_steps - (int) window_steps > 1e-20 || window_steps < 10)
        write_verbose_msg("WARNING: md_timestep/pic_dtmax = " + d2s(window_steps));

    int max_steps = 50; //maximum window iterations to achieve convergence
    double I_mean_prev = stats.Itot_mean;
    int pic_error;
    int converged = 0;

    for (int i = 1; i <= max_steps; i++) {
        GLOBALS.TIMESTEP++;
        int error = pic.run(conf.behaviour.timestep_fs, i == 1);
        if (error) cout << "WARNING: Solve pic returned with error";

        emission.calc_global_stats();
        double err = (stats.Itot_mean - I_mean_prev) / stats.Itot_mean;
        double err_threshold = conf.scharge.convergence * stats.Itot_std / stats.Itot_mean;
        if (fabs(err) < 5e-3 || (fabs(err) < err_threshold && fabs(err) < 0.05))
            converged++;
        else
            converged = 0;

        if (MODES.VERBOSE) {
            printf("t=%.2e ps, i=%d, I_mean=%e, I_std=%.2f, error=%.2f, #converged=%d",
                    GLOBALS.TIME * 1.e-3, i, stats.Itot_mean,
                    100. *  stats.Itot_std /  stats.Itot_mean,
                    100 * err, converged);
            cout << endl;
        }

        I_mean_prev = stats.Itot_mean;

        // check if converged 3 consecutive times. (make sure no oscillations)
        if (converged >= 4) {
            write_verbose_msg("Running pic averaging...");
            error = pic.run(2 * conf.behaviour.timestep_fs, true);
            return 0;
        }
    }
    return 0;
}

template <int dim>
void SpaceChargeRunner<dim>::write_emission(const string &file_name, const string &label,
        double factor, bool first_line)
{
    emission.FileWriter::write("out/" + file_name + ".movie", FileIO::no_update | FileIO::force);
    ofstream out;
    out.open("out/" + file_name + ".dat", ofstream::out | ofstream::app);
    out.setf(std::ios::scientific);
    out.precision(6);

    if (first_line)
        emission.write_dat(out, "# " + label + "      ");

    out << factor << ' ';
    emission.write_dat(out);

    out.close();
}

template class SpaceChargeRunner<2>;
template class SpaceChargeRunner<3>;

/* =====================================================================
 *                         SpaceChargeRamper
 * ===================================================================== */

template <int dim>
SpaceChargeRamper<dim>::SpaceChargeRamper(Config &config) :
        SpaceChargeRunner<dim>(config),
        t0(0),
        deff(100.0),
        I_pic(config.scharge.N_fields),
        I_sc(config.scharge.N_fields),
        Vappl(config.scharge.N_fields),
        Flap(config.scharge.N_fields)
{}

template <int dim>
int SpaceChargeRamper<dim>::run() {
    start_msg(t0, "Solving Laplace equation");
    this->pic.setup(1., 1., this->conf.behaviour.axisymmetry, false);
    this->pic.assemble_laplace(true);
    this->pic.assemble_finalize();
    this->pic.solve();
    end_msg(t0);

    double beta = this->pic.max_grad();
    double delta_Flap = this->conf.scharge.Fl_apex_max - this->conf.scharge.Fl_apex_min;
    delta_Flap /= this->conf.scharge.N_fields - 1;
    for (int i = 0; i < this->conf.scharge.N_fields; i++){
        Flap[i] = this->conf.scharge.Fl_apex_min + i * delta_Flap;
        Vappl[i] = Flap[i] / beta;
    }

    if (this->conf.field.mode != "laplace"){
        if (this->conf.field.mode != "laplace"){
        get_Isc_pic();

            start_msg(t0, "Solving Laplace equation");
            this->pic.setup(1., 1., this->conf.behaviour.axisymmetry);
            this->pic.assemble_laplace(true);
            this->pic.assemble_finalize();
            this->pic.solve();
            end_msg(t0);

            find_deff();
    } else{
        deff = -1.;
        get_Isc_cepd(true);
    }
    } else{
        deff = -1.;
        get_Isc_cepd(true);
        this->pic.write("pic_solver.movie");
    }

    write_SC();

    return 0;
}

template <int dim>
void SpaceChargeRamper<dim>::get_Isc_pic(){
    int N = Vappl.size();
    for (int i = 0; i < N; i++) {
        start_msg( t0, "Running steady state convergence for Laplace apex field = "
                + d2s(Flap[i]) + " and Voltage = " + d2s(Vappl[i]) + "\n" );

        this->pic.setup(Vappl[i], Vappl[i], this->conf.behaviour.axisymmetry, i == 0);

        if (this->conf.field.mode == "steady_state")
            this->run_steady_state(i == 0);
        else if (this->conf.field.mode == "converge")
            this->converge();

        I_pic[i] = this->emission.global_data.I_tot;
        this->write_converged(Vappl[i], i == 0);
        double theta = this->emission.global_data.Fmax / Flap[i];
        write_verbose_msg("SC calculated. apex theta = " + d2s(theta));
        end_msg(t0);

        if (theta < this->conf.scharge.theta_lim){
            N = i+1;
            break;
        }
    }

    Vappl.resize(N);
    Flap.resize(N);
    I_pic.resize(N);
    I_sc.resize(N);
}

template <int dim>
double SpaceChargeRamper<dim>::find_deff() {
    int Nmax = 50;
    double errlim = 1.e-4;
    deff = Vappl[0] / Flap[0];
    double dhigh = 1.e2 * deff;
    double dlow = 1.e-2 * deff;
    double old_error;

    // perform bisection
    for (int i = 0; i < Nmax; ++i) {
        get_Isc_cepd();
        double error = get_current_error();
        cout  << " deff = " << deff << ", error = " <<  error << endl;
        if(error > errlim){
            dlow = deff;
            deff = sqrt(dhigh * dlow);
        }
        else if(error < -errlim){
            dhigh = deff;
            deff = sqrt(dhigh * dlow);
        }
        else {
            get_Isc_cepd(true);
            return deff;
        }
    }
    return 0.;
}

template <int dim>
void SpaceChargeRamper<dim>::get_Isc_cepd(bool write_file){
    for (int i = 0; i < Vappl.size(); i++) {
        this->emission.set_sfactor(Vappl[i]);
        this->emission.extract_fields();
        this->emission.calc_emission(deff, true);
        I_sc[i] = this->emission.global_data.I_tot;
        if (write_file)
            this->write_cepd(Vappl[i], i == 0);
    }
}

template <int dim>
double SpaceChargeRamper<dim>::get_current_error() const {
    require(I_sc.size() == I_pic.size(), "comparison of current vectors no equal sizes");
    double error = 0;
    for (int i = 0; i < I_sc.size(); i++)
        error += log(I_sc[i] / I_pic[i]);
    return error;
}

template <int dim>
void SpaceChargeRamper<dim>::write_cepd(double Vappl, bool first_line) {
    this->write_emission("emission_cepd", "Vappl[V]", Vappl, first_line);
}

template <int dim>
void SpaceChargeRamper<dim>::write_converged(double Vappl, bool first_line) {
    this->write_emission("emission_converged", "Vappl[V]", Vappl, first_line);
}

template <int dim>
void SpaceChargeRamper<dim>::write_SC() {
    ofstream out;
    out.open("out/results_SC.dat");
    out.setf(std::ios::scientific);
    out.precision(6);

    double rms_error = 0.;

    for (int i = 0; i < I_sc.size(); ++i)
        rms_error += ((I_sc[i] - I_pic[i]) / I_pic[i]) * ((I_sc[i] - I_pic[i]) / I_pic[i]);

    out << "deff = " << deff << " rms_err = " << sqrt(rms_error) / I_sc.size() <<endl;
    out << "   F_max_L      Voltage       I_sc        I_pic" << endl;

    for (int i = 0; i < I_sc.size(); ++i)
        out << Flap[i] << " " << Vappl[i] << " " << I_sc[i] << " " << I_pic[i] << endl;

    out.close();
}

template class SpaceChargeRamper<2>;
template class SpaceChargeRamper<3>;

/* =====================================================================
 *                      SpaceChargeLimitedEmission
 * ===================================================================== */

template <int dim>
SpaceChargeLimitedEmission<dim>::SpaceChargeLimitedEmission(Config &config) :
        SpaceChargeRunner<dim>(config)
{}

template <int dim>
int SpaceChargeLimitedEmission<dim>::run() {
    double W_reduction = 0.1;
    double t0;

    this->pic.setup(this->conf.field.V0, this->conf.field.V0, this->conf.behaviour.axisymmetry, true);
    for (int i = 0; i < 50; i++){
        start_msg(t0, "Running steady state convergence for work function "
                + d2s(this->emission.get_work_function()));
        cout << endl;

        int iters = this->run_steady_state(i == 0);
        if (iters) {
            double W = this->emission.get_work_function();
            this->write_emission("emission", "W[eV]", W, i == 0);
            this->emission.reduce_work_function(1 - W_reduction);
        } else {
            this->emission.reduce_work_function(1./ (1. - W_reduction)); //get it back to the original value
            W_reduction *= 0.99; // reduce the reduction factor
            this->emission.reduce_work_function(1 - W_reduction); //reduce work with the new reduction factor
        }
    }

    return 0;
}

template class SpaceChargeLimitedEmission<2>;
template class SpaceChargeLimitedEmission<3>;

} /* namespace femocs */
