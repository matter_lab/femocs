/*
 * DealSolver.cpp
 *
 *  Created on: 12.2.2018
 *      Author: veske
 */

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/grid_reordering.h>
//#include <deal.II/base/numbers.h>

#include <deal.II/base/work_stream.h>
#include <deal.II/dofs/dof_tools.h>

#include "DealSolver.h"
#include "Macros.h"
#include "Globals.h"
#include "Primitives.h"


using namespace dealii;
using namespace std;

namespace femocs {

template<int dim>
DealSolver<dim>::DealSolver() :
        dirichlet_bc_value(0), tria(&triangulation), dof_handler(triangulation),
        fe(shape_degree), utilities(tria, fe, dof_handler, solution)
{}

template<int dim>
DealSolver<dim>::DealSolver(Triangulation<dim> *tr) :
        dirichlet_bc_value(0), tria(tr), dof_handler(*tr),
        fe(shape_degree), utilities(tria, fe, dof_handler, solution)
{}

template<int dim>
DealSolver<dim>::LinearSystem::LinearSystem(Vector<double>* rhs, SparseMatrix<double>* matrix) :
    global_rhs(rhs), global_matrix(matrix)
{}

template<int dim>
DealSolver<dim>::ScratchData::ScratchData (const FiniteElement<dim>  &fe,
        const Quadrature<dim> &quadrature, const UpdateFlags ul) :
    fe_values(fe, quadrature, ul)
{}

template<int dim>
DealSolver<dim>::ScratchData::ScratchData (const ScratchData &sd):
    fe_values(sd.fe_values.get_fe(), sd.fe_values.get_quadrature(), sd.fe_values.get_update_flags())
{}

template<int dim>
DealSolver<dim>::CopyData::CopyData(const unsigned dofs_per_cell, const unsigned n_qp):
    cell_matrix(dofs_per_cell, dofs_per_cell),
    cell_rhs(dofs_per_cell),
    dof_indices(dofs_per_cell),
    n_dofs(dofs_per_cell), n_q_points(n_qp)
{}

template<int dim>
void DealSolver<dim>::copy_global_cell(const CopyData &copy_data, LinearSystem &system) const {
    system.global_rhs->add(copy_data.dof_indices, copy_data.cell_rhs);

    for (unsigned int i = 0; i < copy_data.n_dofs; ++i) {
        for (unsigned int j = 0; j < copy_data.n_dofs; ++j)
            system.global_matrix->add(copy_data.dof_indices[i], copy_data.dof_indices[j], copy_data.cell_matrix(i, j));
    }
}

template<int dim>
double DealSolver<dim>::max_solution() const {
    return solution.linfty_norm();
}

template<int dim>
double DealSolver<dim>::max_grad() const {
    vector<Tensor<1, dim>> grads;
    export_solution_grad(grads);
    double max_grad = 0.;
    for (auto grad : grads){
        double grad_norm = grad.norm();
        if (grad_norm > max_grad)
            max_grad = grad_norm;
    }
    return max_grad;
}

template<int dim>
void DealSolver<dim>::clamp_sol(const double low_limit, const double high_limit) {
    for (double &s : this->solution) {
        if (s > high_limit)
            s = high_limit;
        else if (s < low_limit)
            s = low_limit;
    }
}

template<int dim>
bool DealSolver<dim>::check_limits(const double low_limit, const double high_limit) {
    if (low_limit == high_limit)
        return false;

    stat.sol_min = 1e100;
    stat.sol_max = -1e100;
    for (double s : this->solution) {
        stat.sol_min = min(stat.sol_min, s);
        stat.sol_max = max(stat.sol_max, s);
    }

    return stat.sol_min < low_limit || stat.sol_max > high_limit;
}

template<int dim>
bool DealSolver<dim>::import_mesh(const string &file_name) {
    const string file_type = get_file_type(file_name);
    require(file_type == "msh", "Unimplemented file type for mesh importing: " + file_type);

    ifstream infile(file_name);
    require(infile.is_open(), "Can't open a file " + file_name);

    GridIn<dim, dim> gi;
    gi.attach_triangulation(triangulation);
    gi.read_msh(infile);
    return true;
}

template<int dim>
bool DealSolver<dim>::import_mesh(vector<Point<dim>> vertices, vector<CellData<dim>> cells) {
    try {
        SubCellData subcelldata;
        // Do some clean-up on vertices...
        GridTools::delete_unused_vertices(vertices, cells, subcelldata);
        // ... and on cells
        GridReordering<dim, dim>::invert_all_cells_of_negative_grid(vertices, cells);
        // Clean previous mesh
        triangulation.clear();
        // Create new mesh
        triangulation.create_triangulation_compatibility(vertices, cells, SubCellData());
    } catch (exception &exc) {
        return false;
    }

    mark_mesh();
    return true;
}

template<int dim>
void DealSolver<dim>::write_vtk(ofstream& out) const {
    DataOut<dim> data_out;
    data_out.attach_dof_handler(dof_handler);
    data_out.add_data_vector(solution, "solution");

    data_out.build_patches();
    data_out.write_vtk(out);
}

template<int dim>
void DealSolver<dim>::write_msh(ofstream& out) const {
    GridOut grid_out;
    grid_out.set_flags(GridOutFlags::Msh(true, true));
    grid_out.write_msh(triangulation, out);
}

template<>
void DealSolver<3>::export_surface_centroids(Medium& medium) const {
    const int n_faces_per_cell = GeometryInfo<3>::faces_per_cell;
    typename DoFHandler<3>::active_cell_iterator cell;

    unsigned int n_nodes = 0;
    for (cell = dof_handler.begin_active(); cell != dof_handler.end(); ++cell)
        for (int f = 0; f < n_faces_per_cell; ++f)
            if (cell->face(f)->boundary_id() == BoundaryID::copper_surface)
                n_nodes++;

    medium.reserve(n_nodes);

    for (cell = dof_handler.begin_active(); cell != dof_handler.end(); ++cell)
        for (int f = 0; f < n_faces_per_cell; ++f)
            if (cell->face(f)->boundary_id() == BoundaryID::copper_surface)
                medium.append( femocs::Point3(cell->face(f)->center()) );
}

template<int dim>
void DealSolver<dim>::export_surface_centroids(vector<Point<dim>>* centrs,
        vector<Cell>* cells, vector<int>* faces, vector<double>* areas) const
{
    if (centrs) centrs->clear();
    if (cells) cells->clear();
    if (faces) faces->clear();
    if (areas) areas->clear();

    for (Cell cell = dof_handler.begin_active(); cell != dof_handler.end(); ++cell)
        for (int f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f)
            if (cell->face(f)->boundary_id() == BoundaryID::copper_surface) {
                Point<dim> centroid = cell->face(f)->center();
                double ax_factor = axisymmetry_factor(centroid);

                if (centrs) centrs->push_back(centroid);
                if (cells) cells->push_back(cell);
                if (faces) faces->push_back(f);
                if (areas) areas->push_back(cell->face(f)->measure() * ax_factor);
            }
}

template<>
void DealSolver<3>::export_vertices(Medium& medium) {
    vector<Point<3>> support_points;
    export_dofs(support_points);

    const unsigned int n_verts = tria->n_used_vertices();
    calc_vertex2dof();
    require(n_verts == vertex2dof.size(), "Mismatch between #vertices and vertex2dof size: "
            + d2s(n_verts) + " vs " + d2s(vertex2dof.size()));

    medium.reserve(n_verts);
    for (int i = 0; i < n_verts; ++i)
        medium.append( Atom(i, support_points[vertex2dof[i]], 0) );
}

template<int dim>
void DealSolver<dim>::export_dofs(vector<Point<dim>>& points) const {
    points.resize(size());
    DoFTools::map_dofs_to_support_points<dim>(StaticMappingQ1<dim>::mapping,
            dof_handler, points);
}

template<int dim>
void DealSolver<dim>::export_solution(vector<double> &sol) const {
    const int n_verts = tria->n_used_vertices();
    require(n_verts == vertex2dof.size(), "Mismatch between #vertices and vertex2dof size: "
            + d2s(n_verts) + " vs " + d2s(vertex2dof.size()));

    sol.resize(n_verts);
    for (unsigned i = 0; i < n_verts; i++)
        sol[i] = solution[vertex2dof[i]];
}

template<int dim>
void DealSolver<dim>::export_solution_grad(vector<Tensor<1, dim>> &grads) const {
    const int n_verts = tria->n_used_vertices();
    require(n_verts == vertex2cell.size(), "Mismatch between #vertices and vertex2cell size: "
            + d2s(n_verts) + " vs " + d2s(vertex2cell.size()));

    QGauss<dim> quadrature_formula(this->quadrature_degree);
    FEValues<dim> fe_values(this->fe, quadrature_formula, update_gradients);

    vector<Tensor<1, dim>> solution_gradients(quadrature_formula.size());
    grads.resize(n_verts);

    for (unsigned i = 0; i < n_verts; i++) {
        // Using DoFAccessor (groups.google.com/forum/?hl=en-GB#!topic/dealii/azGWeZrIgR0)
        // NB: only works without refinement !!!
        Cell dof_cell(tria, 0, vertex2cell[i], &dof_handler);

        fe_values.reinit(dof_cell);
        fe_values.get_function_gradients(this->solution, solution_gradients);
        grads[i] = -1.0 * solution_gradients.at(vertex2node[i]);
    }
}

template<int dim>
void DealSolver<dim>::import_solution(const vector<double>* new_solution) {
    const unsigned int n_verts = vertex2dof.size();

    require(new_solution, "Can't use NULL solution vector!");
    require(n_verts == new_solution->size(), "Mismatch between #vertices and solution vector size: "
            + d2s(n_verts) + " vs " + d2s(new_solution->size()));

    // Initialize the solution with non-constant values
    for (size_t i = 0; i < n_verts; i++) {
        this->solution[vertex2dof[i]] = (*new_solution)[i];
    }
}

template<int dim>
void DealSolver<dim>::write_solution(ofstream& out) const {
    vector<double> sol;
    export_solution(sol);

    for (int i = 0; i < sol.size(); ++i) {
        out << sol[i] << endl;
    }

    out.close();
}

template<int dim>
void DealSolver<dim>::read_solution(const string &file_name) {
    ifstream in(file_name);
    require(in.is_open(), "Can't open a file " + file_name);

    string str;
    vector<double> new_solution;

    while (in >> str) {
        new_solution.push_back(stod(str));
    }

    in.close();

    import_solution(&new_solution);
}

template<int dim>
void DealSolver<dim>::calc_vertex2dof() {
    static constexpr int n_verts_per_elem = GeometryInfo<dim>::vertices_per_cell;
    require(tria, "Pointer to triangulation missing!");
    const unsigned int n_verts = tria->n_used_vertices();
    require(n_verts > 0, "Can't generate map with empty triangulation!");

    // create mapping from mesh vertex to cell index & cell node
    vertex2cell.resize(n_verts);
    vertex2node.resize(n_verts);
    vector<vector<unsigned>> vertex2cells(n_verts);

    for (Cell cell = this->dof_handler.begin_active(); cell != this->dof_handler.end(); ++cell)
        for (int i = 0; i < n_verts_per_elem; ++i) {
            vertex2cell[cell->vertex_index(i)] = cell->active_cell_index();
            vertex2cells[cell->vertex_index(i)].push_back(cell->active_cell_index());
            vertex2node[cell->vertex_index(i)] = i;
        }

    // create mapping from vertex index to dof index
    vertex2dof.resize(n_verts);
    for (unsigned i = 0; i < n_verts; ++i) {
        Cell cell(tria, 0, vertex2cell[i], &this->dof_handler);
        vertex2dof[i] = cell->vertex_dof_index(vertex2node[i], 0);
    }
}

template<int dim>
void DealSolver<dim>::calc_dof_volumes() {
    QGauss<dim> quadrature_formula(quadrature_degree);
    FEValues<dim> fe_values(fe, quadrature_formula,  update_values | update_quadrature_points | update_JxW_values);
    vector<types::global_dof_index> local_dof_indices(fe.dofs_per_cell);

    // reset volumes
    dof_volume.resize(size());
    std::fill(dof_volume.begin(), dof_volume.end(), 0);

    double ax_factor;

    // Iterate over all cells
    for (Cell cell = dof_handler.begin_active(); cell != dof_handler.end(); ++cell) {
        fe_values.reinit(cell);
        cell->get_dof_indices(local_dof_indices);

        // Iterate through quadrature points to integrate
        for (unsigned q = 0; q < quadrature_formula.size(); ++q) {
            //iterate through local dofs
            ax_factor = axisymmetry_factor(fe_values, q);

            for (unsigned int i = 0; i < fe.dofs_per_cell; ++i)
                dof_volume[local_dof_indices[i]] += ax_factor * fe_values.JxW(q) * fe_values.shape_value(i, q);
        }
    }
}

template<int dim>
void DealSolver<dim>::setup_system(bool axisymmetry) {
    require(tria->n_used_vertices() > 0, "Can't setup system with no mesh!");
    require((dim != 2) != axisymmetry, "Axisymmetry makes sense only for 2D systems");

    this->dof_handler.distribute_dofs(this->fe);
    this->boundary_values.clear();

    this->axisymmetry = axisymmetry;

    require((dim!=2) != axisymmetry, "Axisymmetry makes sense only for 2D systems");

    const unsigned int n_dofs = size();

    DynamicSparsityPattern dsp(n_dofs);
    DoFTools::make_sparsity_pattern(this->dof_handler, dsp);
    this->sparsity_pattern.copy_from(dsp);

    this->system_matrix.reinit(this->sparsity_pattern);
    this->system_matrix_save.reinit(this->sparsity_pattern);
    this->system_rhs.reinit(n_dofs);
    this->solution.reinit(n_dofs);
    this->solution = this->dirichlet_bc_value;

    this->calc_vertex2dof();
    utilities.precompute();
}

template<int dim>
void DealSolver<dim>::assemble_parallel() {
    LinearSystem system(&this->system_rhs, &this->system_matrix);
    QGauss<dim> quadrature_formula(this->quadrature_degree);

    const unsigned int n_dofs = this->fe.dofs_per_cell;
    const unsigned int n_q_points = quadrature_formula.size();

    WorkStream::run(this->dof_handler.begin_active(),this->dof_handler.end(),
            std::bind(&DealSolver<dim>::assemble_local_cell,
                    this,
                    std::placeholders::_1,
                    std::placeholders::_2,
                    std::placeholders::_3),
            std::bind(&DealSolver<dim>::copy_global_cell,
                    this,
                    std::placeholders::_1,
                    std::ref(system)),
            ScratchData(this->fe, quadrature_formula, update_gradients | update_quadrature_points | update_JxW_values),
            CopyData(n_dofs, n_q_points)
    );

    // save system matrix
    this->system_matrix_save.copy_from(this->system_matrix);
}

template<int dim>
void DealSolver<dim>::assemble_local_cell(const Cell &cell,
        ScratchData &scratch_data, CopyData &copy_data) const
{
	const unsigned int n_dofs = copy_data.n_dofs;
    const unsigned int n_q_points = copy_data.n_q_points;

    scratch_data.fe_values.reinit(cell);

    // Local matrix assembly
    copy_data.cell_matrix = 0;
    for (unsigned int q = 0; q < n_q_points; ++q) {
        double ax_factor = axisymmetry_factor(scratch_data.fe_values, q);
        for (unsigned int i = 0; i < n_dofs; ++i) {
            for (unsigned int j = 0; j < n_dofs; ++j) {
                copy_data.cell_matrix(i, j) += ax_factor * scratch_data.fe_values.JxW(q) *
                scratch_data.fe_values.shape_grad(i, q) * scratch_data.fe_values.shape_grad(j, q);
            }
        }
    }

    // Nothing to add to local right-hand-side vector assembly
//    copy_data.cell_rhs = 0;

    // Obtain dof indices for updating global matrix and right-hand-side vector
    cell->get_dof_indices(copy_data.dof_indices);
}

template<int dim>
void DealSolver<dim>::assemble_rhs(const int bid) {

    QGauss<dim-1> face_quadrature_formula(this->quadrature_degree);
    FEFaceValues<dim> fe_face_values(this->fe, face_quadrature_formula,
            update_values | update_quadrature_points | update_JxW_values);

    const unsigned int dofs_per_cell = this->fe.dofs_per_cell;
    const unsigned int n_face_q_points = face_quadrature_formula.size();

    Vector<double> cell_rhs(dofs_per_cell);
    vector<types::global_dof_index> local_dof_indices(dofs_per_cell);

    // Iterate over all cells (quadrangles in 2D, hexahedra in 3D) of the mesh
    unsigned int boundary_face_index = 0;
    for (Cell cell = this->dof_handler.begin_active(); cell != this->dof_handler.end(); ++cell) {
        // Loop over all faces (lines in 2D, quadrangles in 3D) of the cell
        for (unsigned int f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f) {
            // Apply boundary condition at faces on top of vacuum domain
            if (cell->face(f)->at_boundary() && cell->face(f)->boundary_id() == bid) {
                fe_face_values.reinit(cell, f);
                double bc_value = get_face_bc(boundary_face_index++);

                // Compose local rhs update
                cell_rhs = 0;
                for (unsigned int q = 0; q < n_face_q_points; ++q) {
                    double ax_factor = axisymmetry_factor(fe_face_values, q);
                    for (unsigned int i = 0; i < dofs_per_cell; ++i) {
                        cell_rhs(i) += fe_face_values.shape_value(i, q)
                                * bc_value * fe_face_values.JxW(q) * ax_factor;
                    }
                }

                // Add the current cell rhs entries to the system rhs
                cell->get_dof_indices(local_dof_indices);
                for (unsigned int i = 0; i < dofs_per_cell; ++i)
                    this->system_rhs(local_dof_indices[i]) += cell_rhs(i);
            }
        }
    }
}

template<int dim>
void DealSolver<dim>::append_dirichlet(const int bid, const double value) {
    VectorTools::interpolate_boundary_values(this->dof_handler, bid, dealii::Functions::ConstantFunction<dim>(value), boundary_values);
}

template<int dim>
inline double DealSolver<dim>::axisymmetry_factor(const Point<dim> &p) const{
    if (axisymmetry)
        return 2 * M_PI * p(0);
    else
        return 1.;
}

template<int dim>
inline double DealSolver<dim>::axisymmetry_factor(const FEFaceValues<dim>& fe_face_values, int q) const{
    if (!axisymmetry) return 1.;
    Point<dim> qpoint = 2 * M_PI * fe_face_values.quadrature_point(q);
    return qpoint(0);
}

template<int dim>
inline double DealSolver<dim>::axisymmetry_factor(const FEValues<dim>& fe_values, int q) const{
    if (!axisymmetry) return 1.;
    Point<dim> qpoint = 2 * M_PI * fe_values.quadrature_point(q);
    return qpoint(0);
}

template<int dim>
void DealSolver<dim>::apply_dirichlet() {
    MatrixTools::apply_boundary_values(boundary_values, this->system_matrix, this->solution, this->system_rhs);
}

template<int dim>
int DealSolver<dim>::solve_cg(int max_iter, double tol, double ssor_param) {
    SolverControl solver_control(max_iter, tol);
    SolverCG<> solver(solver_control);
    try {
        if (ssor_param > 0.0) {
            PreconditionSSOR<> preconditioner;
            preconditioner.initialize(system_matrix, ssor_param);
            solver.solve(system_matrix, solution, system_rhs, preconditioner);
        } else
            solver.solve(system_matrix, solution, system_rhs, PreconditionIdentity());

        return solver_control.last_step();
    } catch (exception &exc) {
        return -1 * solver_control.last_step();
    }
}

template<int dim>
void DealSolver<dim>::solve_umf(bool first_run) {
    if (first_run) {
        solver_umf.initialize(this->sparsity_pattern);
        solver_umf.factorize(this->system_matrix);
    }
    solver_umf.vmult(this->solution, this->system_rhs);
}

template<int dim>
void DealSolver<dim>::mark_boundary(int top, int bottom, int sides, int other) {
    static constexpr double eps = 1e-6;
    double xmax = -1e16, ymax = -1e16, zmax = -1e16;
    double xmin = 1e16, ymin = 1e16, zmin = 1e16;

    typename Triangulation<dim>::active_face_iterator face;

    // Loop through the faces and find maximum and minimum values for coordinates
    for (face = triangulation.begin_face(); face != triangulation.end_face(); ++face) {
        if (face->at_boundary()) {
            double x = face->center()[0];
            double y = face->center()[1];
            xmax = max(x, xmax);
            xmin = min(x, xmin);
            ymax = max(y, ymax);
            ymin = min(y, ymin);

            if (dim == 3) {
                double z = face->center()[2];
                zmax = max(z, zmax);
                zmin = min(z, zmin);
            }
        }
    }

    // Loop through the faces and mark them by their position
    for (face = triangulation.begin_face(); face != triangulation.end_face(); ++face) {
        if (face->at_boundary()) {
            if (dim == 2) {
                double x = face->center()[0];
                double y = face->center()[1];

                if (on_boundary(x, xmin, xmax, eps))
                    face->set_all_boundary_ids(sides);
                else if (on_boundary(y, ymax, eps))
                    face->set_all_boundary_ids(top);
                else if (on_boundary(y, ymin, eps))
                    face->set_all_boundary_ids(bottom);
                else
                    face->set_all_boundary_ids(other);
            }
            else if (dim == 3) {
                double x = face->center()[0];
                double y = face->center()[1];
                double z = face->center()[2];

                if (on_boundary(x, xmin, xmax, eps) || on_boundary(y, ymin, ymax, eps))
                    face->set_all_boundary_ids(sides);
                else if (on_boundary(z, zmax, eps))
                    face->set_all_boundary_ids(top);
                else if (on_boundary(z, zmin, eps))
                    face->set_all_boundary_ids(bottom);
                else
                    face->set_all_boundary_ids(other);
            }
        }
    }
}

template class DealSolver<2>;
template class DealSolver<3>;

} /* namespace femocs */
