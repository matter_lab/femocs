/*
 * Femocs.cpp
 *
 *  Created on: 14.12.2015
 *      Author: veske
 */

#define MAINFILE

#include "Femocs.h"
#include "Globals.h"
#include "ProjectRunaway.h"
#include "ProjectSwiftHI.h"
#include "ProjectSpaceCharge.h"
#include "ProjectPlasma.h"
#include "Version.h"

#include <omp.h>


using namespace std;
namespace femocs {

Femocs::Femocs() : t0(0), reader(&conf), project(NULL)
{}

Femocs::Femocs(const string &conf_file) : t0(0), reader(&conf), project(NULL) {
    init(conf_file);
}

Femocs::~Femocs() {
    if (project) delete project;
}

string Femocs::input_dir() const {
    return MODES.IN_FOLDER;
}

string Femocs::output_dir() const {
    return MODES.OUT_FOLDER;
}

string Femocs::version() const {
    return FEMOCS_VERSION_STR;
}

void Femocs::init_working_dirs(const string &conf_file) {
    // Do static check to prevent multiple Femocs instances
    // from doing the initialization many times
    static bool init_done = false;
    if (init_done) return;

    // Extract path to the working directory from the file path
    const size_t index = conf_file.rfind('/');
    if (index == string::npos)
        MODES.IN_FOLDER = "./";
    else
        MODES.IN_FOLDER = conf_file.substr(0, index+1);

    // Specify path to the folder where output files will be written
    if (conf.path.out_folder != "")
        MODES.OUT_FOLDER = conf.path.out_folder;
    else
        MODES.OUT_FOLDER = MODES.IN_FOLDER + "out";

    // Ensure that output folder path ends with a slash
    if (MODES.OUT_FOLDER.back() != '/')
        MODES.OUT_FOLDER += '/';

    // Clear the results from previous run
    int fail;
    if (conf.run.output_cleaner)
        fail = execute("rm -rf " + MODES.OUT_FOLDER);
    fail = execute("mkdir -p " + MODES.OUT_FOLDER);

    // Make sure that input files have correct path
    conf.append_input_dir();

    init_done = true;
}

void Femocs::init(const string &conf_file) {
    bool fail;

    // Read configuration parameters from configuration file
    conf.read_all(conf_file);

    // Pick the correct verbosity mode flags
    if      (conf.behaviour.verbosity == "mute")    { MODES.MUTE = true;  MODES.VERBOSE = false; }
    else if (conf.behaviour.verbosity == "silent")  { MODES.MUTE = false; MODES.VERBOSE = false; }
    else if (conf.behaviour.verbosity == "verbose") { MODES.MUTE = false; MODES.VERBOSE = true;  }

    // Pick correct flags for writing log file
    MODES.WRITELOG = conf.behaviour.n_write_log != 0;
    MODES.SHORTLOG = conf.behaviour.n_write_log < 0;

    init_working_dirs(conf_file);

    write_verbose_msg("===== Starting Femocs =====");
    write_verbose_msg("version: " + version());
    write_verbose_msg("input directory:  " + MODES.IN_FOLDER);
    write_verbose_msg("output directory: " + MODES.OUT_FOLDER);
    omp_set_num_threads(conf.behaviour.n_omp_threads);

    // pick the project to run

    const string& prj = conf.behaviour.project;
    const int dim = conf.behaviour.dimension;

    require(prj == "runaway" || prj == "SwiftHI" || prj == "space-charge" ||
            prj == "plasma", "Invalid project: " + d2s(prj));
    require(dim == 2 || dim == 3, "Invalid dimension: " + d2s(dim));

    if (project) delete project;

    if (prj == "runaway") {
        project = new ProjectRunaway(reader, conf);
    } else if (prj == "SwiftHI") {
        if (dim == 2)
            project = new ProjectSwiftHI<2>(reader, conf);
        else
            project = new ProjectSwiftHI<3>(reader, conf);
    } else if (prj == "space-charge") {
        if (dim == 2)
            project = new ProjectSpaceCharge<2>(reader, conf);
        else
            project = new ProjectSpaceCharge<3>(reader, conf);
    } else if (prj == "plasma") {
    	if (dim == 2)
    		project = new ProjectPlasma<2>(reader, conf);
    	else
    		project = new ProjectPlasma<3>(reader, conf);
    }

    // Initialize AtomReader file writer
    reader.reset_write_time();
}

int Femocs::run(const int timestep, const double time) {
    return project->run(timestep, time);
}

int Femocs::import_atoms(const string& file_name, const int add_noise) {
    clear_log();
    int n_atoms;

    if (file_name == "generate") {
        start_msg(t0, "Generating nanotip");
        n_atoms = reader.generate_nanotip(conf.geometry.height, conf.geometry.radius, conf.geometry.latconst);
        end_msg(t0);
    } else {
        start_msg(t0, "Importing atoms");
        n_atoms = reader.import_file(file_name, add_noise);
        end_msg(t0);
    }

    write_verbose_msg( "#input atoms: " + d2s(reader.size()) );
    reader.write("atomreader.ckx");
    return n_atoms;
}

int Femocs::import_lammps(const int n_atoms, const double* const* xyz,
        const double* const* vel, const int* mask, const int groupbit)
{
    if (vel) {
        start_msg(t0, "Importing velocities");
        reader.import_lammps_velocities(n_atoms, &vel[0][0]);
        end_msg(t0);
    }

    if (!xyz) return 0;

    clear_log();

    start_msg(t0, "Importing coordinates");
    reader.import_lammps_coordinates(n_atoms, &xyz[0][0]);
    end_msg(t0);
    write_verbose_msg( "#input atoms: " + d2s(reader.size()) );

    start_msg(t0, "Analyzing coordinates");
    reader.analyse(NULL);
    end_msg(t0);
    write_verbose_msg(d2s(reader));

    reader.write("atomreader.ckx");
    return 0;
}

int Femocs::import_parcas(const int n_atoms, const double* x0, const double* x1,
        const double* box, const int* nborlist)
{
    clear_log();

    start_msg(t0, "Importing coordinates & velocities");
    reader.set_parcas_box(box);
    reader.import_parcas_coordinates(n_atoms, x0);
    reader.import_parcas_velocities(n_atoms, x1);
    end_msg(t0);
    write_verbose_msg( "#input atoms: " + d2s(reader.size()) );

    start_msg(t0, "Analyzing coordinates");
    reader.analyse(nborlist);
    end_msg(t0);
    write_verbose_msg(d2s(reader));

    reader.write("atomreader.ckx");
    return 0;
}

int Femocs::import_atoms(const int n_atoms, const double* x, const double* y, const double* z, const int* types) {
    clear_log();
    conf.run.surface_cleaner = false; // disable the surface cleaner for atoms with known types

    start_msg(t0, "Importing atoms");
    reader.import_atoms(n_atoms, x, y, z, types);
    end_msg(t0);
    write_verbose_msg( "#input atoms: " + d2s(reader.size()) );

    reader.write("atomreader.ckx");
    return 0;
}

int Femocs::interpolate_surface_elfield(const int n_points, const double* x, const double* y, const double* z,
        double* Ex, double* Ey, double* Ez, double* Enorm, int* flag)
{
    double *fields = new double[3*n_points];
    int retval = project->interpolate(fields, flag, n_points, LABELS.elfield, true, x, y, z);

    for (int i = 0; i < n_points; ++i) {
        int I = 3*i;
        Ex[i] = fields[I];
        Ey[i] = fields[I+1];
        Ez[i] = fields[I+2];
        Enorm[i] = sqrt(fields[I]*fields[I] + fields[I+1]*fields[I+1] + fields[I+2]*fields[I+2]);
    }

    delete[] fields;
    return retval;
}

int Femocs::interpolate_elfield(const int n_points, const double* x, const double* y, const double* z,
        double* Ex, double* Ey, double* Ez, double* Enorm, int* flag)
{
    double *fields = new double[3*n_points];
    int retval = project->interpolate(fields, flag, n_points, LABELS.elfield, false, x, y, z);

    for (int i = 0; i < n_points; ++i) {
        int I = 3*i;
        Ex[i] = fields[I];
        Ey[i] = fields[I+1];
        Ez[i] = fields[I+2];
        Enorm[i] = sqrt(fields[I]*fields[I] + fields[I+1]*fields[I+1] + fields[I+2]*fields[I+2]);
    }

    delete[] fields;
    return retval;
}

int Femocs::interpolate_phi(const int n_points, const double* x, const double* y, const double* z,
        double* phi, int* flag)
{
    return project->interpolate(phi, flag, n_points, LABELS.potential, false, x, y, z);
}

int Femocs::import_file(const string &file_name) {
    return project->import_file(file_name);
}

int Femocs::import_data(const double* data, const int n_points, const string& data_type) {
    return project->import_data(data, n_points, data_type);
}

int Femocs::import_data(const int* data, const int n_points, const string& data_type) {
    return project->import_data(data, n_points, data_type);
}

int Femocs::export_data(double* data, const int n_points, const string& data_type) {
    return project->export_data(data, n_points, data_type);
}

int Femocs::export_data(int* data, const int n_points, const string& data_type) {
    return project->export_data(data, n_points, data_type);
}

int Femocs::export_data(const double** data, const string& data_type) const {
    return project->export_data(data, data_type);
}

int Femocs::export_data(const int** data, const string& data_type) const {
    return project->export_data(data, data_type);
}

int Femocs::interpolate(double* data, int* flag,
        const int n_points, const string &data_type, const bool near_surface,
        const double* x, const double* y, const double* z)
{
    return project->interpolate(data, flag, n_points, data_type, near_surface, x, y, z);
}

int Femocs::parse_command(const string& command, int* arg) {
    return conf.read_command(command, arg[0]);
}

int Femocs::parse_command(const string& command, double* arg) {
    return conf.read_command(command, arg[0]);
}

int Femocs::parse_command(const string& command, string& arg) {
    return conf.read_command(command, arg);
}

int Femocs::parse_command(const string& command, char* arg) {
    string string_arg;
    bool fail = conf.read_command(command, string_arg);
    if (!fail) string_arg.copy(arg, string_arg.length());
    return fail;
}

} // namespace femocs
