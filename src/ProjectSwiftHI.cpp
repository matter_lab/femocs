/*
 * ProjectSwiftHI.cpp
 *
 *  Created on: 31 Jan 2020
 *      Author: kyritsak
 */

#include "ProjectSwiftHI.h"
#include "PicSolver.h"
#include "CurrentHeatSolver.h"


namespace femocs {

template <int dim>
ProjectSwiftHI<dim>::ProjectSwiftHI(AtomReader &reader, Config &config) :
        GeneralProject(reader, config),
        phys_quantities(config.heating),
        pic(&emission, config, config.behaviour.rnd_seed),
        emission(&pic, config, NULL),
        ch_solver(&phys_quantities, &config.heating,
                emission.get_surface_heat(), emission.get_current_densities())
{
    double t0;

    if (conf.path.bulk_mesh_file != ""){
        start_msg(t0, "Reading bulk mesh from " + conf.path.bulk_mesh_file + " and setting up emission");
        ch_solver.import_mesh(conf.path.bulk_mesh_file);
        ch_solver.setup(300., true);
        emission.set_chsolver(&ch_solver);
        end_msg(t0);
    }

    start_msg(t0, "Reading vacuum mesh from " + conf.path.vacuum_mesh_file + " and setting up solvers");
    pic.import_mesh(conf.path.vacuum_mesh_file);
    pic.setup(conf.field.E0, conf.field.V0, conf.behaviour.axisymmetry);
    pic.write("mesh.msh", FileIO::no_update);
    phys_quantities.initialize();
    emission.initialize();
    end_msg(t0);
}

template <int dim>
int ProjectSwiftHI<dim>::run(const int timestep, const double time){
    pic.run(conf.behaviour.timestep_fs, true);
    return 0;
}

template <int dim>
void ProjectSwiftHI<dim>::write_output(double factor, bool first_line){
    ofstream out;
//    out.open("out/results_SC.dat", ofstream::out | ofstream::app);
//    out.setf(std::ios::scientific);
//    out.precision(6);
//
//    if (first_line)
//        emission.write_stats(out, #  Factor      ");
//
//    out << factor << ' ';
//    emission.write_stats(out);
//
//    out.close();
}


template class ProjectSwiftHI<2>;
template class ProjectSwiftHI<3>;

} /* namespace femocs */
