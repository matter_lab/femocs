/*
 * ParticleSet.cpp
 *
 *  Created on: May 26, 2020
 *      Author: kyritsak
 */

#include "ParticleSet.h"
#include "Globals.h"
#include "Emission.h"
#include "FieldSolver.h"
#include "Macros.h"
#include "Interpolator.h"
#include "PicData.h"

#include <deal.II/base/work_stream.h>


namespace femocs {

template<int dim>
ParticleSet<dim>::ParticleSet(double mass0, double charge0, double Wsp,
        const FieldSolver<dim> *solver, const string &label) :
        q_over_m(charge0 / mass0), q_over_eps0(charge0 / CONSTANTS.eps0),
        label(label), solver(solver), Wsp(Wsp),
        interpolator(NULL),
        mass0(mass0), charge0(charge0),
        reflection(1)
{};

template<int dim>
Particle<dim> ParticleSet<dim>::operator [](const size_t i) const {
    require(i < positions.size(), "Invalid index: " + d2s(i));
    return Particle<dim>(positions[i], velocities[i], cells[i], weights[i]);
}

template<int dim>
double ParticleSet<dim>::calc_energy() const {
    double energy = 0.0;
    for (int i = 0; i < size(); ++i)
        energy += 0.5 * get_Msp(i) * velocities[i].norm_square();
    return energy;
}

template<int dim>
Vec3 ParticleSet<dim>::calc_momentum() const {
    Vec3 momentum(0);
    for (int i = 0; i < size(); ++i)
        momentum += velocities[i] * get_Msp(i);
    return momentum;
}

template<int dim>
void ParticleSet<dim>::inject(const Particle<dim> &sp) {
    require(sp.cell->state() == IteratorState::valid, "invalid injection cell");

    positions.push_back(sp.pos);
    velocities.push_back(sp.vel);
    unit_positions.push_back(Point<dim>());
    weights.push_back(sp.weight);
    cells.push_back(sp.cell);
    fields.push_back(Tensor<1,dim>());
    magnetic_fields.push_back(Tensor<1,dim>());
    add_map_entry(cells.size() - 1);
}

template<int dim>
void ParticleSet<dim>::resize(int N) {
    positions.resize(N);
    velocities.resize(N);
    unit_positions.resize(N);
    weights.resize(N);
    cells.resize(N);
    fields.resize(N);
    magnetic_fields.resize(N);
}

template<int dim>
void ParticleSet<dim>::clear() {
    positions.clear();
    velocities.clear();
    unit_positions.clear();
    weights.clear();
    cells.clear();
    fields.clear();
    magnetic_fields.clear();
}

template<int dim>
void ParticleSet<dim>::relocate_particle(int source, int dest) {
    erase_map_entry(source);

    positions[dest] = positions[source];
    velocities[dest] = velocities[source];
    weights[dest] = weights[source];
    fields[dest] = fields[source];
    magnetic_fields[dest] = magnetic_fields[source];
    unit_positions[dest] = unit_positions[source];
    cells[dest] = cells[source];

    add_map_entry(dest);
    
}

template<int dim>
void ParticleSet<dim>::regroup_all() {
    const int n_cells = solver->get_n_cells();
    cell2parts = vector<vector<int>>(n_cells);
    filled_cells.clear();
    for (int i = 0; i < size(); ++i)
        add_map_entry(i);
}

template<int dim>
void ParticleSet<dim>::sort() {
    std::sort(filled_cells.begin(), filled_cells.end());
    for (vector<int> &parts : cell2parts)
        std::sort(parts.begin(), parts.end());
}

template<int dim>
void ParticleSet<dim>::erase_map_entry(int i) {
    require(i < cells.size(), "Index out of bounds: " + d2s(i));
    Cell &cell = cells[i];
    uint cell_index = cell->index();
    vector<int> &indices = cell2parts[cell_index];

    // In case of empty vector, an overflow will occur
    // Therefore the check could be done against the vector size
    uint last_index = indices.size() - 1;
    require(last_index < indices.size(), "Could not remove entry from an empty vector!");

    // replace the entry that is occupied by i-th particle with the last entry
    int last_i = indices[last_index];
    // no need to search if the entry that needs to be deleted is in the end of the vector
    if (i != last_i) {
        bool entry_found = false;
        for (int &index : indices) {
            if (index == i) {
                entry_found = true;
                index = last_i;
                break;
            }
        }
        require(entry_found, "Particle " + d2s(i) + " was not found from a map "
                "associated with the cell " + d2s(cell_index));
    }

    // remove the last entry from indices
    indices.resize(last_index);

    if (last_index != 0) return;

    // if the cell becomes empty, remove it from the filled_cells
    last_index = filled_cells.size() - 1;
    require(last_index < filled_cells.size(), "Could not remove entry from an empty vector!");

    // replace the entry that is occupied by i-th particle with the last entry
    Cell last_cell = filled_cells[last_index];
    // no need to search if the entry that needs to be deleted is in the end of the vector
    if (cell != last_cell) {
        bool entry_found = false;
        for (Cell &c : filled_cells) {
            if (c == cell) {
                entry_found = true;
                c = last_cell;
                break;
            }
        }
        require(entry_found, "Cell " + d2s(cell_index) + " was not found from a filled_cells!");
    }

    // remove the last entry from filled_cells
    filled_cells.resize(last_index);
}

template<int dim>
void ParticleSet<dim>::add_map_entry(int id) {
    require(id >= 0 && id < cells.size(), "Invalid index: " + d2s(id));
    const uint cell_index = cells[id]->index();

    require(cell_index < cell2parts.size(), "Mismatch between cell index and map size: "
            + d2s(cell_index) + " vs " + d2s(cell2parts.size()));
    if (cell2parts[cell_index].size() == 0)
        filled_cells.push_back(cells[id]);
    cell2parts[cell_index].push_back(id);
}

template<int dim>
int ParticleSet<dim>::apply_reflective(int i){
    // return if the point is inside the mesh or there is no reflection
    if (!reflection || solver->bbox.point_inside(this->positions[i]))
        return 0;

    const pair<Point<dim>, Point<dim>> &bpoints = solver->bbox.get_boundary_points();
    double left_bound = min(bpoints.first(0), bpoints.second(0));
    double right_bound = max(bpoints.first(0), bpoints.second(0));

    if (double diff = this->positions[i](0) - left_bound < 0){//left reflection
        this->positions[i](0) += 2 * diff; //reflect position
        this->velocities[i][0] *= -1;  //reflect velocity
    }

    if (reflection == 1) //if only left boundary reflection
        return 0;

    if (double diff = this->positions[i](0) - right_bound > 0){//right reflection
        this->positions[i](0) -= 2 * diff; //reflect position
        this->velocities[i][0] *= -1;  //reflect velocity
    }

    if (dim == 3 || reflection == 3){ // if 3D and side reflection or 2D and all reflection

        double front_bound = min(bpoints.first(1), bpoints.second(1));
        double back_bound = max(bpoints.first(1), bpoints.second(1));

        if (double diff = this->positions[i](1) - front_bound < 0){//front reflection
            this->positions[i](0) += 2 * diff; //reflect position
            this->velocities[i][0] *= -1;  //reflect velocity
        }

        if (double diff = this->positions[i](1) - back_bound > 0){//back reflection
            this->positions[i](0) -= 2 * diff; //reflect position
            this->velocities[i][0] *= -1;  //reflect velocity
        }
    }

    if (dim == 3 && reflection == 3){
        double bottom_bound = min(bpoints.first(1), bpoints.second(1));
        double top_bound = max(bpoints.first(1), bpoints.second(1));

        if (double diff = this->positions[i](2) - bottom_bound < 0){//bottom reflection
            this->positions[i](0) += 2 * diff; //reflect position
            this->velocities[i][0] *= -1;  //reflect velocity
        }

        if (double diff = this->positions[i](2) - top_bound > 0){//top reflection
            this->positions[i](0) -= 2 * diff; //reflect position
            this->velocities[i][0] *= -1;  //reflect velocity
        }
    }
    return 0;
}

template<int dim>
void ParticleSet<dim>::update_position_parallel(const PointIterator &posit,
        const ScratchData &sd, CopyData &cd, double dt)
{
    int i = posit - this->positions.begin();
    Point<dim> &pos = this->positions[i];
    Vec3 &vel = this->velocities[i];
    for (int j = 0; j < dim; ++j)
        pos[j] += vel[j] * dt;
    apply_reflective(i);

    cd.index = i;
    cd.searches = find_new_cell(i, dt, cd.new_cell, cd.face);
}

template<int dim>
int ParticleSet<dim>::update_positions(double delta_t, bool clear) {
    int total_searches = 0;

    WorkStream::run(this->positions.begin(), this->positions.end(),
            std::bind(&ParticleSet<dim>::update_position_parallel,
                    this,
                    std::placeholders::_1, // with arg1 the position iterator
                    std::placeholders::_2, // and arg2 the Scratch Data
                    std::placeholders::_3, // and arg3 the CopyData
                    delta_t),              // and arg4 delta_t
            std::bind(&ParticleSet<dim>::copier,
                    this,
                    std::placeholders::_1,// with arg1 the CopyData
                    ref(total_searches)), // and arg2 the total_searches to add
            ScratchData(),                // make dummy ScratchData object
            CopyData(),                   // make CopyData object
            4 * MultithreadInfo::n_threads(), // #items than can be alive at a given time
            16  // #items to be worked by the same thread
    );

    if (solver->axisymmetric())
        this->rotate_2d3v(delta_t);
    if (clear) this->clear_lost();
    return total_searches;
}

// Nothing to do in 3D case
template<int dim>
void ParticleSet<dim>::rotate_2d3v(double delta_t) {}

template<>
void ParticleSet<2>::rotate_2d3v(double delta_t) {
    /* After particle push the particle has moved away
     * from xy-plane due to non-zero vz.
     * We can move back to 2D coordinates by rotating
     * the coordinate system around y-axis until z-coordinate = 0.
     *
     * Mathematically the rotation of x & z-coordinates looks like this
     * (xyz & xyz' are coordinates before & after rotation, phi rotation angle):
     *
     * | x'| = | cos_phi sin_phi | * | x |  => x' = sqrt(x^2 + z^2)
     * | z'|   |-sin_phi cos_phi |   | z |     y' = y
     *                                         z' = 0
     *
     * After rotating coordinates, also the velocities need to be rotated
     * (we use relations sin_phi = z / x', cos_phi = x / x'):
     *
     * | vx'| = | cos_phi sin_phi | * | vx | => vx' = vx * x / x' + vz * z / x'
     * | vz'|   |-sin_phi cos_phi | * | vz |    vy' = vy
     *                                          vz' =-vx * z / x' + vz * x / x'
     */

    const double epsilon = 1e-8;

    for (int i = 0; i < this->size(); ++i) {
        Vec3 &vel = this->velocities[i];
        double &x = this->positions[i][0];

        double z = vel.z * delta_t;
        double x_rot = sqrt(x * x + z * z);

        // For the sake of numerical stability, don't do very small rotations
        if (x_rot < epsilon)
            continue;

        double vx_rot =  vel.x * x / x_rot + vel.z * z / x_rot;
        double vz_rot = -vel.x * z / x_rot + vel.z * x / x_rot;
        vel.x = vx_rot;
        vel.z = vz_rot;

        x = x_rot;
    }
}

template<int dim>
void ParticleSet<dim>::read(const string &filename) {
    string ftype = get_file_type(filename);
    require(ftype == "restart", "Unimplemented file type: " + ftype);

    ifstream in(filename);
    require(in.is_open(), "Can't open a file " + filename);

    const string start_str = "$" + get_restart_label();
    string str;

    regroup_all();

    while (in >> str) {
        if (str == start_str) {
            clear();

            int n_particles;
            in >> n_particles >> GLOBALS.TIME >> GLOBALS.TIMESTEP;
            getline(in, str);

            Particle<dim> particle;
            for (int i = 0; i < n_particles; ++i) {
                in.read(reinterpret_cast<char*>(&particle), sizeof(Particle<dim>));

                Cell cell(solver->get_triangulation(), 0, particle.cell_ind,
                        solver->get_dof_handler());

                particle.cell = cell;
                inject(particle);
            }
        }
    }

    in.close();
}

template<int dim>
void ParticleSet<dim>::write_xyz(ofstream &out) const {
    FileWriter::write_xyz(out);

    // write Ovito header
    out << "properties=id:I:1:pos:R:" << dim << ":velo:R:3:cell:I:1:weight:R:1"
        << ":force:R:" << dim << ":mag_force:R:3" << endl;

    if (this->size() == 0)
        out << "0 " << Point<dim>() << ' ' << Vec3() << " 0 0.0 "
            << Tensor<1,dim>() << ' ' << Vec3() << endl;
    else {
        for (int i = 0; i < this->size(); ++i)
            out << i << ' ' << (*this)[i] << ' ' << fields[i] << ' ' << magnetic_fields[i] << endl;
    }
}

template<int dim>
void ParticleSet<dim>::write_bin(ofstream &out) const {
    for (int i = 0; i < this->size(); i++){
        Particle<dim> sp = (*this)[i];
        out.write((char*)&sp, sizeof (Particle<dim>));
    }
}

template<int dim>
int ParticleSet<dim>::update_cell(int i, double dt){
    int face;
    Cell new_cell = this->cells[i];
    int searches = find_new_cell(i, dt, new_cell, face);
    update_pcd(i, new_cell);

    return searches;
}

template<int dim>
void ParticleSet<dim>::update_pcd(int i, const Cell &new_cell) {
    if (new_cell == this->cells[i])
        return;

    erase_map_entry(i);
    this->cells[i] = new_cell; // update the cell in the list
    add_map_entry(i);
}

template<int dim>
int ParticleSet<dim>::find_new_cell(int i, double dt, Cell &new_cell, int &face){
    const Point<dim> &point = this->positions[i];

    if (!solver->bbox.point_inside(point))
        return -1;

    new_cell = this->cells[i];
    Point<dim> &upoint = this->unit_positions[i];

    if (interpolator && dim == 3) {
        int cell_index = new_cell->index();
        cell_index = interpolator->linhex.deal2femocs(cell_index);
        cell_index = interpolator->linhex.locate_cell(Point3(point[0], point[1], point[2]), cell_index);
        if (cell_index >= 0) {
            cell_index = interpolator->linhex.femocs2deal(cell_index);
            if (cell_index >= 0)
                new_cell = Cell(solver->get_triangulation(), 0, cell_index, solver->get_dof_handler());
        }
    }

    if (dt != 0.0)  {
        Tensor<1,dim> displacement;
        for (int j = 0; j < dim; ++j)
            displacement[j] = this->velocities[i][j] * dt;
        return this->solver->utilities.locate_cell(point, upoint, new_cell, displacement, face);
    }

    return this->solver->utilities.locate_cell(point, upoint, new_cell);
}

template<int dim>
int ParticleSet<dim>::clear_lost() {
    int npart = this->size();
    int nlost = 0; //keep number of total lost particles

    int j = npart; // index starting from the end of the vector
    for (int i = 0; i < j; i++) {
        // If particle i is out of the mesh domain
        if (this->weights[i] == 0.0) {
            nlost++;
            erase_map_entry(i);
            for (j--; j > i; j--) {
                // If particle j is in the mesh domain
                if (this->weights[j] != 0.0) {
                    relocate_particle(j, i);
                    break;
                } else {
                    erase_map_entry(j);
                    nlost++;
                }
            }
        }
    }

    // Shrink the array
    if (nlost > 0) resize(npart-nlost);
    return nlost;
}

template<int dim>
void ParticleSet<dim>::clear_lost(int i) {
    const int n_particles = this->size();
    const int last_index = n_particles - 1;
    erase_map_entry(i);
    if (last_index > i) relocate_particle(last_index, i);
    resize(last_index);
}

template<int dim>
void ParticleSet<dim>::rnd_points_on_face(const Cell&, int, vector<Point<dim>>&) const {
    throw runtime_error("Unimplemented function: " + d2s(__PRETTY_FUNCTION__));
}

template<>
void ParticleSet<2>::rnd_points_on_face(const Cell &cell, int f,
        vector<Point<2>> &points) const
{
    Face face = cell->face(f);
    Tensor<1,2> dvertex = face->vertex(1) - face->vertex(0);
    for (int i = 0; i < points.size(); i++) {
        points[i] = face->vertex(0) + solver->rnd() * dvertex;
        Point<2> upoint = GeometryInfo<2>::project_to_unit_cell(
                StaticMappingQ1<2>::mapping.transform_real_to_unit_cell(cell, points[i]));
        points[i] = StaticMappingQ1<2>::mapping.transform_unit_to_real_cell(cell, upoint);
    }
}

template<>
void ParticleSet<3>::rnd_points_on_face(const Cell &cell, int f,
        vector<Point<3>> &points) const
{
    Face face = cell->face(f);
    Tensor<1,3> center = face->center();
    Tensor<1,3> normal;
    Tensor<1,3> max_rad;
    solver->utilities.get_face_normal(cell, f, normal);

    double max_dist = 0.;
    for (int i = 0; i < GeometryInfo<3>::vertices_per_face; i++){
        Tensor<1,3> rad = face->vertex(i) - center;
        double dist = rad.norm_square();
        if (dist > max_dist) {
            max_rad = rad;
            max_dist = dist;
        }
    }

    Tensor<1,3> dir2 = cross_product_3d(max_rad, normal);
    dir2 /= dir2.norm();

    for (int i = 0; i < points.size(); i++) {
        Point<3> &point = points[i];

        // limit the amount of tries to find a point that lays inside the face
        int j = 0;
        for (; j < 100; ++j) {
            double u1 = solver->rnd() * 2.0 - 1.0;
            double u2 = solver->rnd() * 2.0 - 1.0;
            point = center + max_rad * u1 + dir2 * u2;
            if (solver->utilities.point_inside_face(point, cell, f))
                break;
        }
        require(j < 100, "Did not manage to generate a point on a face " + d2s(i));
        if (j >= 100) point = center;

        Point<3> upoint = GeometryInfo<3>::project_to_unit_cell(
                StaticMappingQ1<3>::mapping.transform_real_to_unit_cell(cell, points[i]));
        points[i] = StaticMappingQ1<3>::mapping.transform_unit_to_real_cell(cell, upoint);
    }
}

template<int dim>
double ParticleSet<dim>::calc_number_density(const Cell &cell) const
{
    vector<int> parts = parts_at(cell);
    double vol = solver->get_cell_vol(cell);
    return parts.size() / vol;
}

template<int dim>
Vec3 ParticleSet<dim>::rnd_direction(const Tensor<1,dim> &normal, double tol) const {
    Vec3 vel;
    do {
        // pick a random point on unit sphere by the method described here:
        // https://mathworld.wolfram.com/SpherePointPicking.html

        // rejection method
        /*double u1 = solver->rnd() * 2.0 - 1.0;
        double u2 = solver->rnd() * 2.0 - 1.0;
        double sum_sqr = u1 * u1 + u2 * u2;
        if (sum_sqr > 1.0)
            continue;

        double root_factor = sqrt(1 - sum_sqr);
        vel[0] = 2 * u1 * root_factor;
        vel[1] = 2 * u2 * root_factor;
        vel[2] = 1 - 2 * sum_sqr;*/

        // analytical method
        double phi = 2. * M_PI * solver->rnd();
        double theta = acos(2. * solver->rnd() - 1.);

        vel[0] = sin(theta) * cos(phi);
        vel[1] = sin(theta) * sin(phi);
        vel[2] = cos(theta);
    } while(vel * normal < tol); //check if direction is inwards

    return vel;
}

/* ===================================================================== */
/*                              Neutrals                                 */
/* ===================================================================== */

template<int dim>
Neutrals<dim>::Neutrals(double mass0, double Wsp,
        const FieldSolver<dim> *solver) :
        ParticleSet<dim>(mass0, 0.0, Wsp, solver, "Neutrals")
{};

template<int dim>
void Neutrals<dim>::copier(const CopyData &cd, int &total_searches) {
    // if the particle has left the simubox...
    if (cd.searches < 0) {
        this->weights[cd.index] = 0.0;
        return;
    }
    this->update_pcd(cd.index, cd.new_cell);
    total_searches += cd.searches;
}

template<int dim>
double Neutrals<dim>::velocity(double m, double T) const {
    // TODO: take into account evaporation potential energy!
    double p;
    do {
        p = this->solver->rnd();
    } while (p == 0.);
    const double E = -CONSTANTS.kboltz * T * log(p);
    return sqrt(2.0 * E / m);
}

template<int dim>
double Neutrals<dim>::inject_face(const Emission<dim> &emission, int i, double delta_t,
        const Config::PIC &conf, const double vnum, const double E0)
{
    double total_injected = 0.; // sum of injected weights

    Tensor<1, dim> face_normal;

    double weight = conf.weight_neutral;

    if (conf.weight_neutral <= 0.) { // weight not specified, scale automatically
        double density = this->calc_number_density(emission.get_cell(i));

        if (conf.weight_min > 0.) {
            // increase weight of neutrals based on density and number evaporated, inject integer number of particles
            weight = min(ceil(max(1.0, sqrt((density / conf.density_max) * (vnum / conf.weight_min)))) * conf.weight_min, conf.weight_max);
        } else {
            weight = min(vnum, conf.weight_max);
        }
    }

    if (weight <= 0.)
        return 0;

    int face_injections = (int) floor(vnum/weight);
    if (this->solver->rnd() < vnum/weight - face_injections)
        face_injections++;

    if (face_injections <= 0)
        return 0;

    // ensure that the amount of injected neutrals does not grow too large
    if (face_injections > conf.max_injected)
        return -face_injections;

    Cell cell = emission.get_cell(i);
    int face = emission.get_face(i);
    vector<Point<dim>> points(face_injections);
    this->rnd_points_on_face(cell, face, points);

    this->solver->utilities.get_face_normal(cell, face, face_normal);
    total_injected += face_injections * weight;

    require(cell->state() == IteratorState::valid,
            "Neutral injection in cell " + d2s(i) + " is invalid");

    // loop through random points on face
    for (int j = 0; j < face_injections; ++j) { // total energy = E0 * weight * face_injections
        Vec3 vel = this->rnd_direction(face_normal);
        // multiply by energy to get correct magnitude
        vel *= sqrt(2. * E0 / (conf.vapor_mass * CONSTANTS.amu)); // velocity of one atom
        ParticleSet<dim>::inject(Particle<dim>(points[j], vel, cell, weight)); // E = E0 * weight
    }

    return total_injected;
}

template<int dim>
double Neutrals<dim>::inject_sput(const Emission<dim> &emission, double delta_t,
        const Config::PIC &conf, const Ions<dim> &ions)
{
    double total_injected = 0.;

    if (!conf.inject_neutrals)
        return 0;

    for (int i = 0; i < ions.sput_faces.size(); ++i) {
        for (int j = 0; j < emission.size(); ++j) {
            Cell cell = emission.get_cell(j);
            Face face = cell->face(emission.get_face(j));

            if (ions.sput_faces[i] == face) {
                double vnum = ions.sput_yields[i];

                total_injected += inject_face(emission, j, delta_t, conf, vnum, ions.sput_energies[i]);
            }
        }
    }

    return total_injected;
}

template<int dim>
double Neutrals<dim>::inject(const Emission<dim> &emission, double delta_t,
        const Config::PIC &conf)
{
    double total_injected = 0.;

    if (!conf.inject_neutrals)
        return 0;

    // loop through faces on surface
    for (int i = 0; i < emission.size(); ++i) {
        double T = emission.get_temperature(i);
        double area = emission.get_area(i);
        double vnum = emission.get_vapor_flux(i) * delta_t * area;

        if (T <= 0.0)
            continue;

        double E0 = velocity(conf.vapor_mass * CONSTANTS.amu, T);
        E0 = (conf.vapor_mass * CONSTANTS.amu) * E0 * E0 / 2.;

        total_injected += inject_face(emission, i, delta_t, conf, vnum, E0);
    }

    return total_injected;
}

/* ===================================================================== */
/*                          ChargedParticles                             */
/* ===================================================================== */

template<int dim>
ChargedParticles<dim>::ChargedParticles(double mass0, double charge0, double Wsp,
        const FieldSolver<dim> *solver, const string &label) :
        ParticleSet<dim>(mass0, charge0, Wsp, solver, label),
        current(0)
{};

template<int dim>
void ChargedParticles<dim>::inject(const Particle<dim> &sp) {
    ParticleSet<dim>::inject(sp);
    lfields.push_back(Tensor<1,dim>());
}

template<int dim>
void ChargedParticles<dim>::resize(int N) {
    ParticleSet<dim>::resize(N);
    lfields.resize(N);
}

template<int dim>
void ChargedParticles<dim>::clear() {
    ParticleSet<dim>::clear();
    lfields.clear();
}

template<int dim>
void ChargedParticles<dim>::relocate_particle(int source, int dest) {
    ParticleSet<dim>::relocate_particle(source, dest);
    lfields[dest] = lfields[source];
}

template<int dim>
int ChargedParticles<dim>::update_positions(double delta_t, bool clear) {
    current = 0.0;
    return ParticleSet<dim>::update_positions(delta_t, clear);
}

template<int dim>
void ChargedParticles<dim>::update_velocities(double delta_t) {
    for  (int i = 0; i < this->size(); ++i) {
        Vec3 electric_acceleration = this->fields[i] * this->q_over_m;
        Vec3 v = this->velocities[i];
        Vec3 B = this->magnetic_fields[i];
        Vec3 magnetic_acceleration = v.crossProduct(B) * this->q_over_m;
        this->velocities[i] += (electric_acceleration + magnetic_acceleration) * delta_t;
    }
}

/* ===================================================================== */
/*                               Electrons                               */
/* ===================================================================== */

template<int dim>
Electrons<dim>::Electrons(double Wsp, const FieldSolver<dim> *solver) :
        ChargedParticles<dim>(CONSTANTS.me, -1.0, Wsp, solver, "Electrons")
{};

template<int dim>
void Electrons<dim>::copier(const CopyData &cd, int &total_searches) {
    // if the particle has left the simubox...
    if (cd.searches < 0) {
        this->weights[cd.index] = 0.0;
        return;
    }

    this->current += this->velocities[cd.index] * this->lfields[cd.index] * (1e15 * this->get_Csp() / CONSTANTS.e_charge);
    this->update_pcd(cd.index, cd.new_cell);
    total_searches += cd.searches;
}

template<int dim>
int Electrons<dim>::update_positions_serial(double delta_t, bool clear) {
    int total_searches = 0;
    this->current = 0.0;

    const int n_particles = this->size();
    for (int i = 0; i < n_particles; ++i) {
        for (int j = 0; j < dim; ++j)
            this->positions[i][j] += this->velocities[i][j] * delta_t;
        this->apply_reflective(i);

        int face = 0;
        Cell cell;
        int searches = this->find_new_cell(i, delta_t, cell, face);

        // mark the particles that are out of the simubox with zero weight
        if (searches < 0) {
            this->weights[i] = 0.0;
            continue;
        }

        total_searches += searches;
        this->current += this->velocities[i] * this->lfields[i] * (1e15 * this->get_Csp() / CONSTANTS.e_charge);
        this->update_pcd(i, cell);
    }

    if (clear) this->clear_lost();
    return total_searches;
}

template<int dim>
double Electrons<dim>::inject(const Emission<dim> &emission, double delta_t,
        const Config::PIC &conf)
{
    double total_injected = 0.; // sum of injected weights

    // total charge to be injected
    const double total_charge = emission.global_data.I_tot * CONSTANTS.ampere * delta_t;
    // minimum face  current percentage for injection of a particle.
    const double injection_cutoff = 1.e-3;

    // loop through faces on surface
    for (int i = 0; i < emission.size(); ++i) {
        Cell cell = emission.get_cell(i);
        int face = emission.get_face(i);

        // charge to be injected in the face [e]
        double charge = emission.get_current(i) * CONSTANTS.ampere * delta_t;
        double face_weight = 0.0;
        int face_injections = 0;

        if (this->Wsp > 0.0) {
            face_weight = this->Wsp;
            double n_sps = charge / this->Wsp;
            face_injections = (int) floor(n_sps);
            if (this->solver->rnd() < (n_sps - face_injections))
                face_injections++;
        } else {
            // desired # injections on face
            double n_sps = conf.injections * charge / total_charge;
            if (n_sps >= injection_cutoff) {
                face_injections = max(1, (int) round(n_sps));
                face_weight = charge / face_injections;
            }
        }

        // if no injection on this face continue to save time
        if (face_injections == 0)
            continue;

        // ensure that the amount of injected electrons does not grow too large
        if (face_injections > conf.max_injected)
            return -face_injections;

        vector<Point<dim>> points(face_injections);
        vector<Tensor<1,dim>> fields(face_injections);
        vector<Cell> cells(face_injections);
        for (auto &c : cells)
            c = cell;

        this->rnd_points_on_face(cell, face, points);
        this->solver->probe_field(points, cells, fields);

        // loop through the generated random points on the face
        for (int j = 0; j < face_injections; j++) {
            const Vec3 vel = fields[j] * (this->solver->rnd() * delta_t * this->q_over_m);
            Point<dim> &pos = points[j];

            // Random fractional push
            if (conf.fractional_push) {
                Vec3 delta_pos = vel * (delta_t * this->solver->rnd());
                for (int k = 0; k < dim; ++k)
                    pos[k] += delta_pos[k];
            }

            inject(Particle<dim>(pos, vel, cell, face_weight));
        }

        total_injected += face_injections * face_weight;
    }

    return total_injected;
}

template<int dim>
double Electrons<dim>::inject_paths(const Emission<dim> &emission, double delta_t) {
    double total_injected = 0.; // sum of injected weights
    // total charge to be injected
    double total_charge = emission.global_data.I_tot * CONSTANTS.ampere * delta_t;
    // minimum face  current percentage for injection of a particle
    const double injection_cutoff = 1.e-5;

    // loop through faces on surface
    for (int i = 0; i < emission.size(); ++i) {
        // charge to be injected in the face [e]
        double charge = emission.get_current(i) * CONSTANTS.ampere * delta_t;

        if (charge > total_charge * injection_cutoff) {
            const Cell &cell = emission.get_cell(i);
            const Point<dim> &point = emission.get_centroid(i);
            this->inject(Particle<dim>(point, Vec3(), cell, charge));
            this->update_cell(this->size() - 1, delta_t);
            total_injected += charge;
        }
    }

    return total_injected;
}

template<int dim>
int Electrons<dim>::read_table(const string &filepath) {
    double t0;
    start_msg(t0, "Reading injection table from " + filepath);
    ifstream infile(filepath);
    require(infile.is_open(), "Could not open " + filepath);

    int n_particles;
    infile >> n_particles;
    this->resize(n_particles);

    string line;
    getline(infile, line);  // flush the first line
    getline(infile, line);

    bool timed = line.find("t[fs]") != string::npos; // check if header has time
    double total_electrons = 0.0;

    if (timed) {
        injection_times.resize(n_particles);
        // TODO It would be more foolproof to implement it with regular expression
        int ipos = line.find("nemitted = ");
        stringstream ss;
        ss << line.substr(ipos + 11);
        ss >> total_electrons;
    }

    Cell cell = this->solver->get_dof_handler()->end();

    for (int i = 0; i < n_particles; i++) {
        infile >> this->positions[i][0]
               >> this->velocities[i][0]
               >> this->velocities[i][1];
        this->positions[i][1] = 1.0e-10;
        this->cells[i] = cell;

        if (timed) {
            infile >> injection_times[i];
            this->weights[i] = total_electrons / n_particles;
        } else
            this->weights[i] = this->Wsp;
    }

    infile.close();
    end_msg(t0);
    return 0;
}

template<int dim>
double Electrons<dim>::inject_table(const Electrons &table, double delta_t,
        const Config::PIC &conf)
{
    double total_injected = 0.; // sum of injected weights

    if (GLOBALS.TIME > conf.injection_interval)
        return 0;

    int old_index = inj_index;

    for (int i = 0; i < conf.injections; i++) {
        if (inj_index >= table.size())
            return inj_index - old_index;

        this->inject(table[inj_index]);
        this->update_cell(this->size() - 1, delta_t);

        total_injected += table.get_Wsp(inj_index);

        inj_index++;
        if ((table.injection_times.size() > 0) &&
                (table.injection_times[inj_index] > GLOBALS.TIME + conf.dt_max))
            break;
    }

    return total_injected;
}

/* ===================================================================== */
/*                                IonSet                                 */
/* ===================================================================== */

template<int dim>
IonSet<dim>::IonSet(double mass0, int ionization, double Wsp,
        const FieldSolver<dim> *solver, Ions<dim> *ions) :
        ChargedParticles<dim>(mass0 - ionization * CONSTANTS.me,
                (double) ionization, Wsp, solver, "Ions_" + d2s(ionization)),
        ions(*ions)
{};

template<int dim>
void IonSet<dim>::copier(const CopyData &cd, int &total_searches) {
    // if the particle has left the simubox...
    if (cd.searches < 0) {
        if (cd.searches == -BoundaryID::copper_surface && cd.face >= 0) {
            double E0 = this->velocities[cd.index].norm_square() * this->mass0 / 2.0; // energy of one atom
            double weight = this->weights[cd.index];

            double Es = ions.calc_sput(E0, weight); // returns total energy lost to sputtering
            ions.sput_faces.push_back(cd.new_cell->face(cd.face));

            ions.bombard_energies.push_back(max(0., E0*weight-Es)); // remaining energy deposited as bombardment heat
            ions.bombard_faces.push_back(cd.new_cell->face(cd.face));

            ions.cathode_deposited_charge += this->get_Csp();
        }
        this->weights[cd.index] = 0.0;
        return;
    }
    this->current += this->velocities[cd.index] * this->lfields[cd.index] * (1e15 * this->get_Csp() / CONSTANTS.e_charge);
    this->update_pcd(cd.index, cd.new_cell);
    total_searches += cd.searches;
}

/* ===================================================================== */
/*                                 Ions                                  */
/* ===================================================================== */


template<int dim>
Ions<dim>::Ions(double mass0, double Wsp,
        const PicData *pic_data, const FieldSolver<dim> *solver) :
        cathode_deposited_charge(0), pic_data(pic_data)
{
    for (int i = 1; i <= pic_data->ionization_levels; ++i)
        ions.push_back(IonSet<dim>(mass0, i, Wsp, solver, this));
};

template<int dim>
void Ions<dim>::write(const string &file_name, unsigned int flags) {
    string extension = get_file_type(file_name);
    string name_prefix = remove_extension(file_name);
    uint i = 1;
    for (auto &ion : ions)
        ion.write(name_prefix + '_' + d2s(i++) + '.' + extension, flags);
}

template<int dim>
double Ions<dim>::calc_energy() const {
    double energy = 0.0;
    int ionization_level = 1;

    for (const IonSet<dim> &ion : ions) {
        energy += ion.calc_energy();

        double ionization_energy = pic_data->sum_ionization_energy(ionization_level++);
        for (int i = 0; i < ion.size(); ++i)
            energy += ionization_energy * ion.get_Wsp(i);
    }

    return energy;
}

template<int dim>
Vec3 Ions<dim>::calc_momentum() const {
    Vec3 momentum(0);
    for (const auto &ion : ions)
        momentum += ion.calc_momentum();
    return momentum;
}

template<int dim>
double Ions<dim>::calc_sput(double E0, double weight) {
    double Es = pic_data->get_sput_energy(E0, 0.); // get sputtering energy of one neutral
    sput_energies.push_back(Es);
    sput_yields.push_back(pic_data->get_sput_yield(E0, 0.) * weight); // sputtering yield of one * weight
    return Es * weight; // return total energy
}

template<int dim>
IonSet<dim>& Ions<dim>::operator [](const size_t i) {
    require(i < ions.size(), "Invalid index: " + d2s(i));
    return ions[i];
}

template<int dim>
const IonSet<dim>& Ions<dim>::operator [](const size_t i) const {
    require(i < ions.size(), "Invalid index: " + d2s(i));
    return ions[i];
}

template class Particle<2>;
template class Particle<3>;
template class ParticleSet<2>;
template class ParticleSet<3>;
template class Neutrals<2>;
template class Neutrals<3>;
template class ChargedParticles<2>;
template class ChargedParticles<3>;
template class Electrons<2>;
template class Electrons<3>;
template class IonSet<2>;
template class IonSet<3>;
template class Ions<2>;
template class Ions<3>;


} /* namespace femocs */
