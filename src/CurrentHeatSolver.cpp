/*
 * currents_and_heating.cc -> CurrentsAndHeating.cpp
 *
 *  Created on: Jul 28, 2016
 *      Author: kristjan, Mihkel
 */

#include <deal.II/numerics/data_out.h>
#include <deal.II/base/work_stream.h>

#include "CurrentHeatSolver.h"
#include "Macros.h"


namespace femocs {
using namespace dealii;
using namespace std;

// ----------------------------------------------------------------------------------------
/* Class for outputting the current density distribution
 * being calculated from current potential distribution */
template <int dim>
class CurrentPostProcessor : public DataPostprocessorVector<dim> {
public:
    CurrentPostProcessor() : DataPostprocessorVector<dim>("current_density", update_gradients) {}

    virtual void evaluate_scalar_field(
        const DataPostprocessorInputs::Scalar<dim> &input_data,
        std::vector<Vector<double>> &computed_quantities) const override
    {
        AssertDimension(input_data.solution_gradients.size(),
                        computed_quantities.size());

        for (unsigned int p = 0; p < input_data.solution_gradients.size(); ++p) {
            AssertDimension(computed_quantities[p].size(), dim);
            for (unsigned int d = 0; d < dim; ++d)
                computed_quantities[p][d] = input_data.solution_gradients[p][d];
        }
    }

    void
    compute_derived_quantities_scalar (
            const vector<double>               &/*uh*/,
            const vector<Tensor<1,dim> >       &duh,
            const vector<Tensor<2,dim> >       &/*dduh*/,
            const vector<Point<dim> >          &/*normals*/,
            const vector<Point<dim> >          &/*evaluation_points*/,
            vector<Vector<double> >            &computed_quantities) const {
        for (unsigned int i=0; i<computed_quantities.size(); i++) {
            for (unsigned int d=0; d<dim; ++d)
                computed_quantities[i](d) = duh[i][d];
        }
    }
};
// ----------------------------------------------------------------------------------------
// Class for outputting the electrical conductivity distribution
template <int dim>
class SigmaPostProcessor : public DataPostprocessorScalar<dim> {
    const HeatData *pq;
public:
    SigmaPostProcessor(const HeatData *pq_) :
            DataPostprocessorScalar<dim>("conductivity", update_values), pq(pq_) {}

    virtual void evaluate_scalar_field(
            const DataPostprocessorInputs::Scalar<dim> &input_data,
            std::vector<Vector<double>> &computed_quantities) const override
    {
        AssertDimension(input_data.solution_values.size(),
                        computed_quantities.size());

        for (unsigned int i = 0; i < input_data.solution_values.size(); ++i)
        {
            double temperature = input_data.solution_values[i];
            computed_quantities[i](0) = pq->sigma(temperature);
        }
    }

    void
    compute_derived_quantities_scalar (
            const vector<double>               &uh,
            const vector<Tensor<1,dim> >       &/*duh*/,
            const vector<Tensor<2,dim> >       &/*dduh*/,
            const vector<Point<dim> >          &/*normals*/,
            const vector<Point<dim> >          &/*evaluation_points*/,
            vector<Vector<double> >            &computed_quantities) const {
        for (unsigned int i=0; i<computed_quantities.size(); i++) {
            double temperature = uh[i];
            computed_quantities[i](0) = pq->sigma(temperature);
        }
    }
};
// ----------------------------------------------------------------------------

/* ==================================================================
 *  ========================== BulkSolver ==========================
 * ================================================================== */

template<int dim>
BulkSolver<dim>::BulkSolver(Triangulation<dim> *tria, const HeatData *heat_data,
        const Config::Heating *config, const vector<double>* bcs) :
        DealSolver<dim>(tria), pq(heat_data), conf(config), bc_values(bcs)
        {}

template<int dim>
double BulkSolver<dim>::get_face_bc(const unsigned int face) const {
    require(bc_values && face < bc_values->size(), "Invalid index: " + d2s(face));
    return (*bc_values)[face];
}

/* ==================================================================
 *  ========================== HeatSolver ==========================
 * ================================================================== */

template<int dim>
HeatSolver<dim>::HeatSolver(Triangulation<dim> *tria,
        const CurrentSolver<dim> *cs, const HeatData *pq,
        const Config::Heating *conf, const vector<double>* bcs) :
        BulkSolver<dim>(tria, pq, conf, bcs),
        current_solver(cs), one_over_delta_time(0)
        {}

template<int dim>
void HeatSolver<dim>::write_vtk(ofstream& out) const {
    SigmaPostProcessor<dim> post_processor(this->pq);
    DataOut<dim> data_out;

    const int n_dofs = this->size();
    Vector<double> surface_heat(n_dofs);

    for (int i = 0; i < n_dofs; ++i)
        surface_heat[i] = total_heat(i) - joule_heat(i);

    data_out.attach_dof_handler(this->dof_handler);
    data_out.add_data_vector(this->solution, "temperature");
    data_out.add_data_vector(this->solution, post_processor);
    data_out.add_data_vector(total_heat, "total_heat");
    data_out.add_data_vector(surface_heat, "surface_heat");
    data_out.add_data_vector(joule_heat, "joule_heat");

    data_out.build_patches();
    data_out.write_vtk(out);
}

template<int dim>
void HeatSolver<dim>::setup_system(bool axisymmetry) {
    DealSolver<dim>::setup_system(axisymmetry);

    const unsigned int n_dofs = this->size();
    joule_heat.reinit(n_dofs);
    total_heat.reinit(n_dofs);
    this->dof_volume.resize(n_dofs);
}

template<int dim>
void HeatSolver<dim>::assemble(const double delta_time) {
    require(current_solver, "NULL current solver can't be used!");
    require(delta_time > 0, "Invalid delta time: " + d2s(delta_time));

    this->one_over_delta_time = 1.0 / delta_time;
    this->system_matrix = 0;
    this->system_rhs = 0;

    LinearSystem system(&this->system_rhs, &this->system_matrix);
    QGauss<dim> quadrature_formula(this->quadrature_degree);

    const unsigned int n_dofs = this->fe.dofs_per_cell;
    const unsigned int n_q_points = quadrature_formula.size();

    WorkStream::run(this->dof_handler.begin_active(),this->dof_handler.end(),
            std::bind(&HeatSolver<dim>::assemble_local_cell,
                    this,
                    std::placeholders::_1,
                    std::placeholders::_2,
                    std::placeholders::_3),
            std::bind(&HeatSolver<dim>::copy_global_cell,
                    this,
                    std::placeholders::_1,
                    std::ref(system)),
            ScratchData(this->fe, quadrature_formula, update_values | update_gradients | update_quadrature_points | update_JxW_values),
            CopyData(n_dofs, n_q_points)
    );

    if (this->write_time()) {
        this->calc_joule_heat();
        this->calc_dof_volumes();
        //temporarily total_heat keeps the previous T component of the rhs
        for (int i = 0; i < this->total_heat.size(); ++i)
            this->total_heat[i] = this->system_rhs[i] - this->joule_heat[i];
    }

    this->assemble_rhs(BoundaryID::copper_surface);

    if (this->write_time()){ // save total heat also
        for (int i = 0; i < this->total_heat.size(); ++i)
            this->total_heat[i] = this->system_rhs[i] - this->total_heat[i]; //subtract the previous T component
    }
    this->append_dirichlet(BoundaryID::copper_bottom, this->dirichlet_bc_value);
    this->apply_dirichlet();
}

template<int dim>
void HeatSolver<dim>::assemble_crank_nicolson(const double delta_time) {
    require(false, "Implementation of Crank-Nicolson assembly not verified!");

    /* TODO:
     * change temperature_grad to temperature
     * add prev_nottingham values
     * interpolate prev_potential and prev_nottingham for current mesh
     *   prev_potential could be read from heat_transfer
     *   for nottingham something else should be done...
     */

    /*
    require(current_solver, "NULL current solver can't be used!");

    const double gamma = cu_rho_cp / delta_time;
    this->system_matrix = 0;
    this->system_rhs = 0;

    QGauss<dim> quadrature_formula(this->quadrature_degree);
    QGauss<dim-1> face_quadrature_formula(this->quadrature_degree);

    // Heating finite element values
    FEValues<dim> fe_values(this->fe, quadrature_formula,
            update_values | update_gradients | update_quadrature_points | update_JxW_values);
    FEFaceValues<dim> fe_face_values(this->fe, face_quadrature_formula,
            update_values | update_quadrature_points | update_JxW_values);

    // Finite element values for accessing current calculation
    FEValues<dim> fe_values_current(current_solver->fe, quadrature_formula, update_gradients);

    const unsigned int dofs_per_cell = this->fe.dofs_per_cell;
    const unsigned int n_q_points = quadrature_formula.size();
    const unsigned int n_face_q_points = face_quadrature_formula.size();

    FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);
    Vector<double> cell_rhs(dofs_per_cell);

    vector<types::global_dof_index> local_dof_indices(dofs_per_cell);

    // ---------------------------------------------------------------------------------------------
    // The other solution values in the cell quadrature points
    vector<Tensor<1, dim>> potential_grads(n_q_points);
    vector<Tensor<1, dim>> prev_potential_grads(n_q_points);
    vector<double> prev_temperatures(n_q_points);
    vector<Tensor<1, dim>> prev_temperature_grads(n_q_points);
    // ---------------------------------------------------------------------------------------------

    typename DoFHandler<dim>::active_cell_iterator cell = this->dof_handler.begin_active();
    typename DoFHandler<dim>::active_cell_iterator current_cell = current_solver->dof_handler.begin_active();

    int face_index = 0;
    for (; cell != this->dof_handler.end(); ++cell, ++current_cell) {
        fe_values.reinit(cell);
        cell_matrix = 0;
        cell_rhs = 0;

        fe_values.get_function_values(this->solution_save, prev_temperatures);
        fe_values.get_function_gradients(this->solution_save, prev_temperature_grads);

        fe_values_current.reinit(current_cell);
        fe_values_current.get_function_gradients(current_solver->solution, potential_grads);
        fe_values_current.get_function_gradients(current_solver->solution_save, prev_potential_grads);

        // ----------------------------------------------------------------------------------------
        // Local matrix assembly
        // ----------------------------------------------------------------------------------------
        for (unsigned int q = 0; q < n_q_points; ++q) {

            double temperature = prev_temperatures[q];
            double kappa = this->pq->kappa(temperature);
            double sigma = this->pq->sigma(temperature);

            Tensor<1, dim> temperature_grad = prev_temperature_grads[q];

            double pot_grad_squared = potential_grads[q].norm_square();
            double prev_pot_grad_squared = prev_potential_grads[q].norm_square();

            for (unsigned int i = 0; i < dofs_per_cell; ++i) {
                for (unsigned int j = 0; j < dofs_per_cell; ++j) {
                    cell_matrix(i, j) += fe_values.JxW(q) * (
                            gamma*fe_values.shape_value(i, q) * fe_values.shape_value(j, q) // Mass matrix
                            + 0.5*kappa*fe_values.shape_grad(i, q) * fe_values.shape_grad(j, q) );
                }
                cell_rhs(i) += fe_values.JxW(q) * (
                        gamma*fe_values.shape_value(i, q)*temperature
                        - 0.5*kappa*fe_values.shape_grad(i, q)*temperature_grad  // TODO check this
                        + 0.5*sigma*fe_values.shape_value(i, q)*(pot_grad_squared+prev_pot_grad_squared) );
            }
        }

        // ----------------------------------------------------------------------------------------
        // Local right-hand side assembly
        // ----------------------------------------------------------------------------------------
        // Nottingham BC at the copper surface
        for (unsigned int f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f) {
            if (cell->face(f)->boundary_id() == BoundaryID::copper_surface) {
                fe_face_values.reinit(cell, f);
                double nottingham_heat = this->get_face_bc(face_index++);

                for (unsigned int q = 0; q < n_face_q_points; ++q) {
                    for (unsigned int i = 0; i < dofs_per_cell; ++i) {
                        cell_rhs(i) += fe_face_values.shape_value(i, q)
                                * nottingham_heat * fe_face_values.JxW(q);
                    }
                }
            }
        }

        cell->get_dof_indices(local_dof_indices);
        for (unsigned int i = 0; i < dofs_per_cell; ++i) {
            for (unsigned int j = 0; j < dofs_per_cell; ++j)
                this->system_matrix.add(local_dof_indices[i], local_dof_indices[j], cell_matrix(i, j));

            this->system_rhs(local_dof_indices[i]) += cell_rhs(i);
        }
    }
    //*/
}

template<int dim>
void HeatSolver<dim>::calc_joule_heat() {
    require(current_solver, "NULL current solver can't be used!");

    this->joule_heat = 0;

    QGauss<dim> quadrature_formula(this->quadrature_degree);

    // Heating finite element values
    FEValues<dim> fe_values(this->fe, quadrature_formula,
            update_values | update_gradients | update_quadrature_points | update_JxW_values);

    const unsigned int dofs_per_cell = this->fe.dofs_per_cell;
    const unsigned int n_q_points = quadrature_formula.size();

    vector<types::global_dof_index> local_dof_indices(dofs_per_cell);

    // The other solution values in the cell quadrature points
    vector<Tensor<1, dim>> potential_gradients(n_q_points);
    vector<double> prev_temperatures(n_q_points);

    typename DoFHandler<dim>::active_cell_iterator cell = this->dof_handler.begin_active();

    for (; cell != this->dof_handler.end(); ++cell) {
        fe_values.reinit(cell);
        fe_values.get_function_values(this->solution, prev_temperatures);
        fe_values.get_function_gradients(current_solver->solution, potential_gradients);

        cell->get_dof_indices(local_dof_indices);
        // Local joule heat vector assembly
        for (unsigned int q = 0; q < n_q_points; ++q) {
            double pot_grad_squared = potential_gradients[q].norm_square();
            double temperature = prev_temperatures[q];
            double rho = this->pq->evaluate_resistivity(temperature);
            double ax_factor = this->axisymmetry_factor(fe_values, q);

            for (unsigned int i = 0; i < dofs_per_cell; ++i) {
                this->joule_heat(local_dof_indices[i]) += fe_values.JxW(q) *
                        fe_values.shape_value(i, q) * rho * pot_grad_squared * ax_factor;
            }
        }
    }
}

template<int dim>
void HeatSolver<dim>::assemble_euler_implicit(const double delta_time) {
    require(current_solver, "NULL current solver can't be used!");

    const double gamma = cu_rho_cp / delta_time;
    this->system_matrix = 0;
    this->system_rhs = 0;

    QGauss<dim> quadrature_formula(this->quadrature_degree);

    // Heating finite element values
    FEValues<dim> fe_values(this->fe, quadrature_formula,
            update_values | update_gradients | update_quadrature_points | update_JxW_values);

    const unsigned int dofs_per_cell = this->fe.dofs_per_cell;
    const unsigned int n_q_points = quadrature_formula.size();

    FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);
    Vector<double> cell_rhs(dofs_per_cell);

    vector<types::global_dof_index> local_dof_indices(dofs_per_cell);

    // The other solution values in the cell quadrature points
    vector<Tensor<1, dim>> potential_gradients(n_q_points);
    vector<double> prev_temperatures(n_q_points);

    typename DoFHandler<dim>::active_cell_iterator cell = this->dof_handler.begin_active();

    for (; cell != this->dof_handler.end(); ++cell) {
        fe_values.reinit(cell);
        fe_values.get_function_values(this->solution, prev_temperatures);
        fe_values.get_function_gradients(current_solver->solution, potential_gradients);

        // Local matrix assembly
        cell_matrix = 0;
        for (unsigned int q = 0; q < n_q_points; ++q) {
            double temperature = prev_temperatures[q];
            double kappa = this->pq->kappa(temperature);
            double ax_factor = this->axisymmetry_factor(fe_values, q);

            for (unsigned int i = 0; i < dofs_per_cell; ++i) {
                for (unsigned int j = 0; j < dofs_per_cell; ++j) {
                    cell_matrix(i, j) += fe_values.JxW(q) * ax_factor * (
                            gamma * fe_values.shape_value(i, q) * fe_values.shape_value(j, q) // Mass matrix
                            + kappa * fe_values.shape_grad(i, q) * fe_values.shape_grad(j, q) );
                }
            }
        }

        // Local right-hand-side vector assembly
        cell_rhs = 0;
        for (unsigned int q = 0; q < n_q_points; ++q) {
            double pot_grad_squared = potential_gradients[q].norm_square();
            double temperature = prev_temperatures[q];
            double rho = this->pq->evaluate_resistivity(temperature);
            double ax_factor = this->axisymmetry_factor(fe_values, q);

            for (unsigned int i = 0; i < dofs_per_cell; ++i) {
                cell_rhs(i) += fe_values.JxW(q) * ax_factor * fe_values.shape_value(i, q)
                        * (gamma * temperature + rho * pot_grad_squared);
            }
        }

        // Update global matrix and right-hand-side vector
        cell->get_dof_indices(local_dof_indices);
        for (unsigned int i = 0; i < dofs_per_cell; ++i) {
            this->system_rhs(local_dof_indices[i]) += cell_rhs(i);

            for (unsigned int j = 0; j < dofs_per_cell; ++j)
                this->system_matrix.add(local_dof_indices[i], local_dof_indices[j], cell_matrix(i, j));
        }
    }
}

template<int dim>
void HeatSolver<dim>::assemble_local_cell(const typename DoFHandler<dim>::active_cell_iterator &cell,
        ScratchData &scratch_data, CopyData &copy_data) const
{
    const unsigned int n_dofs = copy_data.n_dofs;
    const unsigned int n_q_points = copy_data.n_q_points;

    const double gamma = cu_rho_cp * one_over_delta_time;

    // The other solution values in the cell quadrature points
    vector<Tensor<1, dim>> potential_gradients(n_q_points);
    vector<double> prev_temperatures(n_q_points);

    scratch_data.fe_values.reinit(cell);
    scratch_data.fe_values.get_function_values(this->solution, prev_temperatures);
    scratch_data.fe_values.get_function_gradients(current_solver->solution, potential_gradients);

    // Local matrix assembly
    copy_data.cell_matrix = 0;
    for (unsigned int q = 0; q < n_q_points; ++q) {
        double temperature = prev_temperatures[q];
        double kappa = this->pq->kappa(temperature);
        double ax_factor = this->axisymmetry_factor(scratch_data.fe_values, q);

        for (unsigned int i = 0; i < n_dofs; ++i) {
            for (unsigned int j = 0; j < n_dofs; ++j) {
                copy_data.cell_matrix(i, j) += scratch_data.fe_values.JxW(q) * (
                        gamma * scratch_data.fe_values.shape_value(i, q) * scratch_data.fe_values.shape_value(j, q) // Mass matrix
                        + kappa * scratch_data.fe_values.shape_grad(i, q) * scratch_data.fe_values.shape_grad(j, q) ) * ax_factor;
            }
        }
    }

    // Local right-hand-side vector assembly
    copy_data.cell_rhs = 0;
    for (unsigned int q = 0; q < n_q_points; ++q) {
        double pot_grad_squared = potential_gradients[q].norm_square();
        double temperature = prev_temperatures[q];
        double rho = this->pq->evaluate_resistivity(temperature);
        double ax_factor = this->axisymmetry_factor(scratch_data.fe_values, q);

        for (unsigned int i = 0; i < n_dofs; ++i) {
            copy_data.cell_rhs(i) += scratch_data.fe_values.JxW(q) * scratch_data.fe_values.shape_value(i, q)
                    * (gamma * temperature + rho * pot_grad_squared) * ax_factor;
        }
    }

    // Obtain dof indices for updating global matrix and right-hand-side vector
    cell->get_dof_indices(copy_data.dof_indices);
}

/* ==================================================================
 *  ========================= CurrentSolver ========================
 * ================================================================== */

template<int dim>
CurrentSolver<dim>::CurrentSolver(Triangulation<dim> *tria,
        const HeatSolver<dim> *hs, const HeatData *pq,
        const Config::Heating *conf, const vector<double> *bcs) :
        BulkSolver<dim>(tria, pq, conf, bcs), heat_solver(hs)
        {}

template<int dim>
void CurrentSolver<dim>::write_vtk(ofstream& out) const {
    CurrentPostProcessor<dim> post_processor; // needs to be before data_out
    DataOut<dim> data_out;

    data_out.attach_dof_handler(this->dof_handler);
    data_out.add_data_vector(this->solution, "potential");
    data_out.add_data_vector(this->solution, post_processor);

    data_out.build_patches();
    data_out.write_vtk(out);
}

template<int dim>
void CurrentSolver<dim>::assemble(const bool full_run) {

    require(heat_solver, "NULL heat solver can't be used!");

    if (full_run) this->system_matrix = 0;
    this->system_rhs = 0;

    if (full_run)
        this->assemble_parallel();
    else
        this->restore_system_matrix();

    this->assemble_rhs(BoundaryID::copper_surface);

    this->append_dirichlet(BoundaryID::copper_bottom, this->dirichlet_bc_value);

    this->apply_dirichlet();
}


/* ==================================================================
 *  ======================= CurrentHeatSolver ======================
 * ================================================================== */

template<int dim>
CurrentHeatSolver<dim>::CurrentHeatSolver(const HeatData *heat_data,
        const Config::Heating *config,
        const vector<double> *surface_heat,
        const vector<double> *current_densities) :
        DealSolver<dim>(),
        heat(&this->triangulation, &current, heat_data, config, surface_heat),
        current(&this->triangulation, &heat, heat_data, config, current_densities),
        pq(heat_data), conf(config)
{}


template<int dim>
void CurrentHeatSolver<dim>::setup(const double temperature, bool axisymmetry) {
    heat.dirichlet_bc_value = temperature;
    current.setup_system(axisymmetry);
    heat.setup_system(axisymmetry);
}

template<int dim>
int CurrentHeatSolver<dim>::run(double dt, bool full_run) {
    int ncg;
    double t0;
    bool fail;

    start_msg(t0, "Calculating current density");
    current.assemble(full_run);
    ncg = current.solve();
    end_msg(t0);

    fail = current.check_limits(0.0, 100.0);
    write_verbose_msg("#CG=" + d2s(ncg) + ", J_min=" + d2s(current.stat.sol_min)
            + ", J_max=" + d2s(current.stat.sol_max));
    check_return(ncg < 0, "Current solver did not converge within nominal #CG steps!");
    check_return(fail, "Current density is out of limits!");

    start_msg(t0, "Calculating temperature distribution");
    heat.assemble(dt * 1.e-15); // caution!! ch_solver internal time in sec
    ncg = heat.solve();
    if (conf->T_clamp)
        heat.clamp_sol(conf->T_min,  conf->T_max);
    end_msg(t0);

    fail = heat.check_limits(conf->T_min,  conf->T_max);
    write_verbose_msg("#CG=" + d2s(ncg) + ", T_min=" + d2s(heat.stat.sol_min)
            + " K, T_max=" + d2s(heat.stat.sol_max) + " K");
    check_return(ncg < 0, "Heat solver did not converge within nominal #CG steps!");
    check_return(fail, "Temperature is out of limits!");

    this->write(conf->heating_file);
    return 0;
}

template<int dim>
void CurrentHeatSolver<dim>::export_temp_rho(vector<double> &temp, vector<Tensor<1,dim>> &rho) const {
    heat.export_solution(temp);        // extract temperatures
    current.export_solution_grad(rho); // extract current densities
}

template<int dim>
void CurrentHeatSolver<dim>::mark_mesh() {
    this->mark_boundary(BoundaryID::copper_surface, BoundaryID::copper_bottom,
            BoundaryID::copper_sides, BoundaryID::copper_surface);
}

template<int dim>
void CurrentHeatSolver<dim>::write_restart(ofstream &out) const {
    out << "$CHSolver" << endl;
    write_xyz(out);
    out << "$EndCHSolver" << endl;
}

template<int dim>
void CurrentHeatSolver<dim>::read(const string &file_name) {
    string ftype = get_file_type(file_name);
    require(ftype == "restart", "Unimplemented file type: " + ftype);

    ifstream in(file_name);
    require(in.is_open(), "Can't open a file " + file_name);

    const int n_dofs = heat.size();
    const int n_verts = heat.tria->n_used_vertices();
    vector<double> temperature(n_dofs);

    // generate dof index -> vertex index mapping
    vector<int> dof2vertex(n_dofs);
    for (int i = 0; i < n_verts; ++i)
        dof2vertex[heat.vertex2dof[i]] = i;

    const string start_str = "$CHSolver";
    string str;

    // read data
    while (in >> str) {
        if (str == start_str) { // look for start tag
            in >> str; // n_dofs
            getline(in, str);
            string header;
            getline(in, header); // Ovito header

            for (int i = 0; i < n_dofs && !in.eof(); ++i) {
                int id;
                double total_heat, nottingham_heat, joule_heat, volume, temp,
                        potential, current_density;
                in >> id;
                for (int i = 0; i < dim; ++i) {
                    double pos;
                    in >> pos;
                }
                for (int i = 0; i < dim; ++i) {
                    double force;
                    in >> force;
                }
                in >> total_heat >> nottingham_heat >> joule_heat >> volume
                        >> temp >> potential >> current_density;
                getline(in, str);
                temperature[dof2vertex[i]] = temp;
            }
        }
    }

    heat.import_solution(&temperature);
}

template<int dim>
void CurrentHeatSolver<dim>::write_xyz(ofstream& out) const {
    // write the start of xyz header
    FileWriter::write_xyz(out);

    // write Ovito header
    out << "properties=id:I:1:pos:R:" << dim << ":force:R:" << dim <<
            ":total_heat:R:1:surface_heat:R:1"
            ":joule_heat:R:1:volume:R:1:temperature:R:1:potential:R:1:current_density:R:1\n";

    // extract coordinates of dofs
    vector<Point<dim>> support_points;
    heat.export_dofs(support_points);

    vector<Tensor<1,dim>> cur_dens;
    current.export_solution_grad(cur_dens); // extract current densities

    const int n_dofs = support_points.size();
    const int n_verts = heat.tria->n_used_vertices();

    // generate dof index -> vertex index mapping
    vector<int> dof2vertex(n_dofs);
    for (int i = 0; i < n_verts; ++i)
        dof2vertex[heat.vertex2dof[i]] = i;

    // write data
    for (int i = 0; i < n_dofs; ++i) {
        out << dof2vertex[i] << " " << support_points[i] << " " << cur_dens[dof2vertex[i]]
                << " " << heat.total_heat(i) << " " << heat.total_heat(i) - heat.joule_heat(i)
                << " " << heat.joule_heat(i) << " " << heat.dof_volume[i] << " " << heat.solution(i)
                << " " << current.solution(i) / pq->sigma(heat.solution(i)) << " " << cur_dens[dof2vertex[i]].norm() << "\n";
    }
}

template<int dim>
void CurrentHeatSolver<dim>::write_vtk(ofstream& out) const {
    CurrentPostProcessor<dim> current_post_processor; // needs to be before data_out
    DataOut<dim> data_out;

    data_out.attach_dof_handler(current.dof_handler);
    data_out.add_data_vector(current.solution, "potential");
    data_out.add_data_vector(current.solution, current_post_processor);

    SigmaPostProcessor<dim> sigma_post_processor(heat.pq);

    const int n_dofs = heat.size();
    Vector<double> surface_heat(n_dofs);

    for (int i = 0; i < n_dofs; ++i)
        surface_heat[i] = heat.total_heat(i) - heat.joule_heat(i);

    data_out.add_data_vector(heat.solution, "temperature");
    data_out.add_data_vector(heat.solution, sigma_post_processor);
    data_out.add_data_vector(heat.total_heat, "total_heat");
    data_out.add_data_vector(surface_heat, "surface_heat");
    data_out.add_data_vector(heat.joule_heat, "joule_heat");

    data_out.build_patches();
    data_out.write_vtk(out);
}

/* ==================================================================
 * Declare above classes with desired dimensions
 * ================================================================== */

template class BulkSolver<3> ;
template class HeatSolver<3> ;
template class CurrentSolver<3> ;
template class CurrentHeatSolver<3> ;

template class BulkSolver<2> ;
template class HeatSolver<2> ;
template class CurrentSolver<2> ;
template class CurrentHeatSolver<2> ;

} // namespace femocs
