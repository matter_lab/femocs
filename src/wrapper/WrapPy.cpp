// Femocs python interface

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>
#include <numpy/arrayobject.h>
#include <math.h>

#include "Femocs.h"

using namespace std;
using namespace femocs;


/*
 * A tutorial about how to create interfaces for various input-output combinations:
 * https://scipy.github.io/old-wiki/pages/Cookbook/C_Extensions/NumPy_arrays.html
 *
 * An official documentation:
 * https://docs.python.org/3/extending/extending.html
 *
 * Parsing arguments and building values:
 * https://docs.python.org/3/c-api/arg.html
 *
 * Instructions how to do it with cython:
 * https://groups.google.com/g/cython-users/c/sqeY7GO3U7k
 * https://github.com/cython/cython/wiki/tutorials-NumpyPointerToC
 */

/* =====================================================================
 *                         Macros & Femocs struct
 * ===================================================================== */

typedef struct {
    PyObject_HEAD // no semicolon
    Femocs *ptr;
} FEMOCS;

bool femocs_not_valid(FEMOCS *self) {
    if (self == nullptr || self->ptr == nullptr)
        return true;
    return false;
}

bool vector_not_valid(PyArrayObject *vec, int vec_type=NPY_DOUBLE) {
    if (vec == nullptr || vec->descr->type_num != vec_type)
        return true;
    return false;
}

/* =====================================================================
 *                  Implementations of member methods
 * ===================================================================== */

static PyObject* Femocs_run(FEMOCS *self, PyObject *args) {
    int timestep;
    double time;

    // "i" means an "int" type argument
    // "d" means a "double" type argument
    // "s" means a "char *" type argument
    if (!PyArg_ParseTuple(args, "id", &timestep, &time))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    int res = self->ptr->run(timestep, time);
    return Py_BuildValue("i", res);
}

static PyObject* Femocs_import_file(FEMOCS *self, PyObject *args) {
    const char* file_name;
    if (!PyArg_ParseTuple(args, "s", &file_name))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    int res = self->ptr->import_file(string(file_name));
    return Py_BuildValue("i", res);
}

static PyObject* Femocs_import_doubles(FEMOCS *self, PyObject *args) {
    PyArrayObject *py_data = nullptr;
    const double *data;
    const char* data_type;
    int retval;

    // "|" makes the arguments on the right optional
    // "0" is Python object (Numpy array), "!" invokes type check
    if (!PyArg_ParseTuple(args, "s|O!", &data_type, &PyArray_Type, &py_data))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    // not providing array as an argument is considered equal to providing NULL array
    if (py_data == nullptr) {
        retval = self->ptr->import_data((double*) NULL, 0, string(data_type));
        return Py_BuildValue("i", retval);
    }

    if (vector_not_valid(py_data))
        return Py_BuildValue("i", -3);

    int n_data = py_data->dimensions[0];
    // multidimensional array is flattened into a linear one
    // i.e py_data[0][0] == data[0], py_data[0][1] == data[1], py_data[0][2] == data[2]
    //     py_data[1][0] == data[3], py_data[1][1] == data[4] etc
    data = (double*) py_data->data;

    // array of zero length is considered equal to NULL array
    if (n_data == 0)
        retval = self->ptr->import_data((double*) NULL, 0, string(data_type));
    else
        retval = self->ptr->import_data(data, n_data, string(data_type));
    return Py_BuildValue("i", retval);
}

static PyObject* Femocs_import_ints(FEMOCS *self, PyObject *args) {
    PyArrayObject *py_data = nullptr;
    const int *data;
    const char* data_type;
    int retval;

    if (!PyArg_ParseTuple(args, "s|O!", &data_type, &PyArray_Type, &py_data))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    // not providing array as an argument is considered equal to providing NULL array
    if (py_data == nullptr) {
        retval = self->ptr->import_data((int*) NULL, 0, string(data_type));
        return Py_BuildValue("i", retval);
    }

    if (vector_not_valid(py_data, NPY_INT))
        return Py_BuildValue("i", -3);

    int n_data = py_data->dimensions[0];
    // multidimensional array is flattened into a linear one
    // i.e py_data[0][0] == data[0], py_data[0][1] == data[1], py_data[0][2] == data[2]
    //     py_data[1][0] == data[3], py_data[1][1] == data[4] etc
    data = (int*) py_data->data;

    // array of zero length is considered equal to NULL array
    if (n_data == 0)
        retval = self->ptr->import_data((int*) NULL, 0, string(data_type));
    else
        retval = self->ptr->import_data(data, n_data, string(data_type));
    return Py_BuildValue("i", retval);
}

static PyObject* Femocs_export_doubles(FEMOCS *self, PyObject *args) {
    PyArrayObject *py_data;
    double *data;
    const char* data_type;

    if (!PyArg_ParseTuple(args, "O!s", &PyArray_Type, &py_data, &data_type))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    if (vector_not_valid(py_data))
        return Py_BuildValue("i", -3);

    int n_atoms = py_data->dimensions[0];
    data = (double *) py_data->data;

    int res = self->ptr->export_data(data, n_atoms, string(data_type));
    return Py_BuildValue("i", res);
}

static PyObject* Femocs_export_ints(FEMOCS *self, PyObject *args) {
    PyArrayObject *py_data;
    int *data;
    const char* data_type;

    if (!PyArg_ParseTuple(args, "O!s", &PyArray_Type, &py_data, &data_type))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    if (vector_not_valid(py_data, NPY_INT))
        return Py_BuildValue("i", -3);

    int n_atoms = py_data->dimensions[0];
    data = (int *) py_data->data;

    int res = self->ptr->export_data(data, n_atoms, string(data_type));
    return Py_BuildValue("i", res);
}

static PyObject* Femocs_export_double(FEMOCS *self, PyObject *args) {
    const char* data_type;

    if (!PyArg_ParseTuple(args, "s", &data_type))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    const double *data = NULL;
    int n_data = self->ptr->export_data(&data, string(data_type));

    npy_intp dims = n_data;
    PyArrayObject *py_data = (PyArrayObject *) PyArray_SimpleNewFromData(1, &dims, NPY_DOUBLE, (void *)data);
    return PyArray_Return(py_data);
}

static PyObject* Femocs_export_int(FEMOCS *self, PyObject *args) {
    const char* data_type;

    if (!PyArg_ParseTuple(args, "s", &data_type))
        return Py_BuildValue("i", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    const int *data = NULL;
    int n_data = self->ptr->export_data(&data, string(data_type));

    npy_intp dims = n_data;
    PyArrayObject *py_data = (PyArrayObject *) PyArray_SimpleNewFromData(1, &dims, NPY_INT, (void *)data);
    return PyArray_Return(py_data);
}

static PyObject* Femocs_parse_int(FEMOCS *self, PyObject *args) {
    const char* command;
    int retval = 0;

    if (!PyArg_ParseTuple(args, "s", &command))
        return Py_BuildValue("ii", retval, -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("ii", retval, -2);

    int error = self->ptr->parse_command(string(command), &retval);
    return Py_BuildValue("ii", retval, error);
}

static PyObject* Femocs_parse_double(FEMOCS *self, PyObject *args) {
    const char* command;
    double retval = 0.0;

    if (!PyArg_ParseTuple(args, "s", &command))
        return Py_BuildValue("di", retval, -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("di", retval, -2);

    int error = self->ptr->parse_command(string(command), &retval);
    return Py_BuildValue("di", retval, error);
}

static PyObject* Femocs_parse_string(FEMOCS *self, PyObject *args) {
    const char* command;
    string retval = "";

    if (!PyArg_ParseTuple(args, "s", &command))
        return Py_BuildValue("si", "", -1);

    if (femocs_not_valid(self))
        return Py_BuildValue("si", "", -2);

    int error = self->ptr->parse_command(string(command), retval);
    return Py_BuildValue("si", retval.c_str(), error);
}

static PyObject* Femocs_version(FEMOCS *self, PyObject *Py_UNUSED(ignored)) {
    if (femocs_not_valid(self))
        return Py_BuildValue("i", -2);

    string ver = self->ptr->version();
    return Py_BuildValue("s", ver.c_str());
}

/* =====================================================================
 *                      Definitions of methods & members
 * ===================================================================== */

static PyMethodDef Femocs_methods[] = {
    {"run", (PyCFunction) Femocs_run, METH_VARARGS, "Initiate main processing"},

    {"import_file", (PyCFunction) Femocs_import_file, METH_VARARGS, "Read data from a file"},
    {"import_doubles", (PyCFunction) Femocs_import_doubles, METH_VARARGS, "Import array of double data"},
    {"import_ints", (PyCFunction) Femocs_import_ints, METH_VARARGS, "Import array of integer data"},

    {"export_doubles", (PyCFunction) Femocs_export_doubles, METH_VARARGS, "Export array of double data"},
    {"export_ints", (PyCFunction) Femocs_export_ints, METH_VARARGS, "Export array of integer data"},
    {"export_double", (PyCFunction) Femocs_export_double, METH_VARARGS, "Export pointer to double data"},
    {"export_int", (PyCFunction) Femocs_export_int, METH_VARARGS, "Export pointer to integer data"},

    {"parse_int", (PyCFunction) Femocs_parse_int, METH_VARARGS, "Parse integer data from configuration file"},
    {"parse_double", (PyCFunction) Femocs_parse_double, METH_VARARGS, "Parse double data from configuration file"},
    {"parse_string", (PyCFunction) Femocs_parse_string, METH_VARARGS, "Parse string data from configuration file"},

    {"version", (PyCFunction) Femocs_version, METH_NOARGS, "Obtain the version of Femocs"},

    {nullptr, nullptr, 0, nullptr}
};

static PyMemberDef Femocs_members[] = {
    {nullptr}
};

/* =====================================================================
 *                             Initializators
 * ===================================================================== */

static void Femocs_dealloc(FEMOCS *self) {
    Py_TYPE(self)->tp_free((PyObject *) self);
    if (self->ptr != nullptr) {
        Femocs *ptr = self->ptr;
        self->ptr = nullptr;
        delete ptr;
    }
}

static PyObject *Femocs_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {
    FEMOCS *self = (FEMOCS *) type->tp_alloc(type, 0);

    if (self != nullptr)
        self->ptr = nullptr;

    return (PyObject *) self;
}

static int Femocs_init(FEMOCS *self, PyObject *args, PyObject *kwds) {
    const char *kwlist[] = {"path_to_conf", nullptr};
    const char* path;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", const_cast<char **>(kwlist), &path))
        return -1;

    self->ptr = new Femocs(string(path));

    return 0;
}

static PyTypeObject FemocsType = {
    PyVarObject_HEAD_INIT(nullptr, 0)
    .tp_name = "femocs.Femocs",
    .tp_basicsize = sizeof(FEMOCS),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) Femocs_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   // Can subclass Femocs
    .tp_doc = "Femocs objects",
    .tp_methods = Femocs_methods,
    .tp_members = Femocs_members,
    .tp_init = (initproc) Femocs_init,
    .tp_new = Femocs_new,
};

static PyModuleDef FemocsModule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "femocs",
    .m_doc = "Finite Elements on Crystal Surfaces",
    .m_size = -1,
};

PyMODINIT_FUNC PyInit_femocs(void) {
    Py_Initialize();
    import_array();

    int res = PyType_Ready(&FemocsType);
    if (res < 0)
        return nullptr;

    PyObject *m = PyModule_Create(&FemocsModule);
    if (m == nullptr)
        return nullptr;

    Py_INCREF(&FemocsType);
    if (PyModule_AddObject(m, "Femocs", (PyObject *) &FemocsType) < 0) {
        Py_DECREF(&FemocsType);
        Py_DECREF(m);
        return nullptr;
    }

    return m;
}
