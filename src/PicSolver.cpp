/*
 * PicGeneric.cpp
 *
 *  Created on: 19 Nov 2019
 *      Author: kyritsak
 */

#include "PicSolver.h"
#include "Config.h"
#include "Globals.h"
#include "Emission.h"
#include "Interpolator.h"
#include "SolutionReader.h"
#include "CircuitModel.h"

#include <deal.II/base/work_stream.h>
#include <deal.II/numerics/data_out.h>

namespace femocs {

template<int dim>
PicSolver<dim>::PicSolver(Emission<dim> *e, const Config& config, unsigned int seed) :
            FieldSolver<dim>(config, seed),
            pic_conf(&config.pic),
            pic_data(config.pic),
            electron_table(config.pic.weight_el, this),
            electrons(config.pic.weight_el, this),
            neutrals(config.pic.vapor_mass * CONSTANTS.amu, config.pic.weight_neutral, this),
            ions(pic_conf->vapor_mass * CONSTANTS.amu, pic_conf->weight_ion, &pic_data, this),
            emission(e),
            collisions(this, neutrals, electrons, ions, pic_data, config.pic),
            timestep(config.pic.dt_max),
            cathode_deposited_charge(0), atoms_lost(0),
            system_energy(0)
{
    // Specify reflective properties for superparticles
    int reflection = 0;
    if (pic_conf->reflective == "left")
        reflection = 1;
    else if (pic_conf->reflective == "sides")
        reflection = 2;
    else if (pic_conf->reflective == "all")
        reflection = 3;

    electrons.set_reflection(reflection);
    neutrals.set_reflection(reflection);
    for (auto &ion : ions)
        ion.set_reflection(reflection);
}

template<int dim>
double PicSolver<dim>::total_charge() const {
    // TODO what about ions?
    return this->system_rhs.mean_value() * this->system_rhs.size() * electrons.q_over_eps0;
}

template<int dim>
double PicSolver<dim>::total_energy() const {
    double energy = electrons.calc_energy();
    energy += neutrals.calc_energy();
    energy += ions.calc_energy();
    return energy;
}

template<int dim>
Vec3 PicSolver<dim>::total_momentum() const {
    Vec3 momentum = electrons.calc_momentum();
    momentum += neutrals.calc_momentum();
    momentum += ions.calc_momentum();
    return momentum;
}

template<int dim>
double PicSolver<dim>::measure_energy() {
    double prev_sys_E = system_energy;
    system_energy = total_energy();
    return system_energy - prev_sys_E;
}

template<int dim>
double PicSolver<dim>::measure_momentum() {
    Vec3 prev_sys_p = system_momentum;
    system_momentum = total_momentum();
    Vec3 delta_p = system_momentum - prev_sys_p;
    return delta_p.norm();
}

template<int dim>
void PicSolver<dim>::read(const string &file_name) {
    electrons.read(file_name);
    neutrals.read(file_name);
    for (auto &ion : ions)
        ion.read(file_name);
}

template<int dim>
void PicSolver<dim>::write_restart(ofstream &out) const {
    electrons.write_restart(out);
    neutrals.write_restart(out);
    for (const auto &ion : ions)
        ion.write_restart(out);
}

template<int dim>
int PicSolver<dim>::run(double advance_time, bool first_run) {
    // Update voltage and current in the gap
    double Igap = electrons.current;
    for (auto &ion : ions)
        Igap += ion.current;
    this->circuit->timestep(advance_time, Igap);

    double t0;
    int n_steps = ceil(advance_time / pic_conf->dt_max);
    timestep = advance_time / n_steps;

    start_msg(t0, "=== Running PIC for delta time = " + d2s(n_steps)
            + "*" + d2s(timestep) + " = " + d2s(advance_time) + " fs\n");

    for (int i = 0; i < n_steps; ++i) {
        if (run_step(timestep, i <= 1))
            return 1;
    }
    end_msg(t0);

    if (this->conf->heating.bombardment_heating) {
        n_bombarded += ions.bombard_faces.size();

        emission->calc_bombardment(ions.bombard_faces, ions.bombard_energies);
    }
    ions.bombard_faces.clear();
    ions.bombard_energies.clear();

    emission->calc_total_heat();

    emission->write();

    return 0;
}

template<int dim>
int PicSolver<dim>::run_step(double delta_t, bool full_run) {
    if (delta_t)
        timestep = delta_t;
    else
        timestep = pic_conf->dt_max;

    if (full_run) {
        electrons.regroup_all();
        neutrals.regroup_all();
        for (auto &ion : ions)
            ion.regroup_all();
    }

    int searches = electrons.update_positions(delta_t, false);
    int n_lost_electrons = electrons.clear_lost();
    neutrals.update_positions(delta_t);
    for (auto &ion : ions)
        ion.update_positions(delta_t);

    this->assemble_laplace(full_run);
    this->assemble_space_charge(electrons);
    this->assemble_space_charge(neutrals);
    for (const auto &ion : ions)
        this->assemble_space_charge(ion);
    this->assemble_finalize();

    this->solve(full_run);
    probe_field(neutrals);
    set_magnetic_field(neutrals);
    probe_field(electrons);
    probe_laplace_field(electrons);
    set_magnetic_field(electrons);
    for (auto &ion : ions) {
        probe_field(ion);
        probe_laplace_field(ion);
        set_magnetic_field(ion);
    }

    electrons.update_velocities(delta_t);
    for (auto &ion : ions)
        ion.update_velocities(delta_t);

    cathode_deposited_charge += ions.cathode_deposited_charge;
    ions.cathode_deposited_charge = 0;

    // Write files before injections to get corresponding electrons and P solver
    electrons.write(pic_conf->electrons_file);
    neutrals.write(pic_conf->neutrals_file);
    ions.write(pic_conf->ions_file);

    double ne_injected;
    if (pic_conf->injection_table == "") {
        emission->extract_fields();
        emission->calc_emission();
        ne_injected = fabs(electrons.inject(*emission, timestep, *pic_conf));
    } else {
        if (full_run) electron_table.read_table(pic_conf->injection_table);
        ne_injected = electrons.inject_table(electron_table, timestep, *pic_conf);
    }

    emission->calc_evaporation(pic_conf->vapor_mass);

    if (this->conf->heating.blackbody) {
        emission->calc_blackbody();
    }

    double nn_injected = fabs(neutrals.inject(*emission, timestep, *pic_conf));

    n_evaporated += nn_injected;
    if (this->write_time())
        write_verbose_msg("evaporated_tot=" + to_string(n_evaporated));

    if (pic_conf->sputtering) {
        double sput_injected = fabs(neutrals.inject_sput(*emission, timestep, *pic_conf, ions));
        nn_injected += sput_injected;
        n_sputtered += sput_injected;

        if (this->write_time())
            write_verbose_msg("sputtered_tot=" + to_string(n_sputtered));

        ions.sput_faces.clear();
        ions.sput_energies.clear();
        ions.sput_yields.clear();
    }

    if (this->conf->heating.bombardment_heating) {
        if (this->write_time())
            write_verbose_msg("bombarded_tot=" + to_string(n_bombarded));
    }

    collisions.run(timestep);
    if (pic_conf->field_ionization) {
        double fi_injected = field_ionization(timestep);
        n_fionized += fi_injected;

        if (this->write_time())
            write_verbose_msg("field_ionized=" + to_string(n_fionized));
    }

    if (this->write_time()) {
        double n_electrons = electrons.get_num();
        double n_neutrals = neutrals.get_num();
        double electron_charge = electrons.charge_tot();
        double ion_charge = 0.;
        double n_ions = 0.;
        int nsp_ions = 0;
        for (const auto &ion : ions) {
            ion_charge += ion.charge_tot();
            n_ions += ion.get_num();
            nsp_ions += ion.size();
        }

        double avg_charge = n_electrons + n_ions == 0 ? 0. : (double) (electron_charge + ion_charge) / (n_electrons + n_ions);
        write_verbose_msg("avg_charge=" + to_string(avg_charge));

        printf("  t=%g, Tmax=%.1f, Jmax=%.3e, Fmax=%.4f, Vgap=%.4f, Itot=%.4f, Igap=%.4f, "
               "#e_inj|e_tot=%.1f|%.1f(%d), #n_tot|i_tot=%.1f(%d)|%.1f(%d)\n",
               GLOBALS.TIME, emission->global_data.Tmax,
               emission->global_data.Jmax, emission->global_data.Fmax,
               this->circuit->Vgap(), emission->global_data.I_tot, this->circuit->Igap(),
               ne_injected, n_electrons, electrons.size(), n_neutrals, neutrals.size(), n_ions, nsp_ions);
    }

    calc_number_densities();

    this->write(pic_conf->pic_file);
    emission->write();

    GLOBALS.TIME += timestep;
    GLOBALS.TIMESTEP++;

    return 0;
}

template<int dim>
int PicSolver<dim>::step_electrons(double delta_t, bool full_run,
        FieldReader *reader, Interpolator *interpolator)
{
    electrons.set_interpolator(interpolator);
    if (full_run) electrons.regroup_all();

    int n_searches = electrons.update_positions(delta_t, false);
    int n_lost = electrons.clear_lost();

    this->assemble_laplace(full_run);
    this->assemble_space_charge(electrons);
    this->assemble_finalize();

    this->solve(full_run);
    bool fail = this->check_limits(this->conf->field.V_min, this->conf->field.V_max);
    check_return(fail, "Potential is out of limits; " + d2s(this->stat));

    probe_field(electrons);
    emission->extract_fields();

    if (reader) {
        check_return(reader->check_limits(emission->global_data.Emax, false),
                "Field enhancement is out of limits; M/A=" + d2s(reader->get_beta()));
    }
    if (interpolator)
        interpolator->extract_solution(*this, this->conf->run.field_smoother);

    electrons.update_velocities(delta_t);

    // Write files before injections to get corresponding electrons and Poisson solver
    electrons.write(pic_conf->electrons_file);

    int error_code = emission->calc_emission();
    check_return(error_code == -10, "Current density is out of limits, Jmax=" + d2s(emission->global_data.Jmax));
    check_return(error_code, "Emission calculation failed with error code " + d2s(error_code));

    int n_injected = electrons.inject(*emission, delta_t, *pic_conf);
    check_return(n_injected < 0, "Too many injected electrons: " + d2s(-n_injected));

    collisions.run(delta_t);

    if (MODES.VERBOSE) {
        // TODO add beta
        printf("  t=%.2f, Vmin=%.1f V, Jmax=%.0e, Fmax=%.3f V/A, "
                "Itot=%.3e A, CSPP=%.2g, #inj|del|tot=%d|%d|%d\n",
                GLOBALS.TIME, this->stat.sol_min, emission->global_data.Jmax,
                emission->global_data.Fmax, emission->global_data.I_tot,
                (double) n_searches / electrons.size(),
                n_injected, n_lost, electrons.size());
    }

    calc_number_densities();

    this->write(pic_conf->pic_file);
    emission->write();

    GLOBALS.TIME += delta_t;
    return 0;
}

template<int dim>
double PicSolver<dim>::run_steady_iteration(double theta, bool first) {
    this->assemble_laplace(first);
    double dt = timestep;

    const double dt_increase_factor = 1.1;
    const double mean_searches = 1.2;
    const double max_searches = 1.6;

    if (first) electrons.regroup_all();

    deque<double> s_per_part;
    for(int i = 0; electrons.size(); i++){
        electrons.update_velocities(dt);
        int searches = electrons.update_positions(dt);
        if (electrons.size() == 0) break;

        s_per_part.push_back((double) searches / electrons.size());
        if (s_per_part.size() > 10) s_per_part.pop_front();
        double s_per_part_ma = 0.;
        for (double x : s_per_part) s_per_part_ma += x;
        s_per_part_ma /= s_per_part.size();


        if (s_per_part_ma < mean_searches && s_per_part.size() >= 10){
            dt *= dt_increase_factor;
            for (double &x : electrons.weights) x *= dt_increase_factor;
        }
        if (s_per_part_ma > max_searches){
            dt /= dt_increase_factor;
            for (double &x : electrons.weights) x /= dt_increase_factor;
            if (i < 20)
                cout << "WARNING: Mean searches too high. You might need to reduce PIC timestep" << endl;
        }

        assemble_space_charge(electrons);
        probe_field(electrons);
        GLOBALS.TIME += dt;
    }

    double error = this->update_rhs(theta);
    this->assemble_finalize();

    this->solve(first);

    calc_number_densities();

    this->write(pic_conf->pic_file);

    emission->extract_fields();
    emission->calc_emission();

    // TODO shouldn't here be dt instead of timestep ?
    int n_injected = electrons.inject_paths(*emission, timestep); //inject for next step
    if (MODES.VERBOSE) {
        printf("#iter = %d, drho_R=%.3e, Jmax=%.3e, Fmax=%.3g V/A, Itot=%.3e A, Qtot=%.3e e, #inj=%d\n",
                GLOBALS.TIMESTEP, error, emission->global_data.Jmax,
                emission->global_data.Fmax, emission->global_data.I_tot,
                this->total_charge(), n_injected);
    }

    return error;
}

template<int dim>
int PicSolver<dim>::field_ionization(double delta_t, int level) {
    if (level >= pic_data.ionization_levels)
        return 0;

    double total_ionized = 0.;
    auto &leveled_ions = ions[level - 1];
    const unsigned neutrals_n = neutrals.size();

    for (int i = 0; i < neutrals_n; ++i) {
        const Tensor<1,dim> &field = neutrals.fields[i];

        double xi = this->pic_data.sum_ionization_energy(level);
        double n = 3.69 * (double) level / sqrt(xi);
        double E = 10.0 * field.norm();

        if (E == 0.)
            continue;

        double xi32 = sqrt(xi * xi * xi);

        // barrier suppression limit
        if (E > 1.5 * xi32)
            continue;

        double p = 1.52 * (pow(4., n) * xi) / (n * tgamma(2. * n)) *
                pow(20.5 * xi32 / E, 2. * n - 1);
        p *= exp(-6.83 * xi32 / E) * delta_t;

        if (collisions.split_particle(i, neutrals, p)) {
            Particle<dim> electron = neutrals[i];
            Particle<dim> ion = neutrals[i];

            electron.vel += field * (this->rnd() * delta_t * electrons.q_over_m);
            ion.vel += field * (this->rnd() * delta_t * leveled_ions.q_over_m);

            // The weight of the electron & ion must be the same as for neutral,
            // as one neutral can produce only one electron-ion pair

            electrons.inject(electron);
            leveled_ions.inject(ion);

            neutrals.weights[i] = 0.; // remove neutral

            ++total_ionized;
        }
    }
    neutrals.clear_lost();

    return total_ionized;
}

template<int dim>
void PicSolver<dim>::assemble_space_charge_serial(const ParticleSet<dim> &particles) {
    if (particles.q_over_eps0 == 0.0)
        return;

    static constexpr int n_dofs = GeometryInfo<dim>::vertices_per_cell;
    vector<unsigned int> dof_indices(n_dofs);

    for (const Cell &cell : particles.filled_cells) {
        cell->get_dof_indices(dof_indices);

        // loop over particles that are in the cell
        const vector<int> &particle_indices = particles.parts_at(cell);
        for (int index : particle_indices) {
            // get the shape functions in the location of the particle with respect to the cell
            const Point<dim> &upoint = particles.unit_positions[index];
            vector<double> shape_funs = this->utilities.shape_funs(upoint, cell);

            // loop over DOFs of the cell and add the particle's charge to the system rhs
            double charge_factor = particles.q_over_eps0 * particles.weights[index];
            for (int i = 0; i < n_dofs; ++i)
                this->system_rhs(dof_indices[i]) += shape_funs[i] * charge_factor;
        }
    }
}

template<int dim>
void PicSolver<dim>::calc_number_density(const ParticleSet<dim> &particles, vector<double> &density) {
    if (!this->write_time())
        return;
    static constexpr int n_dofs = GeometryInfo<dim>::vertices_per_cell;
    vector<unsigned int> dof_indices(n_dofs);
    for (const Cell &cell : particles.filled_cells) {
        cell->get_dof_indices(dof_indices);
        // loop over particles that are in the cell
        const vector<int> &particle_indices = particles.parts_at(cell);
        for (int index : particle_indices) {
            // get the shape functions in the location of the particle with respect to the cell
            const Point<dim> &upoint = particles.unit_positions[index];
            vector<double> shape_funs = this->utilities.shape_funs(upoint, cell);
            // loop over DOFs of the cell and add the particle's charge to the system rhs
            for (int i = 0; i < n_dofs; ++i)
                density[dof_indices[i]] += shape_funs[i] * particles.weights[index];
        }
    }
    for (auto dof : this->vertex2dof)
        density[dof] /= this->dof_volume[dof];
}

template<int dim>
void PicSolver<dim>::calc_number_densities() {
    const int n_dofs = this->size();

    electrons.ndensity.resize(n_dofs);
    calc_number_density(electrons, electrons.ndensity);

    neutrals.ndensity.resize(n_dofs);
    calc_number_density(neutrals, neutrals.ndensity);

    for (auto &ion : ions) {
        ion.ndensity.resize(n_dofs);
        calc_number_density(ion, ion.ndensity);
    }
}

template<int dim>
void PicSolver<dim>::assemble_space_charge(const ParticleSet<dim> &particles) {
    if (particles.q_over_eps0 == 0.0)
        return;

    WorkStream::run(particles.filled_cells,  // loop over the cells that have particles inside
            std::bind(&PicSolver<dim>::assemble_cell, // call the function assemble_cell
                    this,
                    std::placeholders::_1, // with arg1 the CellIterator
                    std::placeholders::_2, // and arg2 the ScratchData
                    std::placeholders::_3, // and arg3 the CopyData
                    cref(particles)),      // and arg4 the particles
            std::bind(&PicSolver<dim>::copy_cell, // call the function copy_cell
                    this,
                    std::placeholders::_1,   // with arg1 the CopyData
                    ref(this->system_rhs)),  // and arg2 the system_rhs
            ScratchData(),                   // make dummy scratch data object
            CopyData(this->fe.dofs_per_cell) // make CopyData object
    );
}

template<int dim>
void PicSolver<dim>::copy_cell(const CopyData &copy_data, Vector<double> &system_rhs) const
{
    system_rhs.add(copy_data.dof_indices, copy_data.cell_rhs);
}

template<int dim>
void PicSolver<dim>::assemble_cell(const CellIterator &cell_it, const ScratchData &sd,
        CopyData &copy_data, const ParticleSet<dim> &particles) const
{
    const Cell &cell = *cell_it;
    if (this->shape_degree == 1) {
        assemble_cell_fast(cell, copy_data, particles);
        return;
    }

    // make a vector of the unit positions in this cell
    const vector<int> &indices = particles.parts_at(cell);
    vector<Point<dim>> unit_positions(indices.size());
    int i = 0;  // index of particle in the cell
    for (int index : indices)
        unit_positions[i++] = particles.unit_positions[index];

    // create Quadrature and FEValues objects
    Quadrature<dim> quadrature(unit_positions);
    FEValues<dim> fe_values(this->fe, quadrature, update_values);
    fe_values.reinit(cell);

    const uint n_dofs = this->fe.dofs_per_cell;
    copy_data.cell_rhs = 0;
    i = 0;
    // loop through the particles that are located in a cell
    for (int index : indices) {
        double charge_factor = particles.q_over_eps0 * particles.weights[index];
        for (uint dof = 0; dof < n_dofs; ++dof)
            copy_data.cell_rhs(dof) += fe_values.shape_value(dof, i) * charge_factor;
        i++;
    }

    // Prepare adding the current cell rhs entries to the system rhs
    cell->get_dof_indices(copy_data.dof_indices);
}

// In linear case, things can be speed up by using custom shape functions
template<int dim>
void PicSolver<dim>::assemble_cell_fast(const Cell &cell,
        CopyData &copy_data, const ParticleSet<dim> &particles) const
{
    const uint n_dofs = this->fe.dofs_per_cell;
    copy_data.cell_rhs = 0;

    // loop over particles that are in the cell
    for (int index : particles.parts_at(cell)) {
        // get the shape functions in the location of the particle with respect to the cell
        const Point<dim> &upoint = particles.unit_positions[index];
        vector<double> shape_funs = this->utilities.shape_funs(upoint, cell);

        // loop over DOFs of the cell and add the particle's charge to the system rhs
        double charge_factor = particles.q_over_eps0 * particles.weights[index];
        for (uint dof = 0; dof < n_dofs; ++dof)
            copy_data.cell_rhs(dof) += shape_funs[dof] * charge_factor;
    }

    // Prepare adding the current cell rhs entries to the system rhs
    cell->get_dof_indices(copy_data.dof_indices);
}

template<int dim>
void PicSolver<dim>::set_magnetic_field(ParticleSet<dim> &particles) const {
    for (auto &magnetic_field : particles.magnetic_fields) {
        magnetic_field = Vec3(this->conf->field.magnetic_field[0],
                this->conf->field.magnetic_field[1],
                this->conf->field.magnetic_field[2]);
    }
}

template<int dim>
void PicSolver<dim>::probe_field(ParticleSet<dim> &particles) const {
    WorkStream::run(particles.filled_cells,  // loop over the cells that have particles inside
            std::bind(&PicSolver<dim>::probe_cell,
                    this,
                    std::placeholders::_1,   // with arg1 the CellIterator
                    std::placeholders::_3,   // and arg2 the CopyData
                    cref(particles),         // and arg3 the particles
                    ref(particles.fields),   // and arg4 the fields vector
                    cref(this->solution)),   // and arg5 the solution vector
            std::bind(&PicSolver<dim>::dummy_copier, this),  // provide dummy copier function
            ScratchData(),                   // make dummy scratch data object
            CopyData(this->fe.dofs_per_cell) // make CopyData object
    );
}

template<int dim>
void PicSolver<dim>::probe_laplace_field(ChargedParticles<dim> &particles) const {
    WorkStream::run(particles.filled_cells,  // loop over the cells that have particles inside
            std::bind(&PicSolver<dim>::probe_cell,
                    this,
                    std::placeholders::_1,   // with arg1 the CellIterator
                    std::placeholders::_3,   // and arg2 the CopyData
                    cref(particles),         // and arg3 the particles
                    ref(particles.lfields),  // and arg4 the fields vector
                    cref(laplace_solution)), // and arg5 the solution vector
            std::bind(&PicSolver<dim>::dummy_copier, this),  // provide dummy copier function
            ScratchData(),                   // make dummy scratch data object
            CopyData(this->fe.dofs_per_cell) // make CopyData object
    );
}

template<int dim>
void PicSolver<dim>::probe_cell(const CellIterator &cell_it, CopyData &copy_data,
        const ParticleSet<dim> &particles, vector<Tensor<1,dim>> &fields,
        const Vector<double> &solution) const
{
    const Cell &cell = *cell_it;
    if (this->shape_degree == 1) {
        probe_cell_fast(cell, copy_data, particles, fields, solution);
        return;
    }

    const uint n_dofs = this->fe.dofs_per_cell;
    const vector<int> &indices = particles.parts_at(cell);
    cell->get_dof_indices(copy_data.dof_indices);

    // make a vector of the unit positions of the particles in this cell
    vector<Point<dim>> unit_positions(indices.size());
    int i = 0;
    for (int index : indices)
        unit_positions[i++] = particles.unit_positions[index];

    // create Quadrature and FEValues objects
    Quadrature<dim> quadrature(unit_positions);
    FEValues<dim> fe_values(this->fe, quadrature, update_gradients);
    fe_values.reinit(cell);

    // calculate the field for the particles that are located in a cell
    i = 0;
    for (int index : indices) {
        fields[index] *= 0.0;
        for (uint dof = 0; dof < n_dofs; ++dof)
            fields[index] -= solution(copy_data.dof_indices[dof]) * fe_values.shape_grad(dof, i);
        i++;
    }
}

template<int dim>
void PicSolver<dim>::probe_cell_fast(const Cell &cell, CopyData &copy_data,
        const ParticleSet<dim> &particles, vector<Tensor<1,dim>> &fields,
        const Vector<double> &solution) const
{
    const uint n_dofs = this->fe.dofs_per_cell;
    cell->get_dof_indices(copy_data.dof_indices);

    // loop through the particles within the cell
    const vector<int> &particle_indices = particles.parts_at(cell);
    for (int index : particle_indices) {
        const Point<dim> &upoint = particles.unit_positions[index];
        vector<Tensor<1,dim>> shape_grad = this->utilities.shape_fun_grads(upoint, cell);

        fields[index] *= 0.0;
        for (uint dof = 0; dof < n_dofs; ++dof)
            fields[index] -= solution(copy_data.dof_indices[dof]) * shape_grad[dof];
    }
}

template<int dim>
void PicSolver<dim>::save_laplace() {
    laplace_solution = this->solution;
    laplace_solution /= this->circuit->Vgap();
}

template<int dim>
double PicSolver<dim>::update_rhs(double theta) {
    if (this->system_rhs_save.size() != this->system_rhs.size()){
        this->system_rhs_save.reinit(this->system_rhs.size());
        this->system_rhs_save = 0.0;
    }

    auto error = this->system_rhs;
    error -= this->system_rhs_save;
    this->system_rhs *= theta;
    this->system_rhs_save *= (1.0 - theta);
    this->system_rhs += this->system_rhs_save;
    this->system_rhs_save = this->system_rhs;

    double rhs_norm = this->system_rhs.norm_sqr();
    if (rhs_norm > numeric_limits<double>::epsilon())
        return sqrt(error.norm_sqr() / this->system_rhs.norm_sqr());
    else
        return sqrt(error.norm_sqr());
}

template<int dim>
void PicSolver<dim>::write_vtk(ofstream& out) const {
    FieldSolver<dim>::write_vtk(out);
}

template<int dim>
void PicSolver<dim>::write_xyz(ofstream& out) const {
    // write the start of xyz header
    FileWriter::write_xyz(out);

    // extract coordinates of dofs
    vector<Point<dim>> support_points;
    this->export_dofs(support_points);

    vector<Tensor<1,dim>> field;
    this->export_solution_grad(field);

    const int n_dofs = support_points.size();
    const int n_verts = this->tria->n_used_vertices();

    // generate dof index -> vertex index mapping
    vector<int> dof2vertex(n_dofs);
    for (int i = 0; i < n_verts; ++i)
        dof2vertex[this->vertex2dof[i]] = i;

    // write Ovito header
    out << "properties=id:I:1:pos:R:" << support_points[0].dimension <<":force:R:" <<
            field[0].dimension << ":elfield:R:1" <<
            ":charge_density:R:1:number_density:R:" << 2 + ions.size() << ":volume:R:1:potential:R:1\n";

    bool e_ndens = electrons.ndensity.size() == n_dofs;
    bool n_ndens = neutrals.ndensity.size() == n_dofs;

    // write data
    for (int i = 0; i < n_dofs; ++i) {
        out << dof2vertex[i] << " " << support_points[i] << " "
                << field[dof2vertex[i]] << " " << field[dof2vertex[i]].norm()
                << " " << this->charge_density(i) << " "
                << (e_ndens ? electrons.ndensity[i] : 0.0) << " " << (n_ndens ? neutrals.ndensity[i] : 0.0) << " ";
        for (const auto &ion : ions) {
            out << (ion.ndensity.size() == n_dofs ? ion.ndensity[i] : 0.0) << " ";
        }
        out << this->dof_volume[i] << " " << this->solution(i) << "\n";
    }
}

template class PicSolver<2>;
template class PicSolver<3>;

} /* namespace femocs */
