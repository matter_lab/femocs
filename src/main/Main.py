import multiprocessing
import numpy as np
import csv
from femocs import Femocs


def test_parsing(fem, cfg_parameter, parameter_type):
    if parameter_type == "int":
        retval, error = fem.parse_int(cfg_parameter)
    elif parameter_type == "double":
        retval, error = fem.parse_double(cfg_parameter)
    elif parameter_type == "string":
        retval, error = fem.parse_string(cfg_parameter)
    else:
        raise RuntimeError("Unavailable parameter type: {}".format(parameter_type))

    if error:
        print("Could not parse " + cfg_parameter)
    else:
        print("{} = {}".format(cfg_parameter, retval))


def read_coordinates(file_name):
    with open(file_name, 'r') as file:
        csvreader = csv.reader(file, delimiter=' ')

        next(csvreader)  # skip nr of atoms
        next(csvreader)  # skip comments

        # read columns 1, 2 & 3 that contain x, y & z coordinates
        coordinates = [(float(row[1]), float(row[2]), float(row[3])) for row in csvreader]

    return np.array(coordinates)


def run_femocs():
    # Create the Femocs object
    fem = Femocs("in/md.in")
    print("\nFemocs version =", fem.version())

    # Read parameter from configuration file
    test_parsing(fem, "write_period", "int")
    test_parsing(fem, "elfield", "double")
    test_parsing(fem, "project", "string")

    # Import atoms coordinates and types from a file
    # n_atoms = fem.import_file("in/nanotip_small.xyz")
    # if n_atoms < 0:
    #     raise RuntimeError("Could not import atoms")
    # else:
    #     print("Imported {} atoms".format(n_atoms))

    # Import atoms coordinates from Numpy array
    coordinates = read_coordinates("in/nanotip_small.xyz")
    print("Importing coordinates {} ... {}".format(coordinates[0], coordinates[-1]))
    error = fem.import_doubles("coordinates", coordinates)
    if error:
        raise RuntimeError("Failed to import coordinates; error code: {}".format(error))

    # Measure RMS distance the atoms have moved & calculate neighbor list if it's > threshold
    neighbor_list = np.array([], dtype=np.int32)
    retval = fem.import_ints("neighbors", neighbor_list)
    # Instead of providing array of zero length, the array could be omitted as well like this:
    # retval = fem.import_ints("neighbors")
    if retval > 0:
        print("Recalculated neighbor list")
    else:
        print("Using available neighbor list")

    # Invoke the solver
    error = fem.run(0, 0.0)
    if error:
        raise RuntimeError("Failed to run Femocs; error code: {}".format(error))

    # Read electric field norm value on the atoms
    elfield = np.zeros(14891)
    error = fem.export_doubles(elfield, "elfield_norm")
    if error:
        raise RuntimeError("Failed to export doubles; error code: {}".format(error))
    print("\nmax elfield norm =", np.max(elfield))

    # Read atom types; capital latter(s) in 'Atom_type' will force truncating first all the array to zero
    atom_types = np.ones(14891, dtype=np.int32)
    error = fem.export_ints(atom_types, "Atom_type")
    if error:
        raise RuntimeError("Failed to export integers; error code: {}".format(error))
    print("types of atoms =", np.unique(atom_types))

    # Read node indices that build up tetrahedra and print the indices of first tetrahedron
    tetrahedra = fem.export_int("tetrahedra")
    print("n_tetrahedra = {}, \ttetrahedra[0:4] = {}".format(len(tetrahedra), tetrahedra[0:4]))

    # Read mesh node coordinates and print the x-, y- & z-coordinates of first node
    nodes = fem.export_double("nodes")
    print("n_nodes = {}, \tnodes[0:3] = {}".format(len(nodes), nodes[0:3]))


# Normally fem.run could not be killed with ctrl+c.
# One of the possible workarounds is to move Femocs runner into a separate process.
try:
    process = multiprocessing.Process(target=run_femocs)
    process.start()
    process.join()
except KeyboardInterrupt:
    process.terminate()
