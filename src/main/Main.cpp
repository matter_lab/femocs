/*
 *  Created on: 19.04.2016
 *      Author: veske
 */

#include "Femocs.h"
#include <filesystem>

using namespace std;


string get_path_to_conf(int n_args, char **args) {
    const string usage = "Usage: " + string(args[0]) + " path/to/md.in [n_iterations]";

    if (n_args < 2) {
        cerr << usage << endl;
        exit(1);
    }

    string path_to_conf = args[1];

    if (!filesystem::is_regular_file(filesystem::path(path_to_conf))) {
        cerr << usage << endl;
        exit(1);
    }

    return path_to_conf;
}

int get_n_iterations(int n_args, char **args) {
    if (n_args > 2)
        return atoi(args[2]);
    return 1;
}

string get_path_to_atoms(femocs::Femocs &femocs) {
    string path_to_atoms = "";
    int fail = femocs.parse_command("infile", path_to_atoms);
    if (path_to_atoms != "")
        path_to_atoms = femocs.input_dir() + path_to_atoms;

    return path_to_atoms;
}

int main(int n_args, char **args) {
    string path_to_conf = get_path_to_conf(n_args, args);
    int n_iterations = get_n_iterations(n_args, args);

    int fail = 0, n_atoms = 0;
    femocs::Femocs femocs(path_to_conf);

    string path_to_atoms = get_path_to_atoms(femocs);
    if (path_to_atoms != "")
        n_atoms = femocs.import_atoms(path_to_atoms);

    // vectors that will hold the results
    vector<double> elfield(3*n_atoms);
    vector<double> temperature(n_atoms);

    for (int i = 1; i <= n_iterations; ++i) {
        if (n_iterations > 1)
            printf("\n> Iteration %d\n", i);

        if (path_to_atoms != "" && i > 1)
            femocs.import_atoms(path_to_atoms, true);

        fail += femocs.run();
//        fail += femocs.export_data(elfield.data(), n_atoms, "elfield");
//        fail += femocs.export_data(temperature.data(), n_atoms, "temperature");
    }

    return 0;
}
