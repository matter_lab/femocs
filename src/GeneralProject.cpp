/*
 * GeneralProject.cpp
 *
 *  Created on: 8.2.2018
 *      Author: veske
 */

#include "GeneralProject.h"

namespace femocs {

GeneralProject::GeneralProject(AtomReader &r, Config& c) :
        reader(r), conf(c), mesh1(&c.mesh), mesh2(&c.mesh), new_mesh(&mesh1), mesh(NULL) {}

int GeneralProject::export_data(const double** data, const string& data_type) const {
    if (data_type == LABELS.nodes) {
        *data = mesh->nodes.get();
        return mesh->nodes.size();
    }

    return -1;
}

int GeneralProject::export_data(const int** data, const string& data_type) const {
    if (data_type == LABELS.edges) {
        *data = mesh->edges.get();
        return mesh->edges.size();
    }

    if (data_type == LABELS.triangles) {
        *data = mesh->tris.get();
        return mesh->tris.size();
    }

    if (data_type == LABELS.tetrahedra) {
        *data = mesh->tets.get();
        return mesh->tets.size();
    }

    if (data_type == LABELS.quadrangles) {
        *data = mesh->quads.get();
        return mesh->quads.size();
    }

    if (data_type == LABELS.hexahedra) {
        *data = mesh->hexs.get();
        return mesh->hexs.size();
    }

    return -1;
}

int GeneralProject::import_file(const string &file_name) {
    string file_type = get_file_type(file_name);
    if (file_type == "xyz" || file_type == "ckx")
        return reader.import_file(file_name);

    return -1;
}

int GeneralProject::import_data(const double* data, const int n_data, const string& data_type) {
    if (data_type == "coordinates")
        return reader.import_lammps_coordinates(n_data, data);
    if (data_type == "parcas-coordinates")
        return reader.import_parcas_coordinates(n_data, data);
    if (data_type == LABELS.velocity)
        return reader.import_lammps_velocities(n_data, data);
    if (data_type == LABELS.parcas_velocity)
        return reader.import_parcas_coordinates(n_data, data);
    if (data_type == "parcas-box") {
        reader.set_parcas_box(data);
        return 0;
    }

    return -1;
}

int GeneralProject::import_data(const int* data, const int n_data, const string& data_type) {
    if (data_type == "neighbors")
        return reader.analyse(data);

    return -1;
}

} /* namespace femocs */
